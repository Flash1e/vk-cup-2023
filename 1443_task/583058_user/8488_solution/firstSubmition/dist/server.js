"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var path_1 = __importDefault(require("path"));
var fs_1 = __importDefault(require("fs"));
var http_1 = __importDefault(require("http"));
var letterController_1 = require("./letterController");
var PORT = 3000;
var MIME_TYPES = new Map([
    ["default", "application/octet-stream"],
    ["html", "text/html; charset=UTF-8"],
    ["js", "application/javascript; charset=UTF-8"],
    ["css", "text/css"],
    ["png", "image/png"],
    ["jpg", "image/jpg"],
    ["gif", "image/gif"],
    ["ico", "image/x-icon"],
    ["svg", "image/svg+xml"],
]);
http_1.default
    .createServer(function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var filePath, exists, ext, contentType;
    return __generator(this, function (_a) {
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Methods", "OPTIONS, GET");
        res.setHeader("Access-Control-Max-Age", 2592000); // 30 days
        if (!req.url)
            throw new Error("No url");
        // Serving letter by id (.../api/:folderName/:letterId?...)
        if (req.url.split("/api/")[1] &&
            req.url.split("/")[3] &&
            req.method === "GET") {
            return [2 /*return*/, (0, letterController_1.getLetterById)(req, res)];
        }
        // Move letter to folder (.../api/:folderName/:letterId?...)
        if (req.url.split("/api/")[1] &&
            req.url.split("/")[3] &&
            req.method === "UPDATE") {
            return [2 /*return*/, (0, letterController_1.moveLetter)(req, res)];
        }
        // Post letter (.../api/:folderName/:letterId?...)
        if (req.url.split("/api/")[1] &&
            req.url.split("/")[3] &&
            req.method === "POST") {
            return [2 /*return*/, (0, letterController_1.sendLetter)(req, res)];
        }
        // Serving letters in folder (.../api/:folderName?...)
        if (req.url.split("/api/")[1] &&
            req.url.split("/")[2] &&
            req.method === "GET") {
            return [2 /*return*/, (0, letterController_1.getLettersByFolderName)(req, res)];
        }
        // Serving static files
        if (!req.url.split(".")[1]) {
            req.url = "index.html";
        }
        filePath = path_1.default.join(__dirname, req.url);
        exists = fs_1.default.existsSync(filePath);
        if (!exists) {
            res.writeHead(404, { "Content-Type": "text/plain" });
            res.write("404 Not Found");
            res.end();
            return [2 /*return*/];
        }
        ext = path_1.default.extname(filePath).substring(1).toLowerCase();
        contentType = MIME_TYPES.get(ext) || MIME_TYPES.get("default");
        res.writeHead(200, { "Content-Type": contentType });
        fs_1.default.createReadStream(filePath).pipe(res);
        return [2 /*return*/];
    });
}); })
    .listen(PORT);
console.log("Server running at http://127.0.0.1:".concat(PORT, "/"));
