const { readFileSync } = require("fs");
const { createServer } = require("http")

const letters = JSON.parse(readFileSync('./db1000.json', { encoding: "utf-8" }));
letters.sort((a, b) => Date.parse(b.date) - Date.parse(a.date));
letters.forEach((letter, idx) => letter.id = idx + '');

const lettersForFolder = letters.map((letter) => {
    const result = JSON.parse(JSON.stringify(letter));
    result.doc = Boolean(result.doc);
    delete result.to;
    result.text = (result.text || '').slice(0, 140 - (result.title || '').length);
    return result;
})

const folders = ['Архив', 'Отправленные', 'Корзина', 'Важное', 'Спам', 'Черновики'];

function handleRequest(req, res) {
    try {
        if (decodeURI(req.url).split('?')[0] === "/letters") {
            const splitted = decodeURI(req.url).split('?')[1]
            const folderMatch = splitted && splitted.match(/folder=([^&]+)/);
            const parsedFolder = folderMatch ? folderMatch[1] : 'Входящие';
            const folder = folders.indexOf(parsedFolder) == -1 ? undefined : parsedFolder;
            const flagFilter = splitted && splitted.match(/flag=1/);
            const unreadFilter = splitted && splitted.match(/unread=1/);
            const filesFilter = splitted && splitted.match(/files=1/);
            const queryFilter = splitted && splitted.match(/query=([^&]+)/);
            const foundLetters = lettersForFolder.filter((letter) => {
                if (letter.folder != folder) {
                    return false;
                }
                if (flagFilter && !letter.bookmark) {
                    return false;
                }
                if (unreadFilter && letter.read) {
                    return false;
                }
                if (filesFilter && !letter.doc) {
                    return false;
                }
                if (queryFilter) {
                    if (
                        letter.text.indexOf(queryFilter[1]) == -1 &&
                        letter.title.indexOf(queryFilter[1]) == -1 &&
                        (!letter.author || !letter.author.name || letter.author.name.indexOf(queryFilter[1]) == -1) &&
                        (!letter.author || !letter.author.email || letter.author.email.indexOf(queryFilter[1]) == -1) &&
                        (!letter.author || !letter.author.surname || letter.author.surname.indexOf(queryFilter[1]) == -1)
                    ) {
                        return false;
                    }
                }
                return true;
            });
            const startPosition = splitted && splitted.match(/start=(\d+)/);
            const endPosition = splitted && splitted.match(/end=(\d+)/);
            let slicedLetters = foundLetters;
            if (startPosition && endPosition) {
                slicedLetters = slicedLetters.slice(Number(startPosition[1]), Number(endPosition[1]));
            }
            res.writeHead(200, { 'Content-Type': 'text/json' })
            res.end(JSON.stringify({
                letters: slicedLetters,
                amount: foundLetters.length
            }));
            return
        }
        if (decodeURI(req.url).split('?')[0] === "/letter") {
            const splitted = decodeURI(req.url).split('?')[1]
            const idMatch = splitted && splitted.match(/id=(\d+)/);
            const parsedId = idMatch ? idMatch[1] : '0';
            const result = letters.filter((letter) => letter.id == parsedId)[0];
            res.writeHead(200, { 'Content-Type': 'text/json' })
            res.end(JSON.stringify(result));
            return
        }
        if (decodeURI(req.url).split('?')[0] === "/letter-new") {
            var body = ''
            req.on('data', function(data) {
                body += data;
            })
            req.on('end', function() {
                try {
                    const rawLetter = JSON.parse(body);
                    const letter = {
                        author: {
                            name: "Королев",
                            surname: "Лев",
                            "email": "yndx-korolev@yandex.ru"
                        },
                        to: rawLetter.to.split(',').map((person) => {
                            return {
                                name: person,
                                surname: "",
                                email: `${person}@yandex.ru`
                            }
                        }),
                        title: rawLetter.title,
                        text: rawLetter.content,
                        bookmark: false,
                        important: rawLetter.important,
                        read: false,
                        folder: 'Отправленные',
                        date: new Date().toISOString(),
                        id: letters.length + ''
                    }
                    letters.unshift(letter);
                    const rectified = JSON.parse(JSON.stringify(letter));
                    rectified.doc = Boolean(rectified.doc);
                    delete rectified.to;
                    rectified.text = (rectified.text || '').slice(0, 140 - (rectified.title || '').length);
                    lettersForFolder.unshift(rectified);
                    res.writeHead(200, {'Content-Type': 'text/json'})
                    res.end(`{"id": "${letter.id}"}`)
                } catch(e) {
                    res.writeHead(500, {'Content-Type': 'text/json'})
                    res.end('{"error": "Failed to send letter"}')
                }
            });
            return
        }
        if (decodeURI(req.url).split('?')[0] === "/letter-update") {
            const splitted = decodeURI(req.url).split('?')[1]
            const idMatch = splitted && splitted.match(/id=(\d+)/);
            const parsedId = idMatch ? idMatch[1] : '-1';
            const readMatch = splitted && splitted.match(/read=(\d+)/);
            const bookmarkMatch = splitted && splitted.match(/bookmark=(\d+)/);
            letters.forEach((letter) => {
                if (letter.id == parsedId) {
                    if (readMatch) {
                        letter.read = readMatch[1] == '1';
                    }
                    if (bookmarkMatch) {
                        letter.bookmark = bookmarkMatch[1] == '1';
                    }
                }
            });
            lettersForFolder.forEach((letter) => {
                if (letter.id == parsedId) {
                    if (readMatch) {
                        letter.read = readMatch[1] == '1';
                    }
                    if (bookmarkMatch) {
                        letter.bookmark = bookmarkMatch[1] == '1';
                    }
                }
            });
            res.writeHead(200)
            res.end("");
            return
        }
        if (decodeURI(req.url).split('?')[0].startsWith("/letter-download-image")) {
            const idMatch = decodeURI(req.url).match(/letter-download-image-(\d+)/);
            const parsedId = idMatch ? idMatch[1] : '-1';
            const letter = letters.filter((letter) => letter.id == parsedId)[0];
            if (letter && letter.doc && letter.doc.img) {
                const img = letter.doc.img;
                const typeMatch = img.match(/data:([^;]+);/);
                const bodyMatch = img.match(/base64,(.+)/);
                if (typeMatch && bodyMatch) {
                    const body = Buffer.from(bodyMatch[1], 'base64');
                    res.writeHead(200, { 'Content-Type': typeMatch[1] });
                    res.end(body);
                    return
                }   
            }
            res.writeHead(404)
            res.end("");
            return
        }
        if (req.url.startsWith("/file-")) {
            try {
                const file = readFileSync(`.${req.url}`);
                if (req.url.endsWith(".svg")) {
                    res.writeHead(200, { 'Content-Type': 'image/svg+xml' });
                } else if (req.url.endsWith(".webp")) {
                    res.writeHead(200, { 'Content-Type': 'image/webp' });
                }
                res.end(file);
            } catch {
                res.writeHead(404);
                res.end("no such file");
            }
            return;
        }
    } catch (e) {

    }
    const index = readFileSync('./index.html', { encoding: "utf-8" });
    res.writeHead(200, { 'Content-Type': 'text/html' });
    res.end(index);
}

createServer(handleRequest).listen(3000);

console.log('Server is started')
