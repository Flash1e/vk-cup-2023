/** @type {import('eslint').ESLint.ConfigData} */
module.exports = {
  root: true,
  extends: ['airbnb', 'airbnb-typescript'],
  parserOptions: {
    project: './tsconfig.eslint.json',
  },
  rules: {
    'import/prefer-default-export': 0,
    'object-curly-newline': 0,
    'implicit-arrow-linebreak': 0,
    'operator-linebreak': 0,
    'no-await-in-loop': 0,
    'consistent-return': 0,
    'no-restricted-syntax': 0,
    'function-paren-newline': 0,
    'no-continue': 0,
    'no-confusing-arrow': 0,
    'prefer-destructuring': 0,

    'react/jsx-props-no-spreading': 0,
    'react/function-component-definition': 0,
    'react/require-default-props': 0,
    'react/prop-types': 0,
    'react/jsx-wrap-multilines': 0,
    'react/jsx-one-expression-per-line': 0,

    '@typescript-eslint/indent': 0,
    '@typescript-eslint/no-shadow': 0,
    '@typescript-eslint/lines-between-class-members': 0,
    '@typescript-eslint/brace-style': 0,
  },
};
