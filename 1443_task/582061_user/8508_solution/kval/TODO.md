### Need to implement

1. ~~Letter page~~
2. ~~Filters~~
3. ~~Theme~~
4. ~~I18n~~
5. ~~Migrate to Preact~~
6. ~~Optimize bundle size~~ (Remove `react-router-dom`, add `dynamic-imports`)
7. ~~Letter list virtualization~~
8. ~~Remove `rc-tooltip`~~
9. New letter interface

### Known issues / missing features

1. Hide popups on scroll
2. Low performance of sidebar collapse
3. Theme "blinks" on site open
4. ~~Loader become dot in popups~~
5. ~~AttachmentPopup sometimes opens immediately. Perhaps, because of fast loader unmounting. Probably would be fixed when network is slower~~
6. ~~Letters fetched twice on page open~~
7. ~~Sidebar does not collapse automatically on small resolution~~
8. Check in different browsers
9. ~~Important flag on letter list~~
10. Add `Return` button to letter page + add keyboard shortcut
11. ~~Add Menu drawer on small resolutions~~
12. ~~Restructure themes in scss~~
13. ~~There is an error in icon sizing. Need to fix it.~~
14. Wrong button color in Language Settings
15. ~~Add pluralization~~
16. Add Avatar placeholder
17. Save scroll position in LetterList
18. Hide Filter button on LetterPage
19. Add letters count to Folders
20. ~~Hide loader in infinite scroll, when there are no portions left~~
21. ~~Optimize state with letter list - using Context is too slow~~ `Context is ok for now`
22. Cancel filter request if filter change before previous request finishes

### Improvements

1. ~~Move UI library outside to different workspace~~
2. Add proper Networking layer

### Think about

1. State Management? - Effector? - it would be better to use `react-sweet-state`, easy to use, lightweight
2. CORS Policy
