export enum ScrollEvent {
  Scroll = 'scroll',
  End = 'end',
}
