import { ISubscribable } from 'pubsub';

import { ScrollHandlerParams } from '../types';
import { ScrollEvent } from '../types/ScrollEvent';

export interface IScrollController
  extends ISubscribable<ScrollEvent, ScrollHandlerParams> {
  /**
   * Gets current scroll speed
   */
  get scrollSpeed(): number;

  /**
   * Gets current scrollTop of the anchor
   */
  get scrollTop(): number;

  /**
   * Attaches scroll event handlers to given node
   * @param node node to attach scroll events
   */
  attach(node: HTMLElement): void;

  /**
   * Scrolls by given top offset
   * @param value
   */
  scrollTo(value: number): void;

  /**
   * Removes all references to event handlers, detaches event handlers from attached node
   */
  dispose(): void;
}
