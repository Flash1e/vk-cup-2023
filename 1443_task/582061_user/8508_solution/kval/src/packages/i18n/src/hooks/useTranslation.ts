import React from 'react';

import { I18n } from '../I18n';

export const useTranslation = () => {
  const t = React.useCallback(I18n.Instance.translate, []);

  return { t, i18n: I18n.Instance };
};
