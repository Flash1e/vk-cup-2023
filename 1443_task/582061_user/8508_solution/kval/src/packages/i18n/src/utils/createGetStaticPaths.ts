import { GetStaticPaths } from 'next';

import { Language, I18nNextParams } from '../types';

export function createGetStaticPaths(): GetStaticPaths<I18nNextParams> {
  return () => ({
    fallback: false,
    paths: [
      {
        params: { lang: [Language.Ru] },
      },
      {
        params: { lang: [Language.En] },
      },

      // DEV ONLY
      {
        params: { lang: ['inbox' as any] },
      },
    ],
  });
}
