import { EventHandler } from '../types';

/**
 * Creates handler that is invoked only if target is present in event
 * @param handler
 * @returns
 */
export function createTargetHandler<T extends React.SyntheticEvent>(
  handler: (target: HTMLElement, event: T) => void,
): EventHandler<T> {
  return (event) => {
    const target = event.currentTarget as HTMLElement | undefined;

    if (!target) {
      return;
    }

    handler(target, event);
  };
}
