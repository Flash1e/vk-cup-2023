import { Trigger, TriggerHandlersFactory } from '../types';

/**
 * Creates new trigger
 * @param key
 * @param defaultOptions
 * @param createHandlers
 * @returns
 */
export function createTrigger<K extends string, O extends object>(
  key: K,
  defaultOptions: O,
  createHandlers: TriggerHandlersFactory<O>,
): Trigger<K, O> {
  return {
    key,
    defaultOptions,
    createHandlers,
  };
}
