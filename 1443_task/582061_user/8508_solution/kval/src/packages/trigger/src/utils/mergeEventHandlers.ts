import { TriggerEvent } from '../types';

/**
 * Merges multiple props into single one, concatenating duplicated event handlers into single chain.
 * Props are merged from end to start order
 * @param base
 * @param merge
 * @returns
 */
export function mergeEventHandlers<P extends object = any>(
  base: P,
  ...merge: P[]
): P {
  let merged = base;

  for (let i = merge.length - 1; i > 0; i -= 1) {
    const props = merge[i];

    merged = Object.values(TriggerEvent).reduce((acc, handlerKey) => {
      const handler = props[
        handlerKey as keyof typeof props
      ] as React.EventHandler<any>;

      if (typeof handler !== 'function') {
        return acc;
      }

      return {
        ...acc,
        ...props,
        [handlerKey]: (event: React.SyntheticEvent) => {
          handler(event);

          const baseHandler = acc[
            handlerKey as keyof typeof acc
          ] as React.EventHandler<any>;

          if (typeof baseHandler !== 'undefined') {
            baseHandler(event);
          }
        },
      };
    }, merged);
  }

  return merged;
}
