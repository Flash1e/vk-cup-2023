import React from 'react';

import { TriggerAnchorCallback, TriggerStateChange } from '../types';
import { TriggerKey, TRIGGERS } from '../triggers';

export type UseTriggerParams<T extends TriggerKey> = {
  readonly trigger: T;
  readonly options: typeof TRIGGERS[T]['defaultOptions'];
  readonly containerNode: HTMLElement | null;
  readonly anchorNode: HTMLElement | null;
  readonly onAnchorSet: TriggerAnchorCallback;
  readonly onStateChange: TriggerStateChange;
};

export const useTrigger = <T extends TriggerKey>(
  params: UseTriggerParams<T>,
) => {
  const {
    trigger: triggerKey,
    containerNode,
    anchorNode,
    options,
    onAnchorSet,
    onStateChange,
  } = params;

  const [anchor, setAnchor] = React.useState<HTMLElement | null>(null);
  const [isOpened, setIsOpened] = React.useState(false);

  const handlers = React.useMemo(() => {
    const trigger = TRIGGERS[triggerKey];

    // TODO: think about types
    return trigger.createHandlers(
      {
        ...trigger.defaultOptions,
        ...options,
      } as any,
      setAnchor,
      setIsOpened,
    );
  }, [triggerKey]);

  React.useEffect(() => handlers.dispose, [handlers.props]);

  React.useEffect(() => {
    if (triggerKey !== 'click') {
      return;
    }

    const handler = (event: MouseEvent) => {
      if (!containerNode || !anchorNode) {
        return;
      }

      const target = event.target as HTMLElement;

      if (!containerNode.contains(target) && !anchorNode.contains(target)) {
        setIsOpened(false);
      }
    };

    document.addEventListener('click', handler);

    return () => document.removeEventListener('click', handler);
  }, [triggerKey, containerNode, anchor]);

  // React to isOpened update
  React.useEffect(() => {
    onStateChange(isOpened);
  }, [isOpened, onStateChange]);

  // React to anchor update
  React.useEffect(() => {
    if (anchor) {
      onAnchorSet(anchor);
    }
  }, [anchor, onAnchorSet]);

  return { handlers: handlers.props };
};
