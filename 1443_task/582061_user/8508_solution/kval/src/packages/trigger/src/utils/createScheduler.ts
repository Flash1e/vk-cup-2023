// TODO: move to different library
export type Scheduler = {
  delay: (delayMs: number, action: () => void) => void;
  clear: () => void;
};

export function createScheduler(): Scheduler {
  let timeoutId: number | null = null;

  const clear: Scheduler['clear'] = () => {
    if (timeoutId) {
      window.clearTimeout(timeoutId);
    }
  };

  const delay: Scheduler['delay'] = (delayMs, action) => {
    clear();
    timeoutId = window.setTimeout(action, delayMs);
  };

  return {
    delay,
    clear,
  };
}
