import React from 'react';
import { createIcon } from './icon/createIcon';

export const IconLoader = createIcon(
  200,
  200,
  <>
    <defs>
      <linearGradient id="spinner-secondHalf">
        <stop offset="0%" stopOpacity="0" stopColor="currentColor" />
        <stop offset="100%" stopOpacity="0.5" stopColor="currentColor" />
      </linearGradient>
      <linearGradient id="spinner-firstHalf">
        <stop offset="0%" stopOpacity="1" stopColor="currentColor" />
        <stop offset="100%" stopOpacity="0.5" stopColor="currentColor" />
      </linearGradient>
    </defs>

    <g strokeWidth="30">
      <path
        stroke="url(#spinner-secondHalf)"
        d="M 20 100 A 70 70 0 0 1 178 100"
      />
      <path
        stroke="url(#spinner-firstHalf)"
        d="M 178 100 A 70 70 0 0 1 20 100"
      />
      <path
        stroke="currentColor"
        strokeLinecap="round"
        d="M 20 100 A 70 70 0 0 1 20 98"
      />
    </g>

    <animateTransform
      from="0 0 0"
      to="360 0 0"
      attributeName="transform"
      type="rotate"
      repeatCount="indefinite"
      dur="1000ms"
    />
  </>,
);
