import React from 'react';
import { createIcon } from './icon/createIcon';

export const IconAttachment = createIcon(
  20,
  20,
  <path
    fill="currentColor"
    d="M16.828 4.587a4 4 0 0 0-5.657 0l-.089.09a4.007 4.007 0 0 1 1.414 1.413l.09-.089a2 2 0 1 1 2.828 2.829L9.12 15.122A3 3 0 0 1 4.88 10.88l3.464-3.464A1 1 0 1 1 9.757 8.83L7.05 11.536a1 1 0 1 0 1.415 1.415l2.706-2.707A3 3 0 0 0 6.93 6.001L3.463 9.465a5 5 0 0 0 7.072 7.071l6.292-6.292a4 4 0 0 0 0-5.657Z"
  />,
);
