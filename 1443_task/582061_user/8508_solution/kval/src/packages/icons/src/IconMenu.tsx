import React from 'react';
import { createIcon } from './icon/createIcon';

export const IconMenu = createIcon(
  20,
  20,
  <path
    fill="currentColor"
    fillRule="evenodd"
    d="M16 14a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2h12Zm0-5a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2h12Zm0-5a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2h12Z"
    clipRule="evenodd"
  />,
);
