import React from 'react';
import { createIcon } from './icon/createIcon';

export const IconPalette = createIcon(
  20,
  20,
  <>
    <path
      stroke="currentColor"
      strokeWidth="2"
      d="m14.284 13.043-.21-.023c-.682-.073-1.064-.114-1.12-.118-.505-.035-1.037-.014-1.48.21a1.496 1.496 0 0 0-.83 1.276c-.026.384.073.777.14 1.037l.067.25c.055.206.105.389.145.592A1.012 1.012 0 0 1 10 17.5a7.474 7.474 0 0 1-5.303-2.197A7.474 7.474 0 0 1 2.5 10c0-2.071.838-3.945 2.197-5.303A7.473 7.473 0 0 1 10 2.5c2.071 0 3.945.838 5.303 2.197A7.473 7.473 0 0 1 17.5 10c0 1.414-.457 2.153-.969 2.548-.546.42-1.34.592-2.247.495Z"
    />
    <rect
      width="2.5"
      height="2.5"
      x="10.8"
      y="4.8"
      fill="currentColor"
      rx="1.25"
    />
    <rect
      width="2.5"
      height="2.5"
      x="13.2"
      y="8.2"
      fill="currentColor"
      rx="1.25"
    />
    <rect
      width="2.5"
      height="2.5"
      x="4.3"
      y="8.2"
      fill="currentColor"
      rx="1.25"
    />
    <rect
      width="2.5"
      height="2.5"
      x="6.6"
      y="4.8"
      fill="currentColor"
      rx="1.25"
    />
  </>,
);
