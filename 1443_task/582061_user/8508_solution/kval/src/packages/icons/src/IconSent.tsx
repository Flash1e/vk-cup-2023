import React from 'react';
import { createIcon } from './icon/createIcon';

export const IconSent = createIcon(
  20,
  20,
  <path
    fill="currentColor"
    fillRule="evenodd"
    d="M10.383 3.076A1 1 0 0 1 11 4v2.5h1a5 5 0 0 1 5 5V16a1 1 0 1 1-2 0 2.5 2.5 0 0 0-2.5-2.5H11V16a1 1 0 0 1-1.707.707l-6-6a1 1 0 0 1 0-1.414l6-6a1 1 0 0 1 1.09-.217ZM15 12.258V11.5a3 3 0 0 0-3-3h-2a1 1 0 0 1-1-1V6.414L5.414 10 9 13.586V12.5a1 1 0 0 1 1-1h2.5c.925 0 1.785.28 2.5.758Z"
    clipRule="evenodd"
  />,
);
