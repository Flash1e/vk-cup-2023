import React from 'react';
import { createIcon } from './icon/createIcon';

export const IconCheckmark = createIcon(
  8,
  6,
  <path
    fill="currentColor"
    fillRule="evenodd"
    d="M7.7.3c-.4-.4-1-.4-1.4 0L3 3.6 1.7 2.3c-.4-.4-1-.4-1.4 0-.4.4-.4 1 0 1.4l2 2c.2.2.4.3.7.3.3 0 .5-.1.7-.3l4-4c.4-.4.4-1 0-1.4Z"
    clipRule="evenodd"
  />,
);
