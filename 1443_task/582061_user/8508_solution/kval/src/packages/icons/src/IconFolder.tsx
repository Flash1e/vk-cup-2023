import React from 'react';
import { createIcon } from './icon/createIcon';

export const IconFolder = createIcon(
  20,
  20,
  <path
    fill="currentColor"
    fillRule="evenodd"
    d="M3 5a2 2 0 0 1 2-2h3.382a2 2 0 0 1 1.789 1.106l.447.894H14a3 3 0 0 1 3 3v6a3 3 0 0 1-3 3H6a3 3 0 0 1-3-3V5Zm5.382 0H5v9a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V8a1 1 0 0 0-1-1H9.382l-1-2Z"
    clipRule="evenodd"
  />,
);
