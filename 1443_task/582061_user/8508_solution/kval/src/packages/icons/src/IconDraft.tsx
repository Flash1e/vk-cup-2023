import React from 'react';
import { createIcon } from './icon/createIcon';

export const IconDraft = createIcon(
  20,
  20,
  <>
    <path
      fill="currentColor"
      d="M13 7a1 1 0 0 1-1 1H8a1 1 0 1 1 0-2h4a1 1 0 0 1 1 1ZM12 11a1 1 0 1 0 0-2H8a1 1 0 1 0 0 2h4ZM10 14a1 1 0 1 0 0-2H8a1 1 0 1 0 0 2h2Z"
    />
    <path
      fill="currentColor"
      fillRule="evenodd"
      d="M3 15a3 3 0 0 0 3 3h8a3 3 0 0 0 3-3V5a3 3 0 0 0-3-3H6a3 3 0 0 0-3 3v10Zm3 1a1 1 0 0 1-1-1V5a1 1 0 0 1 1-1h8a1 1 0 0 1 1 1v10a1 1 0 0 1-1 1H6Z"
      clipRule="evenodd"
    />
  </>,
);
