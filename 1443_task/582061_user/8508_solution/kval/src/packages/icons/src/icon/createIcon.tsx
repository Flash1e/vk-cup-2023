import React from 'react';
import clsx from 'clsx';

import styles from './Icon.module.scss';

export type IconProps = {
  readonly className?: string;
  readonly iconClassName?: string;
  readonly size?: 'md' | 'lg' | 'unset';
  readonly width?: string;
  readonly height?: string;
};

export function createIcon(
  width: number,
  height: number,
  content: React.ReactNode,
): Utils.FCWithComponentProp<IconProps, 'span'> {
  return ({
    className,
    iconClassName,
    color,
    size = 'md',
    component: Component = 'span',
    width: widthFromProps,
    height: heightFromProps,
    ...rest
  }) => (
    <Component
      {...rest}
      className={clsx(className, styles.root, styles[size])}
      style={{ width: widthFromProps, height: heightFromProps }}
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width={width}
        height={height}
        viewBox={`0 0 ${width} ${height}`}
        fill="none"
        className={iconClassName}
      >
        {content}
      </svg>
    </Component>
  );
}
