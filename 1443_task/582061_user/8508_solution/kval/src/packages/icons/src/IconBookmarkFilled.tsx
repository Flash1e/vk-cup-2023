import React from 'react';
import { createIcon } from './icon/createIcon';

export const IconBookmarkFilled = createIcon(
  20,
  20,
  <path
    fill="currentColor"
    fillRule="evenodd"
    d="M15 4.784A1.78 1.78 0 0 0 13.221 3H6.779A1.78 1.78 0 0 0 5 4.784v11.22a1 1 0 0 0 1.746.669l2.793-3.13a.618.618 0 0 1 .922 0l2.793 3.13A1 1 0 0 0 15 16.006V4.784Z"
    clipRule="evenodd"
  />,
);
