import { CustomTheme, Theme } from '../types';

export function isCustomTheme(theme: Theme): theme is CustomTheme {
  return typeof theme === 'object';
}
