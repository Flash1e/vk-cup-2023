import { SystemTheme, Theme } from '../types';
import { isCustomTheme } from './isCustomTheme';

const KEY = 'theme';

export abstract class ThemeLocalStorage {
  public static get(): SystemTheme | string | null {
    return localStorage.getItem(KEY) as SystemTheme | null;
  }

  public static set(theme: Theme): void {
    if (isCustomTheme(theme)) {
      localStorage.setItem(KEY, theme.name);

      return;
    }

    localStorage.setItem(KEY, theme);
  }

  public static isStored(): boolean {
    return Boolean(localStorage.getItem(KEY));
  }
}
