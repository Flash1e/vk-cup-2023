import { SystemTheme, Theme } from '../types';
import { isCustomTheme } from './isCustomTheme';

function getContrastToken(theme: SystemTheme): string {
  return `theme-${theme}-contrast`;
}

function getThemeToken(theme: string): string {
  return `theme-${theme}`;
}

function getThemeTokens(theme: Theme): string[] {
  if (isCustomTheme(theme)) {
    const tokens: string[] = [getThemeToken(theme.extendsFrom)];

    if (theme.contrast) {
      tokens.push(getContrastToken(theme.contrast));
    }

    return tokens;
  }

  return [getThemeToken(theme)];
}

export function setDomTheme(theme: Theme, prevTheme: Theme | null): void {
  if (typeof document === 'undefined') {
    return;
  }

  const root = document.querySelector('html');

  if (!root) {
    return;
  }

  if (prevTheme) {
    root.classList.remove(...getThemeTokens(prevTheme));

    if (isCustomTheme(prevTheme) && prevTheme.overrides) {
      Object.keys(prevTheme.overrides).forEach((key) =>
        root.style.setProperty(key, null),
      );
    }
  }

  if (isCustomTheme(theme)) {
    root.classList.add(...getThemeTokens(theme));

    if (theme.overrides) {
      Object.entries(theme.overrides).forEach(([key, value]) =>
        root.style.setProperty(key, value),
      );
    }

    return;
  }

  root.classList.add(getThemeToken(theme));
}
