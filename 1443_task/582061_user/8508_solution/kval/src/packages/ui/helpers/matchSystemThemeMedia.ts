import { Theme } from '../types';

export function matchSystemThemeMedia(theme: Theme): MediaQueryList {
  return window.matchMedia(`(prefers-color-scheme: ${theme})`);
}
