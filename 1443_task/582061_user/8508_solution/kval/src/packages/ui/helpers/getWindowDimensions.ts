import { getWindowSafe } from './getWindowSafe';

export function getWindowDimensions(): [width: number, height: number] {
  const window = getWindowSafe();

  if (!window) {
    return [NaN, NaN];
  }

  return [window.innerWidth, window.innerHeight];
}
