import React from 'react';

import { SystemTheme, CustomTheme, Theme } from '../types';
import { useSystemTheme } from '../hooks';
import { setDomTheme, ThemeLocalStorage } from '../helpers';
import { ThemeContext, ThemeContextState } from '../contexts/theme-context';

export type ThemeProviderProps = {
  readonly customThemes?: CustomTheme[];
};

export const ThemeProvider: React.FC<
  React.PropsWithChildren<ThemeProviderProps>
> = ({ children, customThemes }) => {
  const prevThemeRef = React.useRef<Theme | null>(null);
  const [theme, setTheme] = React.useState<Theme>(SystemTheme.Light);

  const setThemeSafe = React.useCallback(
    (nextTheme: Theme | CustomTheme, priority: 'user' | 'system') => {
      if (priority === 'system' && ThemeLocalStorage.isStored()) {
        return;
      }

      prevThemeRef.current = theme;

      setTheme(nextTheme);
      ThemeLocalStorage.set(nextTheme);
    },
    [theme],
  );

  const handleSystemThemeChange = React.useCallback(
    (theme: Theme) => setThemeSafe(theme, 'system'),
    [setThemeSafe],
  );

  const set = React.useCallback(
    (theme: Theme) => setThemeSafe(theme, 'user'),
    [setThemeSafe],
  );

  useSystemTheme({ onThemeChange: handleSystemThemeChange });

  React.useEffect(() => setDomTheme(theme, prevThemeRef.current), [theme]);

  // TODO: move to hook `useSavedTheme`
  React.useEffect(() => {
    const theme = ThemeLocalStorage.get();

    if (!theme) {
      return;
    }

    if (Object.values(SystemTheme).includes(theme as SystemTheme)) {
      setThemeSafe(theme as SystemTheme, 'user');

      return;
    }

    const customTheme = customThemes?.find(({ name }) => name === theme);

    if (customTheme) {
      setThemeSafe(customTheme, 'user');
    }
  }, []);

  const contextValue = React.useMemo<ThemeContextState>(
    () => ({ current: theme, set }),
    [theme],
  );

  return (
    <ThemeContext.Provider value={contextValue}>
      {children}
    </ThemeContext.Provider>
  );
};
