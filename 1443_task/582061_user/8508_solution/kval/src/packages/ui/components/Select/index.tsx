import React from 'react';
import clsx from 'clsx';

import { List } from '../List';
import { Divider } from '../Divider';
import { SelectContext, SelectValueState } from './SelectContext';
import { Option } from './partials/Option';
import { Action } from './partials/Action';

import styles from './Select.module.scss';

export type SelectProps = {
  readonly value: SelectValueState;
  readonly onChange: (state: SelectValueState) => void;
  readonly isMultiSelect?: boolean;
  readonly hasResetOption?: boolean;
  readonly resetOptionText?: string;
  readonly onReset?: () => void;
  readonly className?: string;
};

export const Select: React.FC<React.PropsWithChildren<SelectProps>> & {
  Option: typeof Option;
  Action: typeof Action;
} = ({
  value,
  className,
  children,
  isMultiSelect = false,
  hasResetOption = true,
  resetOptionText = 'Сбросить все',
  onChange,
  onReset,
}) => {
  const providerValue = React.useMemo(
    () => ({
      isMultiSelect,
      state: value,
      setState: onChange,
    }),
    [value, onChange],
  );

  const selectedCount = React.useMemo(
    () => Object.values(value).filter((value) => value === true).length,
    [value],
  );

  return (
    <SelectContext.Provider value={providerValue}>
      <List className={clsx(styles.root, className)}>
        {children}
        {hasResetOption && selectedCount > 1 && (
          <>
            <Divider />
            <Action onClick={onReset}>{resetOptionText}</Action>
          </>
        )}
      </List>
    </SelectContext.Provider>
  );
};

Select.Option = Option;
Select.Action = Action;
