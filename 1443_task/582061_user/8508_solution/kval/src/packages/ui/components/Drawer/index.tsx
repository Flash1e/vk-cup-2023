import React from 'react';
import ReactDOM from 'react-dom';
import clsx from 'clsx';
import { IconClose } from 'icons';

import { usePopoverContainer } from '../../hooks';
import { Button } from '../Button';
import { Tooltip } from '../Tooltip';

import styles from './Drawer.module.scss';

export type DrawerProps = React.PropsWithChildren<{
  readonly isOpened: boolean;
  readonly onClose: () => void;
  readonly className?: string;
  readonly direction?: 'bottom' | 'left';
  readonly containerNode?: HTMLElement;
  readonly hasCloseButton?: boolean;
  readonly closeButtonText?: string;
  readonly onOpenChange?: (isOpened: boolean) => void;
}>;

export const Drawer: React.FC<DrawerProps> = ({
  isOpened,
  children,
  onClose,
  className,
  containerNode,
  direction = 'bottom',
  hasCloseButton = true,
  closeButtonText = 'Закрыть',
  onOpenChange,
}) => {
  const { container } = usePopoverContainer(isOpened, containerNode);

  React.useEffect(() => {
    if (onOpenChange) {
      onOpenChange(isOpened);
    }
  }, [isOpened, onOpenChange]);

  if (!container) {
    return null;
  }

  const content = (
    <>
      <div
        className={clsx(
          styles.root,
          styles[direction],
          isOpened ? styles.open : styles.close,
          className,
        )}
      >
        {children}
        {hasCloseButton && (
          <Tooltip overlay={closeButtonText} placement="left">
            <Button
              className={styles.root__close}
              variant="secondary"
              isIconOnly
              onClick={onClose}
            >
              <IconClose />
            </Button>
          </Tooltip>
        )}
      </div>
      {isOpened && (
        <div
          className={styles.root__overlay}
          onClick={onClose}
          onKeyDown={onClose}
          tabIndex={0}
          role="button"
          aria-label="overlay"
        />
      )}
    </>
  );

  return ReactDOM.createPortal(content, container);
};
