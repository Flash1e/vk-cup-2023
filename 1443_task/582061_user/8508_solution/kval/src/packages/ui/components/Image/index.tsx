import React from 'react';
import clsx from 'clsx';

import { Loader } from '../Loader';

import styles from './Image.module.scss';

export type ImageProps = {
  readonly alt: string;
  readonly src?: string;
  readonly display?: 'miniature' | 'wide';
  readonly className?: string;
  readonly width?: number | string;
  readonly height?: number | string;
};

export const Image: React.FC<ImageProps> = ({
  src,
  alt,
  className,
  width,
  height,
  display = 'wide',
}) => {
  const [isLoading, setIsLoading] = React.useState(true);

  const handleImageLoad = React.useCallback(() => {
    setIsLoading(false);
  }, []);

  return (
    <div
      className={clsx(styles.root, styles[display], className)}
      style={{ width, height }}
    >
      {isLoading && <Loader isStretched />}
      <img
        className={clsx(styles.root__image, !isLoading && styles.visible)}
        src={src}
        alt={alt}
        onLoad={handleImageLoad}
      />
    </div>
  );
};
