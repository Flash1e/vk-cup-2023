import clsx from 'clsx';
import React from 'react';

import { Popover, PopoverProps } from '../Popover';

import styles from './Tooltip.module.scss';

export type TooltipProps = Omit<PopoverProps, 'events' | 'trigger'>;

export const Tooltip: React.FC<TooltipProps> = ({ className, ...rest }) => (
  <Popover
    className={clsx(styles.root, className)}
    trigger="hover"
    placement="bottom"
    mouseEnterDelay={0.5}
    events="none"
    {...rest}
  />
);
