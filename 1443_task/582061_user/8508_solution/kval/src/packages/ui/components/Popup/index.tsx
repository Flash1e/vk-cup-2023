import clsx from 'clsx';
import React from 'react';

import { Popover, PopoverProps } from '../Popover';

import styles from './Popup.module.scss';

export type PopupProps = Omit<PopoverProps, 'events'>;

export const Popup: React.FC<PopupProps> = ({ className, ...rest }) => (
  <Popover {...rest} className={clsx(styles.root, className)} />
);
