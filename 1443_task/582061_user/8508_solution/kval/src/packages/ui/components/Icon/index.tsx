import React from 'react';
import clsx from 'clsx';

import styles from './Icon.module.scss';

export type IconProps = {
  readonly icon: React.ReactElement;
  readonly color?: 'accent' | 'secondary' | 'error';
  readonly className?: string;
};

export const Icon: React.FC<IconProps> = ({ color, icon, className }) => (
  <span className={clsx(styles.root, color && styles[color], className)}>
    {icon}
  </span>
);
