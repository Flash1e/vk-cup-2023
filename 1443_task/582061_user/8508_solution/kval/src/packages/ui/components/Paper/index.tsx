import React from 'react';
import clsx from 'clsx';

import styles from './Paper.module.scss';

export type PaperProps = React.PropsWithChildren<{
  readonly className?: string;
}>;

export const Paper: Utils.FCWithComponentProp<PaperProps, 'div'> = ({
  children,
  className,
  component: Component = 'div',
}) => (
  <Component className={clsx(styles.root, className)}>{children}</Component>
);
