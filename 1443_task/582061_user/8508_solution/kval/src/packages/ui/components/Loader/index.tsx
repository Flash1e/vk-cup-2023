import React from 'react';
import clsx from 'clsx';
import { IconLoader } from 'icons';

import styles from './Loader.module.scss';

export type LoaderProps = {
  readonly className?: string;
  readonly isStretched?: boolean;
};

export const Loader: React.FC<LoaderProps> = ({ className, isStretched }) => (
  <div className={clsx(className, styles.root, isStretched && styles.stretch)}>
    <IconLoader className={styles.root__icon} size="md" />
  </div>
);
