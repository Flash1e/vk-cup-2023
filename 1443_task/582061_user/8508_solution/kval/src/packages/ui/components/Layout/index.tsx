import React, { HTMLAttributes } from 'react';
import clsx from 'clsx';

import styles from './Layout.module.scss';

export type LayoutProps = React.PropsWithChildren<{
  readonly className?: string;
  readonly header?: React.ReactNode;
  readonly sidebar?: React.ReactNode;
}> &
  HTMLAttributes<HTMLDivElement>;

export const Layout: React.FC<LayoutProps> = ({
  className,
  header,
  sidebar,
  children,
  ...rest
}) => {
  const hasSidebar = Boolean(sidebar);

  return (
    <div
      {...rest}
      className={clsx(className, styles.root, hasSidebar && styles.has_sidebar)}
    >
      {header && <div className={styles.root__header}>{header}</div>}
      <div className={styles.root__inner}>
        {hasSidebar && <div className={styles.root__sidebar}>{sidebar}</div>}
        <div className={styles.root__content}>{children}</div>
      </div>
    </div>
  );
};
