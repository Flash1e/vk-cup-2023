import React, { InputHTMLAttributes, LabelHTMLAttributes } from 'react';
import clsx from 'clsx';
import { nanoid } from 'nanoid';

import { Checkmark } from './Checkmark';

import styles from './Checkbox.module.scss';

export type CheckboxProps = {
  readonly labelProps?: LabelHTMLAttributes<HTMLLabelElement>;
} & Omit<InputHTMLAttributes<HTMLInputElement>, 'type'>;

export const Checkbox: React.FC<CheckboxProps> = ({
  className,
  labelProps,
  id: idFromProps,
  ...rest
}) => {
  const id = React.useMemo(() => idFromProps || nanoid(), [idFromProps]);

  return (
    <label
      {...labelProps}
      htmlFor={id}
      className={clsx(className, styles.root, rest.checked && styles.checked)}
    >
      <input {...rest} id={id} type="checkbox" className={styles.root__input} />
      <div className={styles.root__border} />
      <Checkmark className={styles.root__checkmark} />
    </label>
  );
};
