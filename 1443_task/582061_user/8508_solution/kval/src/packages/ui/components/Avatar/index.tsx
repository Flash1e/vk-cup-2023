import React from 'react';
import clsx from 'clsx';

import styles from './Avatar.module.scss';

export type AvatarProps = {
  readonly src: string;
  readonly className?: string;
  readonly size?: 'md';
};

export const Avatar: React.FC<AvatarProps> = ({
  className,
  src,
  size = 'md',
}) => (
  <div
    className={clsx(className, styles.root, styles[size])}
    style={{ backgroundImage: `url("${src}")` }}
  />
);
