import React from 'react';

import { getWindowDimensions } from '../helpers';

export type UseSidebarCollapseProps = {
  readonly breakpoint?: number;
};

export const useSidebarCollapse = (params?: UseSidebarCollapseProps) => {
  const { breakpoint = 768 } = params || {};
  const [isCollapsed, setIsCollapsed] = React.useState(
    getWindowDimensions()[0] <= breakpoint,
  );

  React.useEffect(() => {
    const handleResize = () => {
      const shouldCollapse = getWindowDimensions()[0] <= breakpoint;

      setIsCollapsed((isCollapsed) => {
        if (isCollapsed !== shouldCollapse) {
          return shouldCollapse;
        }

        return isCollapsed;
      });
    };

    handleResize();
    window.addEventListener('resize', handleResize);

    return () => window.removeEventListener('resize', handleResize);
  }, [breakpoint]);

  return {
    isCollapsed,
    setIsCollapsed,
  };
};
