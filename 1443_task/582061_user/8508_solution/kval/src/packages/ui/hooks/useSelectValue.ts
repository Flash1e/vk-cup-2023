import React from 'react';

import { SelectValueState } from '../components/Select/SelectContext';

export type UseSelectValueParams = {
  readonly defaultValue?: SelectValueState;
  readonly onChange: (state: SelectValueState) => void;
};

export const useSelectValue = ({
  defaultValue = {},
  onChange,
}: UseSelectValueParams) => {
  const [value, setValue] = React.useState(defaultValue);

  const reset = React.useCallback(() => {
    setValue(defaultValue);
    onChange(defaultValue);
  }, [defaultValue, onChange]);

  const handleChange = React.useCallback(
    (value: SelectValueState) => {
      setValue(value);
      onChange(value);
    },
    [onChange],
  );

  return {
    value,
    reset,
    handleChange,
  };
};
