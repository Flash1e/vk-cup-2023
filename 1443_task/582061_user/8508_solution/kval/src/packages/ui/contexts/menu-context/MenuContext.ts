import React from 'react';

export type MenuContextState = {
  readonly isCollapsed: boolean;
};

export const MenuContext = React.createContext<MenuContextState>({
  isCollapsed: false,
});
