import React from 'react';
import { MenuContext } from './MenuContext';

export const useMenuContext = () => React.useContext(MenuContext);
