import React from 'react';

export type PopoverStackContextState = {
  readonly container: HTMLElement | null;
  readonly zIndex: number;
  readonly isOpened: boolean;
};

export const PopoverStackContext =
  React.createContext<PopoverStackContextState>({
    container: null,
    zIndex: 0,
    isOpened: false,
  });
