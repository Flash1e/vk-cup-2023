export {};

declare global {
  namespace Utils {
    type FCWithComponentProp<P = Record<string, never>, DC = unknown> = {
      <C extends React.ElementType = DC>(
        props: {
          component?: C;
        } & Omit<React.ComponentProps<C>, keyof P> &
          P,
        context?: any,
      ): JSX.Element | null;

      displayName?: string;
    };
  }
}
