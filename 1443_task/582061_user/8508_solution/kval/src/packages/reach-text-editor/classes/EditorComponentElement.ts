import { ElementAttributes, ElementType } from '../types';
import { EditorElement } from './Element';

export class EditorComponentElement extends EditorElement<ElementType.Component> {
  public constructor(
    public readonly key: string,
    attributes: ElementAttributes,
  ) {
    super(ElementType.Component, attributes);
  }
}
