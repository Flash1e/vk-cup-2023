import { ComponentView, AtomView, ElementView, ElementType } from '../types';
import {
  IEditorRenderer,
  IEditorElement,
  IEditorComponentElement,
} from '../interfaces';

function isComponentElement(
  element: IEditorElement,
): element is IEditorComponentElement {
  if (element.type === ElementType.Component) {
    return true;
  }

  return false;
}

export class EditorRenderer implements IEditorRenderer {
  private atomView: AtomView;
  private componentViews: { [key: string]: ComponentView };

  public constructor() {
    this.componentViews = {};
  }

  public includeComponentView(key: string, render: ComponentView): void {
    this.componentViews[key] = render;
  }

  public setAtomView(view: AtomView): void {
    this.atomView = view;
  }

  public getElementView(element: IEditorElement): ElementView | undefined {
    if (isComponentElement(element)) {
      return this.componentViews[element.key] as ElementView;
    }

    return this.atomView as ElementView;
  }
}
