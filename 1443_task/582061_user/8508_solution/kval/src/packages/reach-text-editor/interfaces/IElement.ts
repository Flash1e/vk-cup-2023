import { ISubscribable } from 'pubsub';

import {
  ElementAttributes,
  ElementEvent,
  ElementEventHandlerParams,
  ElementType,
} from '../types';

export interface IEditorElement<T extends ElementType = ElementType>
  extends ISubscribable<ElementEvent, ElementEventHandlerParams> {
  readonly type: T;
  readonly attributes: ElementAttributes;

  focus(): void;

  canMerge(element: IEditorElement): boolean;

  merge(element: IEditorElement): boolean;
}

export interface IEditorComponentElement
  extends IEditorElement<ElementType.Component> {
  readonly key: string;
}

export interface IEditorAtomElement extends IEditorElement<ElementType.Atom> {
  readonly children: (string | IEditorAtomElement)[];
}
