import { IEditorElement } from './IElement';
import { ElementView, AtomView, ComponentView } from '../types';

export interface IEditorRenderer {
  includeComponentView(key: string, render: ComponentView): void;

  setAtomView(view: AtomView): void;

  getElementView(element: IEditorElement): ElementView | undefined;
}
