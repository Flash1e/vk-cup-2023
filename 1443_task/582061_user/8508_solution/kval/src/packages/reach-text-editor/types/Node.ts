import { IEditorElement } from 'src/packages/reach-text-editor/interfaces';

export enum NodeType {
  Text = 'TEXT',
  Composite = 'COMPOSITE',
}

export enum ElementType {
  Atom = 'ATOM',
  Component = 'COMPONENT',
}

export type ElementAttributes = Record<string, any>;

export type Node<T extends IEditorElement = IEditorElement> = {
  readonly id: string;
  readonly element: T;
};
