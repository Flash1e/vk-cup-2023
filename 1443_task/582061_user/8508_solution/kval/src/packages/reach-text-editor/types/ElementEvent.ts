export enum ElementEvent {
  Focus = 'focus',
  Render = 'render',
  Merge = 'merge',
}
