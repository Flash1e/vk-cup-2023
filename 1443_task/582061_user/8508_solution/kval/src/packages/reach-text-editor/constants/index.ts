import { EditorRenderer } from '../classes';
import { EditorAtomView } from '../components/EditorAtomView';

export const DEFAULT_RENDERER = new EditorRenderer();

DEFAULT_RENDERER.setAtomView(EditorAtomView);
