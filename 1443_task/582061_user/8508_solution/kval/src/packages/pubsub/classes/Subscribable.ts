import { Event, Handler, HandlerParams } from '../types';
import { ISubscribable } from '../interfaces';

export class Subscribable<E extends Event, P extends HandlerParams<E>>
  implements ISubscribable<E, P>
{
  public constructor(
    protected readonly handlers: {
      [T in E]: Handler<T, P>[];
    },
  ) {}

  public subscribe<T extends E>(event: T, handler: Handler<T, P>): () => void {
    const index = this.handlers[event].findIndex((h) => h === handler);

    if (index !== -1) {
      return () => {};
    }

    this.handlers[event].push(handler);

    return () => this.unsubscribe(event, handler);
  }

  public unsubscribe<T extends E>(event: T, handler: Handler<T, P>): void {
    const index = this.handlers[event].findIndex((h) => h === handler);

    if (index === -1) {
      return;
    }

    this.handlers[event].splice(index, 1);
  }

  public dispose(): void {
    Object.keys(this.handlers).forEach((key) => {
      this.handlers[key as keyof typeof this.handlers] = [];
    });
  }

  protected publish<T extends E>(event: T, ...params: P[T]): void {
    const handlers = this.handlers[event];

    handlers.forEach((handler) => handler(...params));
  }
}
