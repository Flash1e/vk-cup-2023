import { DateInput } from '../types';

export function isCurrentYear(input: DateInput): boolean {
  const date = new Date(input);
  const currentDate = new Date();

  if (date.getFullYear() === currentDate.getFullYear()) {
    return true;
  }

  return false;
}
