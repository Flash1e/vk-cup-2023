import { DateInput, FormatPattern, FormatPatternRegistry } from '../types';

export class Formatter {
  private readonly registry: FormatPatternRegistry;
  private matchOrder: string[];

  public constructor(patterns: FormatPattern[], private locale = 'ru') {
    this.format = this.format.bind(this);
    this.setLocale = this.setLocale.bind(this);

    this.registry = {};
    this.matchOrder = [];

    this.updateRegistry(patterns);
  }

  public format(
    input: DateInput,
    pattern: string,
    locale = this.locale,
  ): string {
    const date = new Date(input);

    const matches = pattern.match(
      new RegExp(`(${this.matchOrder.join('|')})`, 'g'),
    );

    if (!matches) {
      return pattern;
    }

    let formatted = pattern;

    for (let i = 0; i < matches.length; i += 1) {
      const match = matches[i];
      const replacer = this.registry[match]?.replacer;

      if (replacer) {
        formatted = formatted.replace(match, replacer(date, locale));
      }
    }

    return formatted;
  }

  public setLocale(locale: string): void {
    this.locale = locale;
  }

  private updateRegistry(patterns: FormatPattern[]): void {
    for (const pattern of patterns) {
      this.registry[pattern.match] = pattern;
    }

    this.reorderPatterns();
  }

  private reorderPatterns(): void {
    const patterns = Object.values(this.registry);

    this.matchOrder = patterns
      .sort((a, b) => b.match.length - a.match.length)
      .map(({ match }) => match);
  }
}
