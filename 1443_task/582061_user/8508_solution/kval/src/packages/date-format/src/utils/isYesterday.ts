import { MS_DAY } from '../constants';
import { DateInput } from '../types';
import { subtract } from './subtract';
import { isDateSame } from './isDateSame';

export function isYesterday(input: DateInput): boolean {
  return isDateSame(input, subtract(new Date(), MS_DAY));
}
