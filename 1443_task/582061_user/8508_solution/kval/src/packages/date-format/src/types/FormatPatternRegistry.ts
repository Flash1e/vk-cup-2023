import { FormatPattern } from './FormatPattern';

export type FormatPatternRegistry = {
  [pattern: string]: FormatPattern;
};
