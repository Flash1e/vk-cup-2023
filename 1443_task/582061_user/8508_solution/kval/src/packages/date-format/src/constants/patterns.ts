import { createFormatPattern } from '../utils/createFormatPattern';

// Time patterns
export const MINUTE_DIG_PATTERN = createFormatPattern({
  match: 'mm',
  replacer: (date, locale) => {
    const [, minute] = date
      .toLocaleString(locale, {
        hour: '2-digit',
        minute: '2-digit',
        hour12: false,
      })
      .split(':');

    return minute;
  },
});

export const HOUR_DIG_PATTERN = createFormatPattern({
  match: 'hh',
  replacer: (date, locale) => {
    const [hour] = date
      .toLocaleString(locale, {
        hour: '2-digit',
        minute: '2-digit',
        hour12: false,
      })
      .split(':');

    return hour;
  },
});

export const HOUR_MINUTE_DIG_PATTERN = createFormatPattern({
  match: 'hh:mm',
  replacer: (date, locale) =>
    date.toLocaleString(locale, {
      hour: '2-digit',
      minute: '2-digit',
      hour12: false,
    }),
});

// Date patterns
export const DAY_OF_MONTH_DIG_PATTERN = createFormatPattern({
  match: 'DD',
  replacer: (date, locale) => date.toLocaleString(locale, { day: '2-digit' }),
});

export const MONTH_DIG_PATTERN = createFormatPattern({
  match: 'MM',
  replacer: (date, locale) => date.toLocaleString(locale, { month: '2-digit' }),
});

export const MONTH_SHORT_PATTERN = createFormatPattern({
  match: 'MMM',
  replacer: (date, locale) => {
    const month = date.toLocaleString(locale, { month: 'short' });

    if (month[month.length - 1] === '.') {
      return month.slice(0, month.length - 1);
    }

    return month;
  },
});

export const MONTH_LONG_PATTERN = createFormatPattern({
  match: 'MMMM',
  replacer: (date, locale) => {
    const monthWithDay = date.toLocaleString(locale, {
      day: '2-digit',
      month: 'long',
    });

    return monthWithDay.split(' ')[1];
  },
});

export const YEAR_LONG_PATTERN = createFormatPattern({
  match: 'YYYY',
  replacer: (date, locale) => date.toLocaleString(locale, { year: 'numeric' }),
});

export const DATE_SHORT_PATTERN = createFormatPattern({
  match: 'DD.MM.YYYY',
  replacer: (date, locale) => date.toLocaleDateString(locale),
});
