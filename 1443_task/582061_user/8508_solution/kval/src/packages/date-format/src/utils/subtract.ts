import { DateInput } from '../types';

export function subtract(input: DateInput, time: number): Date {
  const date = new Date(input);

  return new Date(date.getTime() - time);
}
