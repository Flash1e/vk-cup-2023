import { PatternReplacer } from './PatternReplacer';

export type FormatPattern = {
  readonly match: string;
  readonly replacer: PatternReplacer;
};
