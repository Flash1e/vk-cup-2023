import { Mutable } from 'utility-types';
import { Subscribable } from 'pubsub';
import { getNodeInsets } from 'dom-utils';
import {
  IScrollController,
  ScrollController,
  ScrollEvent,
} from 'scroll-controller';

import { IVirtualListController } from '../interfaces';
import {
  VirtualListOptions,
  ViewportItem,
  VirtualListEvent,
  VirtualListHandlerParams,
} from '../types';

export class VirtualListController
  extends Subscribable<VirtualListEvent, VirtualListHandlerParams>
  implements IVirtualListController
{
  private readonly scrollController: ScrollController;

  private options: Mutable<VirtualListOptions> | null;
  private viewportItemsCount: number;
  private viewportItems: ViewportItem[];

  public constructor() {
    super({
      [VirtualListEvent.OnScroll]: [],
      [VirtualListEvent.OnEnd]: [],
    });

    this.updateViewportItems = this.updateViewportItems.bind(this);

    this.scrollController = new ScrollController();
    this.scrollController.subscribe(
      ScrollEvent.Scroll,
      this.updateViewportItems,
    );

    this.options = null;
    this.viewportItemsCount = 0;
    this.viewportItems = [];
  }

  get scroll(): IScrollController {
    return this.scrollController;
  }

  public init(options: VirtualListOptions): void {
    this.options = {
      ...options,
    };

    this.scrollController.attach(this.options.viewportNode);
    this.sync();
  }

  public setItemsCount(value: number): void {
    if (!this.options) {
      return;
    }

    this.options.itemsCount = value;
    this.sync();
  }

  public dispose(): void {
    super.dispose();
    this.scrollController.dispose();
  }

  /**
   * Synchronizes virtual list nodes and properties with current state
   */
  public sync(): void {
    this.syncViewport();
    this.syncContent();
  }

  /**
   * Synchronizes viewport with current list state
   * @returns
   */
  private syncViewport(): void {
    if (!this.options) {
      return;
    }

    const { viewportNode, height, itemHeight } = this.options;

    if (itemHeight === 0) {
      throw new Error('itemHeight could not be 0!');
    }

    this.viewportItemsCount = Math.ceil(height / itemHeight);

    viewportNode.style.overflow = 'auto';
    viewportNode.style.position = 'relative';
    viewportNode.style.height = `${height}px`;

    this.updateViewportItems();
  }

  /**
   * Synchronize content with current list state
   * @returns
   */
  private syncContent(): void {
    if (!this.options) {
      return;
    }

    const { contentNode, itemsCount, itemHeight } = this.options;

    const insets = getNodeInsets(contentNode);
    const contentVerticalInsets = insets.top + insets.bottom;

    const height = itemsCount * itemHeight + contentVerticalInsets;

    contentNode.style.height = `${height}px`;
  }

  /**
   * Updates viewport items
   * @returns
   */
  private updateViewportItems(): void {
    if (!this.options) {
      return;
    }

    const { viewportNode, itemHeight, batchSize } = this.options;

    const viewportInsets = getNodeInsets(viewportNode);
    const viewportVerticalInsets = viewportInsets.top + viewportInsets.bottom;

    const halfBatchSize = Math.floor(batchSize / 2);
    const scrollTop = this.scrollController.scrollTop - viewportVerticalInsets;
    const fromIndex = Math.floor(scrollTop / itemHeight) - halfBatchSize;

    this.viewportItems = [];

    for (let i = 0; i < this.viewportItemsCount + batchSize; i += 1) {
      const index = fromIndex + i;

      this.viewportItems.push({
        index,
        offsetTop: index * itemHeight,
      });
    }

    this.publish(VirtualListEvent.OnScroll, this.viewportItems);
  }
}
