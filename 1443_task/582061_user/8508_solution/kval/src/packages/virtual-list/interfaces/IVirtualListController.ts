import { ISubscribable } from 'pubsub';
import { IScrollController } from 'scroll-controller';

import {
  VirtualListEvent,
  VirtualListHandlerParams,
  VirtualListOptions,
} from '../types';

export interface IVirtualListController
  extends ISubscribable<VirtualListEvent, VirtualListHandlerParams> {
  get scroll(): IScrollController;

  init(options: VirtualListOptions): void;

  setItemsCount(value: number): void;

  sync(): void;

  dispose(): void;
}
