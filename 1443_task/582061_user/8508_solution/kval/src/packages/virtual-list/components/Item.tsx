import React from 'react';

export type ItemProps = React.PropsWithChildren<{
  readonly top: number;
  // eslint-disable-next-line react/no-unused-prop-types
  readonly index: number;
  // eslint-disable-next-line react/no-unused-prop-types
  readonly id?: number | string;
}>;

const Component: React.FC<ItemProps> = ({ top, children }) => (
  <div
    style={{
      top,
      right: 0,
      left: 0,
      position: 'absolute',
    }}
  >
    {children}
  </div>
);

export const Item = React.memo(
  Component,
  (prevProps, nextProps) =>
    prevProps.index === nextProps.index && prevProps.id === nextProps.id,
);
