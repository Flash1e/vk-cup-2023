import { VirtualListEvent } from './VirtualListEvent';
import { ViewportItem } from './ViewportItem';

export type VirtualListHandlerParams = {
  [VirtualListEvent.OnScroll]: [items: ViewportItem[]];
  [VirtualListEvent.OnEnd]: [];
};
