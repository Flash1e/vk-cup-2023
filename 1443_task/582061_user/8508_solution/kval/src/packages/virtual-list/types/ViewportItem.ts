export type ViewportItem = {
  readonly index: number;
  readonly offsetTop: number;
};
