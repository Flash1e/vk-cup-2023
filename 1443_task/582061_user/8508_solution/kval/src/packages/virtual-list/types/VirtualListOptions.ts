export type VirtualListOptions = {
  readonly height: number;
  readonly viewportNode: HTMLElement;
  readonly contentNode: HTMLElement;
  readonly itemsCount: number;
  readonly itemHeight: number;
  readonly batchSize: number;
};
