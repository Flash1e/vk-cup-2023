import { Letter, LetterFlag } from 'types';

export enum LetterExtraCategory {
  Attachment = 'attachment',
}
export type LetterCategory = LetterFlag | LetterExtraCategory;

export type LetterCategoriesConfig = {
  readonly flag?: Letter['flag'];
  readonly hasAttachment: boolean;
};

export function getLetterCategories({
  flag,
  hasAttachment,
}: LetterCategoriesConfig): LetterCategory[] {
  const categories: LetterCategory[] = [];

  if (flag) {
    categories.push(flag);
  }

  if (hasAttachment) {
    categories.push(LetterExtraCategory.Attachment);
  }

  return categories;
}
