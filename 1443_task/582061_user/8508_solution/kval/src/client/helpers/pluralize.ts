export function pluralize(
  count: number,
  forms: [string, string, string],
): string {
  const remainder = count % 10;

  if (count % 100 >= 10 && count % 100 <= 20) {
    return forms[2];
  }

  if (remainder === 1) {
    return forms[0];
  }

  if (remainder >= 2 && remainder <= 4) {
    return forms[1];
  }

  return forms[2];
}
