import { LetterListItem } from 'types';
import { API_URL } from './API_URL';

// TODO: move to client types
export type FolderFilters = {
  readonly unread: boolean;
  readonly bookmark: boolean;
  readonly attachment: boolean;
};

export type FetchLettersParams = {
  readonly folder: string;
  readonly page: number;
  readonly limit: number;
  readonly filters: FolderFilters;
  readonly sort: string;
};

export async function fetchLetters(
  params: FetchLettersParams,
): Promise<{ letters: LetterListItem[]; count: number }> {
  const { folder, page, limit, filters, sort } = params;

  // TODO: move to base Fetcher class
  const filter =
    Object.entries(filters).reduce(
      (acc, [key, value]) => (value ? `${acc},${key}` : acc),
      '',
    ) || null;

  const query = Object.entries({
    page,
    limit,
    filter,
    sort,
  }).reduce((acc, [key, value]) => {
    if ((typeof value !== 'string' && typeof value !== 'number') || !value) {
      return acc;
    }

    const prefix = acc === '' ? '?' : `${acc}&`;

    return `${prefix}${key}=${encodeURIComponent(value)}`;
  }, '');

  const response = await fetch(`${API_URL}/api/letters/${folder}${query}`);
  const json = await response.json();

  return json.data;
}
