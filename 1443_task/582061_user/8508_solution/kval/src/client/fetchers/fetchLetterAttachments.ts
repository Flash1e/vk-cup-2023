import { Letter, LetterAttachment } from 'types';
import { API_URL } from './API_URL';

export type FetchLetterAttachmentsParams = {
  readonly id: Letter['id'];
  readonly folder: string;
};

export async function fetchLetterAttachments(
  params: FetchLetterAttachmentsParams,
): Promise<{ attachments: LetterAttachment[] }> {
  const { id, folder } = params;

  const response = await fetch(
    `${API_URL}/api/letters/${folder}/${id}/attachments`,
  );
  const json = await response.json();

  return json.data;
}
