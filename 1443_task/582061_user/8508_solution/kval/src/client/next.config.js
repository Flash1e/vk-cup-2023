const withPreact = require('next-plugin-preact');
const withTM = require('next-transpile-modules')([
  'types',
  'ui',
  'icons',
  'i18n',
  'pubsub',
  'dom-utils',
  'date-format',
  'scroll-controller',
  'virtual-list',
  'trigger',
]);
const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
});

module.exports = withPreact(
  withBundleAnalyzer(
    withTM({
      webpack: (config, { isServer }) => {
        if (!isServer) {
          config.resolve.alias = {
            ...config.resolve.alias,
            'create-react-class': 'preact-compat/lib/create-react-class',
          };
        }

        return config;
      },
    }),
  ),
);
