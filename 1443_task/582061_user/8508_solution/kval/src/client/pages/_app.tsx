import React from 'react';
import { AppProps } from 'next/app';
import { BrowserRouter } from 'react-router-dom';
import { StaticRouter } from 'react-router-dom/server';
import { ThemeProvider } from 'ui';
import { I18n, I18nEvent, I18nNextProps, useTranslation } from 'i18n';
import { FORMATTER } from 'date-format';
import Cookies from 'js-cookie';
import Head from 'next/head';

import { THEMES } from '../themes';

import 'ui/styles/global.scss';

I18n.init();
I18n.Instance.subscribe(I18nEvent.Init, (lang) => FORMATTER.setLocale(lang));
I18n.Instance.subscribe(I18nEvent.LanguageChange, (lang) => {
  Cookies.set('lang', lang);
  window.location.reload();
});

function App({ Component, pageProps }: AppProps<I18nNextProps>) {
  const [isSSR, setIsSSR] = React.useState(true);
  const { t, i18n } = useTranslation();

  React.useEffect(() => {
    setIsSSR(false);
  }, []);

  const Wrapper = isSSR ? StaticRouter : BrowserRouter;

  if (pageProps.i18nResource) {
    i18n.load(pageProps.lang, pageProps.i18nResource);
  }

  return (
    <>
      <Head>
        <title>{t('title')}</title>
        <meta name="viewport" content="initial-scale=1, viewport-fit=cover" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta
          name="apple-mobile-web-app-status-bar-style"
          content="black-translucent"
        />
      </Head>
      <main suppressHydrationWarning>
        <Wrapper location="/">
          <ThemeProvider customThemes={[...THEMES.colors, ...THEMES.images]}>
            <Component {...pageProps} />
          </ThemeProvider>
        </Wrapper>
      </main>
    </>
  );
}

export default App;
