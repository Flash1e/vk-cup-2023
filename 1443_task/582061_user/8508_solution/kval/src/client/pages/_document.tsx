import React from 'react';
import { Html, Head, Main, NextScript } from 'next/document';

export default function Document() {
  return (
    <Html className="theme-light">
      <Head>
        <link
          nonce=""
          rel="icon"
          sizes="16x16"
          href="/static/favicons/16-fav_mail.png"
        />
        <link
          nonce=""
          rel="icon"
          sizes="24x24"
          href="/static/favicons/24-fav_mail.png"
        />
        <link
          nonce=""
          rel="icon"
          sizes="32x32"
          href="/static/favicons/32-fav_mail.png"
        />
      </Head>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
