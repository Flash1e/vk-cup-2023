import React from 'react';
import { FolderContext } from 'src/client/contexts/FolderContext';
import { useFolderApi } from 'src/client/hooks/useFolderApi';

export type UseFolderParams = {
  readonly folder: string;
  readonly limit?: number;
};

export const useFolder = ({ folder, limit = 50 }: UseFolderParams) => {
  const {
    entities,
    isLoading,
    folderRef,
    pageRef,
    countRef,
    setEntities,
    setIsLoading,
  } = React.useContext(FolderContext);

  const api = useFolderApi();

  const getLetters = React.useCallback(async (page: number, limit: number) => {
    setIsLoading(true);
    const letters = await api.getLetters(page, limit);
    setIsLoading(false);

    return letters;
  }, []);

  const getPortion = React.useCallback(async () => {
    if (isLoading || entities.length >= countRef.current) {
      return;
    }

    const page = pageRef.current + 1;
    const letters = await getLetters(page, limit);

    setEntities((current) => [...current, ...letters]);
  }, [isLoading, entities, limit, getLetters]);

  const refresh = React.useCallback(async () => {
    setEntities([]);
    setEntities(await getLetters(0, limit));
  }, [folder, limit, getLetters]);

  React.useEffect(() => {
    if (!isLoading && folder !== folderRef.current) {
      folderRef.current = folder;
      refresh();
    }
  }, [isLoading, folder, refresh]);

  return {
    isLoading,
    entities,
    hasMore: countRef.current > entities.length,
    getPortion,
    refresh,
  };
};
