import React from 'react';
import { LetterFlag } from 'types';
import {
  IconAttachment,
  IconCart,
  IconGovernment,
  IconKey,
  IconMoney,
  IconPlane,
  IconTicket,
} from 'icons';

import {
  getLetterCategories,
  LetterExtraCategory,
  LetterCategoriesConfig,
} from '../helpers';

export const CategoryIconMap = {
  [LetterFlag.Orders]: IconCart,
  [LetterFlag.Finance]: IconMoney,
  [LetterFlag.Accounts]: IconKey,
  [LetterFlag.Travel]: IconPlane,
  [LetterFlag.Ticket]: IconTicket,
  [LetterFlag.Government]: IconGovernment,
  [LetterExtraCategory.Attachment]: IconAttachment,
};

export const useLetterCategories = (config: LetterCategoriesConfig) =>
  React.useMemo(
    () =>
      getLetterCategories(config)
        .map((key) => ({
          key,
          Icon: CategoryIconMap[key],
        }))
        .filter(({ Icon }) => typeof Icon === 'function'),
    [config.flag, config.hasAttachment],
  );
