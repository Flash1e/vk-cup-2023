import React from 'react';
import {
  IconArchive,
  IconDraft,
  IconFolder,
  IconLetter,
  IconSent,
  IconSpam,
  IconTrash,
} from 'icons';
import { Folder } from 'types';

export const useFolders = () =>
  React.useMemo(
    () => [
      {
        key: Folder.Inbox,
        icon: <IconLetter />,
      },
      {
        key: Folder.Important,
        icon: <IconFolder />,
      },
      {
        key: Folder.Sent,
        icon: <IconSent />,
      },
      {
        key: Folder.Drafts,
        icon: <IconDraft />,
      },
      {
        key: Folder.Archive,
        icon: <IconArchive />,
      },
      {
        key: Folder.Spam,
        icon: <IconSpam />,
      },
      {
        key: Folder.Trash,
        icon: <IconTrash />,
      },
    ],
    [],
  );
