import React from 'react';
import { Drawer } from 'ui';

import { Settings } from '../Settings';

import styles from './SettingsDrawer.module.scss';

export type SettingsDrawerProps = {
  readonly isOpened: boolean;
  readonly onClose: () => void;
};

export const SettingsDrawer: React.FC<SettingsDrawerProps> = ({
  isOpened,
  onClose,
}) => {
  const handleOpenChange = React.useCallback((isOpened: boolean) => {
    const node = document.getElementById('__next');

    if (!node) {
      return;
    }

    node.style.transform = isOpened ? 'scale(0.8)' : 'scale(1)';
  }, []);

  return (
    <Drawer
      isOpened={isOpened}
      direction="bottom"
      className={styles.root}
      onClose={onClose}
      onOpenChange={handleOpenChange}
    >
      <Settings className={styles.root__settings} />
    </Drawer>
  );
};
