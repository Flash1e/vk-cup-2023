import React from 'react';
import clsx from 'clsx';
import { useLocation, useNavigate } from 'react-router-dom';
import {
  Drawer,
  Menu,
  MenuItem,
  Sidebar,
  useModal,
  useSidebarCollapse,
} from 'ui';
import { IconMenu, IconSettings } from 'icons';
import { useTranslation } from 'i18n';
import { getDocumentSafe } from 'dom-utils';

import { LetterMenuItem } from './partials/LetterMenuItem';
import { DrawerMenuItem } from './partials/DrawerMenuItem';
import { AddFolderMenuItem } from './partials/AddFolderMenuItem';
import { SettingsDrawer } from '../SettingsDrawer';

import styles from './AppSidebar.module.scss';

// i18n
export type AppSidebarProps = {
  readonly options: {
    readonly path: string;
    readonly title: string;
    readonly icon: React.ReactNode;
  }[];
};

export const AppSidebar: React.FC<AppSidebarProps> = ({ options }) => {
  const navigate = useNavigate();
  const location = useLocation();
  const settings = useModal();
  const menu = useModal();
  const { t } = useTranslation();
  const { isCollapsed } = useSidebarCollapse();

  return (
    <Sidebar
      className={clsx(styles.root, isCollapsed && styles.collapsed)}
      data-theme-contrast="true"
    >
      <Menu isCollapsed={isCollapsed}>
        {/* TODO: move options to component, to reuse it  */}
        <LetterMenuItem isCollapsed={isCollapsed} />
        <DrawerMenuItem isCollapsed={isCollapsed} onClick={menu.open} />
        {options.map(({ path, title, icon }) => (
          <MenuItem
            key={path}
            title={title}
            icon={icon}
            isCollapsed={isCollapsed}
            isActive={location.pathname.startsWith(path)}
            onClick={() => navigate(path)}
          />
        ))}
        <AddFolderMenuItem isCollapsed={isCollapsed} />
        <MenuItem
          isCollapsed={isCollapsed}
          className={styles.root__theme}
          title={t('component.settings.name')}
          onClick={settings.open}
          icon={<IconSettings />}
          hasTooltip={false}
        />
        <SettingsDrawer {...settings.props} />

        {/* TODO: move to component */}
        <Drawer
          {...menu.props}
          direction="left"
          className={styles.root__menu}
          hasCloseButton={false}
          containerNode={
            getDocumentSafe()?.getElementById('__next') || undefined
          }
        >
          <LetterMenuItem />
          <MenuItem
            icon={<IconMenu />}
            title={t('action.hide')}
            onClick={menu.close}
          />
          {options.map(({ path, title, icon }) => (
            <MenuItem
              key={path}
              title={title}
              icon={icon}
              isActive={location.pathname.startsWith(path)}
              onClick={() => navigate(path)}
            />
          ))}
          <AddFolderMenuItem />
          <MenuItem
            className={styles.root__theme}
            title={t('component.settings.name')}
            onClick={settings.open}
            icon={<IconSettings />}
            hasTooltip={false}
          />
        </Drawer>
      </Menu>
    </Sidebar>
  );
};
