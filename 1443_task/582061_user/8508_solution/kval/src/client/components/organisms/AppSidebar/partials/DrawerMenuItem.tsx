import clsx from 'clsx';
import React from 'react';
import { MenuItem, MenuItemProps } from 'ui';
import { IconMenu } from 'icons';
import { useTranslation } from 'i18n';

import styles from './DrawerMenuItem.module.scss';

export type DrawerMenuItemProps = Pick<
  MenuItemProps,
  'isCollapsed' | 'onClick'
>;

export const DrawerMenuItem: React.FC<DrawerMenuItemProps> = ({
  isCollapsed,
  onClick,
}) => {
  const { t } = useTranslation();

  return (
    <div className={clsx(styles.root, isCollapsed && styles.visible)}>
      <MenuItem
        title={t('component.menu.name')}
        icon={<IconMenu />}
        isCollapsed={isCollapsed}
        onClick={onClick}
      />
    </div>
  );
};
