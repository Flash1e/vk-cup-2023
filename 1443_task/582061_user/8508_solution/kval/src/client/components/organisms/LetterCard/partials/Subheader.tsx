import React from 'react';
import clsx from 'clsx';
import { Letter } from 'types';
import { Avatar, Icon, IconProps, Space, Typography } from 'ui';
import { IconBookmarkFilled, IconImportant } from 'icons';

import { formatLetterDateDetailed } from 'src/client/helpers';
import { Addressees, ReadButton } from 'src/client/components';

import styles from './Subheader.module.scss';

export type SubheaderProps = {
  readonly author: Letter['author'];
  readonly date: Letter['date'];
  readonly bookmark: boolean;
  readonly important: boolean;
  readonly read: boolean;
  readonly to: Letter['to'];
  readonly className?: string;
};

export const Subheader: React.FC<SubheaderProps> = ({
  author,
  date,
  bookmark,
  important,
  read,
  to,
  className,
}) => {
  const humanizedDate = React.useMemo(
    () => formatLetterDateDetailed(date),
    [date],
  );

  const icons = React.useMemo(() => {
    const icons = [
      {
        key: 'important',
        isActive: important,
        icon: IconImportant,
        color: 'error' as IconProps['color'],
      },
      {
        key: 'bookmark',
        isActive: bookmark,
        icon: IconBookmarkFilled,
        color: 'error' as IconProps['color'],
      },
    ];

    return icons;
  }, [important, bookmark]);

  return (
    <div className={clsx(styles.root, className)}>
      <ReadButton className={styles.root__read} isRead={read} />
      <Avatar className={styles.root__avatar} src={author.avatar} />
      <div className={styles.root__details}>
        <Space align="flex-end">
          <Typography preset="body">
            {author.name} {author.surname}
          </Typography>
          <Typography preset="footnote" color="secondary">
            {humanizedDate}
          </Typography>
          {icons.map(({ key, isActive, color, icon: IconSrc }) =>
            isActive ? (
              <Icon
                key={key}
                color={color}
                icon={<IconSrc width="20px" height="20px" />}
              />
            ) : null,
          )}
        </Space>
        <Addressees to={to} />
      </div>
    </div>
  );
};
