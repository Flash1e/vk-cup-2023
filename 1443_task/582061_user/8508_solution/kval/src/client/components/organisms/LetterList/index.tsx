import React from 'react';
import clsx from 'clsx';
import { LetterListItem as LetterListItemType } from 'types';
import { VirtualList } from 'virtual-list';
import { Loader } from 'ui';
import { getWindowDimensions } from 'dom-utils';

import { useFolder } from 'src/client/hooks';
import { LetterListItem } from '../../molecules/LetterListItem';
import { EmptySpace } from '../../atoms';

import styles from './LetterList.module.scss';

export type LetterListProps = {
  readonly folder: string;
};

export const LetterList: React.FC<LetterListProps> = ({ folder }) => {
  const letters = useFolder({ folder });

  const renderLetter = React.useCallback(
    (letter: LetterListItemType) => (
      <LetterListItem key={letter.id} {...letter} folder={folder} />
    ),
    [],
  );

  if (!letters.isLoading && letters.entities.length === 0) {
    return <EmptySpace />;
  }

  if (letters.isLoading && letters.entities.length === 0) {
    return <Loader />;
  }

  return (
    <VirtualList
      height={(getWindowDimensions()[1] || 800) - 56}
      itemHeight={48}
      batchSize={16}
      items={letters.entities}
      renderItem={renderLetter}
      viewportClassName={styles.root}
      contentClassName={clsx(
        styles.root__content,
        letters.hasMore && styles.has_loader,
      )}
      contentBottomSlot={
        letters.hasMore ? (
          <Loader className={clsx(styles.root__loader)} />
        ) : null
      }
      onScrollEnd={letters.getPortion}
    />
  );
};
