import clsx from 'clsx';
import React from 'react';
import { Divider, MenuItem, MenuItemProps } from 'ui';
import { IconPlus } from 'icons';
import { useTranslation } from 'i18n';

import styles from './AddFolderMenuItem.module.scss';

export type AddFolderMenuItemProps = Pick<
  MenuItemProps,
  'isCollapsed' | 'onClick'
>;

export const AddFolderMenuItem: React.FC<AddFolderMenuItemProps> = ({
  isCollapsed,
  onClick,
}) => {
  const { t } = useTranslation();

  return (
    <>
      <Divider className={clsx(styles.root, isCollapsed && styles.hidden)} />
      <MenuItem
        className={clsx(styles.root, isCollapsed && styles.hidden)}
        title={t('domain.action.new_folder')}
        variant="ghost"
        icon={<IconPlus />}
        isCollapsed={isCollapsed}
        onClick={onClick}
        hasTooltip={false}
      />
    </>
  );
};
