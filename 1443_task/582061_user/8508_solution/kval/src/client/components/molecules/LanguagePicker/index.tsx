import React from 'react';
import { Language } from 'types';
import { Button, Radio, Typography } from 'ui';
import { useTranslation } from 'i18n';

import { LanguageFlag } from '../../atoms';

import styles from './LanguagePicker.module.scss';

const LANGUAGES = [Language.Ru, Language.En];

export type LanguagePickerProps = {
  readonly className?: string;
};

export const LanguagePicker: React.FC<LanguagePickerProps> = () => {
  const { t, i18n } = useTranslation();
  const [language, setLanguage] = React.useState(i18n.getLanguage());

  const handleChange = React.useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) =>
      setLanguage(event.target.value as Language),
    [],
  );

  const handleSubmit = React.useCallback(
    () => i18n.changeLanguage(language),
    [language],
  );

  return (
    <div className={styles.root}>
      <Typography className={styles.root__title} preset="body" component="p">
        {t('component.language.title')}
      </Typography>
      <div className={styles.root__languages}>
        {LANGUAGES.map((language) => (
          <Radio
            key={language}
            name="language"
            value={language}
            onChange={handleChange}
            defaultChecked={language === i18n.getLanguage()}
          >
            <LanguageFlag className={styles.root__flag} language={language} />
            <Typography preset="body">
              {i18n.getLanguageName(language)}
            </Typography>
          </Radio>
        ))}
      </div>
      <Button variant="primary" onClick={handleSubmit}>
        {t('action.set_language')}
      </Button>
    </div>
  );
};
