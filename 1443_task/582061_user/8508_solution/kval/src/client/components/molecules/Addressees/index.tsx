import React from 'react';
import clsx from 'clsx';
import { Letter } from 'types';
import { Typography } from 'ui';
import { useTranslation } from 'i18n';

import { pluralize } from 'src/client/helpers';
import { useLetterAddresseeTags } from 'src/client/hooks';

import styles from './Addressees.module.scss';

export type AddresseesProps = {
  readonly to: Letter['to'];
  readonly className?: string;
};

export const Addressees: React.FC<AddresseesProps> = ({ to, className }) => {
  const { t } = useTranslation();
  const addressees = React.useMemo(
    () => [
      {
        name: t('component.addressee.to_self'),
        surname: '',
        email: 'your_email@mail.ru',
        avatar: '',
      },
      ...to,
    ],
    [to],
  );

  const tags = useLetterAddresseeTags({ addressees });

  return (
    <Typography
      className={clsx(styles.root, className)}
      preset="footnote"
      color="secondary"
      component="p"
    >
      {t('component.addressee.to')}: {tags.list}
      {tags.isCropped && (
        <button
          className={styles.root__expand}
          onClick={tags.expand}
          type="button"
        >
          {' '}
          {t('component.addressee.more')} {tags.croppedCount}{' '}
          {pluralize(tags.croppedCount, [
            t('component.addressee.addressees.base'),
            t('component.addressee.addressees.form_1'),
            t('component.addressee.addressees.form_2'),
          ])}
        </button>
      )}
    </Typography>
  );
};
