import React from 'react';
import clsx from 'clsx';
import { Link } from 'react-router-dom';
import { LetterListItem as LetterListItemShape } from 'types';
import { Icon, Space, Tooltip, Typography } from 'ui';
import { IconImportant } from 'icons';
import { useTranslation } from 'i18n';

import { useLetterCategories } from 'src/client/hooks';
import {
  formatLetterDate,
  formatLetterDateDetailed,
  LetterExtraCategory,
} from 'src/client/helpers';
import { BookmarkButton, ReadButton } from '../../atoms';
import { AuthorAvatar } from './partials/AuthorAvatar';
import { AttachmentPopup } from '../AttachmentPopup';

import styles from './LetterListItem.module.scss';

export type LetterListItemProps = LetterListItemShape & {
  readonly folder: string;
};

export const LetterListItem: React.FC<LetterListItemProps> = ({
  id,
  read,
  bookmark,
  important,
  author,
  title,
  text,
  date,
  flag,
  attachment,
  folder,
}) => {
  const { t } = useTranslation();
  const isImportant = important;
  const [isBookmark, setIsBookmark] = React.useState(bookmark);
  const [isRead, setIsRead] = React.useState(read);
  const [isChecked, setIsChecked] = React.useState(false);

  const categories = useLetterCategories({
    flag,
    hasAttachment: attachment,
  });

  return (
    <Link className={styles.root} to={`/${folder}/${id}`}>
      <ReadButton
        className={clsx(styles.root__read, isRead && styles.hover_visible)}
        isRead={isRead}
        // TODO: move to callback
        onClick={(event) => {
          event.preventDefault();
          setIsRead((value) => !value);
        }}
      />
      <AuthorAvatar
        className={styles.root__avatar}
        src={author.avatar}
        isChecked={isChecked}
        onChange={setIsChecked}
        avatarClassName={clsx(styles.hover_shrink, isChecked && styles.shrink)}
        checkboxClassName={clsx(!isChecked && styles.hover_visible)}
      />
      <Typography
        className={styles.root__author}
        preset="button"
        isBold={!isRead}
        shouldCrop
      >
        {author.name} {author.surname}
      </Typography>
      <div className={styles.root__important}>
        {isImportant && !isBookmark && (
          <Icon
            className={styles.hover_hidden}
            color="error"
            icon={<IconImportant />}
          />
        )}
        <BookmarkButton
          className={clsx(
            styles.root__bookmark,
            !isBookmark && styles.hover_visible,
          )}
          isBookmark={isBookmark}
          onClick={(event) => {
            // TODO: move to callback
            event.preventDefault();
            setIsBookmark((value) => !value);
          }}
        />
      </div>
      <div className={styles.root__content}>
        <div className={styles.root__text}>
          <Typography preset="button" isBold={!isRead} shouldCrop>
            {title}
          </Typography>
          <Typography preset="button" color="secondary" shouldCrop>
            {text}
          </Typography>
        </div>
      </div>
      <Space className={styles.root__categories} size="xs">
        {categories.map(({ key, Icon }) => {
          const isAttachment = key === LetterExtraCategory.Attachment;

          const content = (
            <Tooltip key={key} overlay={t(`domain.letter.category.${key}`)}>
              <Icon
                className={clsx(isAttachment && styles.root__attachments)}
                width="24px"
                height="24px"
                component="span"
              />
            </Tooltip>
          );

          if (isAttachment) {
            return (
              <AttachmentPopup id={id} folder={folder} key={key}>
                {content}
              </AttachmentPopup>
            );
          }

          return content;
        })}
      </Space>
      <Tooltip overlay={formatLetterDateDetailed(date)}>
        <Typography
          className={styles.root__date}
          preset="hint"
          color="secondary"
        >
          {formatLetterDate(date)}
        </Typography>
      </Tooltip>
    </Link>
  );
};
