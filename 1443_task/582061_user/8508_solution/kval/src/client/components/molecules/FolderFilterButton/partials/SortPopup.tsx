import React from 'react';
import {
  Popup,
  Select,
  SelectValueState,
  Typography,
  useSelectValue,
} from 'ui';
import { IconChevronRight } from 'icons';
import { useTranslation } from 'i18n';

import { FolderContext } from 'src/client/contexts/FolderContext';

export type SortPopupProps = {
  readonly onChange: (value: string) => void;
};

export const SortPopup: React.FC<SortPopupProps> = ({ onChange }) => {
  const { t } = useTranslation();
  const { sortRef } = React.useContext(FolderContext);

  const handleSortChange = React.useCallback(
    (value: SelectValueState) => {
      const selected = Object.entries(value).find(
        ([, value]) => value === true,
      );
      const [key] = selected || [];

      onChange(key || '');
    },
    [onChange],
  );

  const { value, handleChange } = useSelectValue({
    defaultValue: {
      [sortRef.current]: true,
    },
    onChange: handleSortChange,
  });

  const options = React.useMemo(
    () => [
      {
        value: 'newest',
        title: t('domain.sort.newest'),
      },
      {
        value: 'oldest',
        title: t('domain.sort.oldest'),
      },
      {
        value: 'sender-az',
        title: t('domain.sort.sender-az'),
      },
      {
        value: 'sender-za',
        title: t('domain.sort.sender-za'),
      },
      {
        value: 'subject-az',
        title: t('domain.sort.subject-az'),
      },
      {
        value: 'subject-za',
        title: t('domain.sort.subject-za'),
      },
    ],
    [],
  );

  const overlay = (
    <Select value={value} onChange={handleChange} hasResetOption={false}>
      {options.map(({ value, title }) => (
        <Select.Option key={value} value={value}>
          <Typography preset="body">{title}</Typography>
        </Select.Option>
      ))}
    </Select>
  );

  return (
    <Popup trigger="click" overlay={overlay} offset={12}>
      <Select.Action rightSlot={<IconChevronRight />}>
        {t('action.sort')}
      </Select.Action>
    </Popup>
  );
};
