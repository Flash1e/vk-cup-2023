import React from 'react';
import { LetterListItem } from 'types';
import { Image, List, ListItem, Popup, PopoverProps, Loader } from 'ui';

import { useLetterApi } from 'src/client/hooks';
import { AttachmentMiniature } from '../../atoms';

import styles from './AttachmentPopup.module.scss';

export type AttachmentPopupProps = {
  readonly id: LetterListItem['id'];
  readonly folder: LetterListItem['folder'];
} & Pick<PopoverProps, 'children'>;

export const AttachmentPopup: React.FC<AttachmentPopupProps> = ({
  id,
  folder,
  children,
}) => {
  const api = useLetterApi({ id, folder });

  const [isLoading, setIsLoading] = React.useState(true);

  const handleOpenChange = React.useCallback(
    async (isVisible: boolean) => {
      if (!isVisible || api.attachments.length) {
        return;
      }

      setIsLoading(true);
      try {
        await api.getAttachments();
      } finally {
        setIsLoading(false);
      }
    },
    [api.attachments, api.getAttachments],
  );

  const overlay = (
    <List>
      {isLoading && <Loader isStretched />}
      {!isLoading &&
        api.attachments.map((attachment) => (
          <ListItem
            key={attachment.id}
            className={styles.root__attachment}
            isHoverable
          >
            <Popup
              trigger="hover"
              placement="left"
              offset={16}
              mouseEnterDelay={0.3}
              overlay={
                <Image
                  display="wide"
                  src={attachment.src}
                  alt={attachment.name}
                  width={256}
                  height={190}
                />
              }
              className={styles.root__preview}
            >
              <AttachmentMiniature attachment={attachment} />
            </Popup>
          </ListItem>
        ))}
    </List>
  );

  return (
    <Popup
      trigger="click"
      placement="left"
      overlay={overlay}
      className={styles.root}
      onOpenChange={handleOpenChange}
    >
      {children}
    </Popup>
  );
};
