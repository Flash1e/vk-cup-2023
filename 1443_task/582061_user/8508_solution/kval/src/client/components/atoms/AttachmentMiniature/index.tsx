import React from 'react';
import clsx from 'clsx';
import { Image, Typography } from 'ui';
import { LetterAttachment } from 'types';

import { formatByteSizePower } from 'src/client/helpers';

import styles from './AttachmentMiniature.module.scss';

export type AttachmentMiniatureProps = {
  readonly attachment: Omit<LetterAttachment, 'src'> & {
    readonly src?: LetterAttachment['src'];
  };
  readonly className?: string;
};

export const AttachmentMiniature: React.FC<AttachmentMiniatureProps> = ({
  attachment,
  className,
  ...rest
}) => {
  const { src, size, name } = attachment;

  return (
    <div {...rest} className={clsx(styles.root, className)}>
      <Image
        className={styles.root__image}
        display="miniature"
        src={src}
        alt={name}
        width={32}
        height={32}
      />
      <Typography className={styles.root__title} preset="button">
        {name} {size.value} {formatByteSizePower(size.power)}
      </Typography>
    </div>
  );
};
