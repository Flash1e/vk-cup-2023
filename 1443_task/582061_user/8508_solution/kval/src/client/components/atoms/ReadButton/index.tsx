import React, { HTMLAttributes } from 'react';
import clsx from 'clsx';
import { ButtonBase, Tooltip } from 'ui';
import { useTranslation } from 'i18n';

import styles from './ReadButton.module.scss';

export type ReadButtonProps = {
  readonly className?: string;
  readonly isRead?: boolean;
} & HTMLAttributes<HTMLButtonElement>;

export const ReadButton: React.FC<ReadButtonProps> = ({
  className,
  isRead,
  ...rest
}) => {
  const { t } = useTranslation();

  return (
    <Tooltip
      overlay={
        isRead ? t('domain.action.mark_unread') : t('domain.action.mark_read')
      }
    >
      <ButtonBase
        {...rest}
        className={clsx(className, styles.root, isRead && styles.read)}
      />
    </Tooltip>
  );
};
