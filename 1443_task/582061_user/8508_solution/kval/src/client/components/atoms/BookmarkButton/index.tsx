import React from 'react';
import clsx from 'clsx';
import { ButtonBase, Tooltip } from 'ui';
import { IconBookmarkFilled, IconBookmarkOutlined } from 'icons';
import { useTranslation } from 'i18n';

import styles from './BookmarkButton.module.scss';

export type BookmarkButtonProps = {
  readonly className?: string;
  readonly isBookmark?: boolean;
} & React.ButtonHTMLAttributes<HTMLButtonElement>;

export const BookmarkButton: React.FC<BookmarkButtonProps> = ({
  className,
  isBookmark,
  ...rest
}) => {
  const { t } = useTranslation();

  return (
    <Tooltip
      overlay={
        isBookmark
          ? t('domain.action.remove_bookmark')
          : t('domain.action.place_bookmark')
      }
    >
      <ButtonBase
        {...rest}
        className={clsx(className, styles.root, isBookmark && styles.bookmark)}
      >
        <IconBookmarkFilled className={styles.root__filled} />
        <IconBookmarkOutlined className={styles.root__outlined} />
      </ButtonBase>
    </Tooltip>
  );
};
