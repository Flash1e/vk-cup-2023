import { IncomingMessage, ServerResponse as NodeServerResponse } from 'http';
import * as fsPromises from 'fs/promises';
import * as fs from 'fs';

import { ServerResponse } from '../types';
import { getMimetype } from '../../helpers';
import { getFileExtension } from './getFileExtension';
import { getCacheControl } from './getCacheControl';

export function enhanceServerResponse(
  res: NodeServerResponse<IncomingMessage>,
): ServerResponse {
  const enhanced: ServerResponse = res as ServerResponse;

  enhanced.json = function json(obj: object) {
    this.setHeader('Content-Type', 'application/json');
    this.write(JSON.stringify({ data: obj }));
    this.end();
  };

  enhanced.file = async function file(filePath: string, fallback?: () => void) {
    try {
      const stat = await fsPromises.stat(filePath);

      const mimetype = getMimetype(getFileExtension(filePath));

      this.writeHead(200, {
        'Content-Type': mimetype,
        'Content-Length': stat.size,
        'Cache-Control': getCacheControl(mimetype),
      });

      const stream = fs.createReadStream(filePath);

      stream.pipe(this);
    } catch (error) {
      if (fallback) {
        fallback();

        return;
      }

      this.writeHead(404);
      this.end(String(error));
    }
  };

  enhanced.redirect = function redirect(url: string) {
    this.writeHead(301, { Location: url });
    this.end();
  };

  return enhanced;
}
