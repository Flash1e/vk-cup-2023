export class CookieParser {
  public constructor(private readonly cookies: string) {}

  public get(name: string): string | null {
    // TODO: escape special characters
    const regexp = new RegExp(`${name}=.*?(;|$)`);

    const match = this.cookies.match(regexp);

    if (!match || !match[0]) {
      return null;
    }

    const cookie = match[0];
    const [, value] = cookie.split('=');

    if (value[value.length - 1] === ';') {
      return value.slice(0, value.length - 1);
    }

    return value;
  }
}
