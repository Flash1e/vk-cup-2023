import { RequestEntry, RequestHandler, RequestMethod } from '../types';

const ROUTE_PARAM_REGEXP = /:\w+/gim;

type RouteHandler = {
  [K in RequestMethod]: RequestHandler | undefined;
};

export class RouteHandlerMap {
  private matchers: Map<string, RegExp>;

  private handlers: Map<RegExp, RouteHandler>;

  public constructor() {
    this.matchers = new Map();
    this.handlers = new Map();
  }

  public add(
    path: string,
    method: RequestMethod,
    handler: RequestHandler,
  ): void {
    const matcher = this.getMatcherByPath(path);
    const handlers = this.handlers.get(matcher) || ({} as RouteHandler);

    if (handlers[method]) {
      throw new Error('Handler already added.');
    }

    handlers[method] = handler;
    this.handlers.set(matcher, handlers);

    console.log('Route added:', path, matcher);
  }

  public get(path: string, method: RequestMethod): RequestEntry[] {
    const entries: RequestEntry[] = [];

    // eslint-disable-next-line no-restricted-syntax
    for (const [matcher, handler] of this.handlers.entries()) {
      const match = path.match(matcher);

      if (match && handler[method]) {
        const entry: RequestEntry = {
          matcher,
          params: match.groups || {},
          handler: handler[method]!,
        };

        entries.push(entry);
      }
    }

    return entries;
  }

  private getMatcherByPath(path: string): RegExp {
    let route = path;
    const routeParams = path.match(ROUTE_PARAM_REGEXP);

    if (routeParams) {
      for (let i = 0; i < routeParams.length; i += 1) {
        const param = routeParams[i];

        route = route.replace(param, `(?<${param.slice(1)}>[^\\/]+)`);
      }
    }

    return this.getMatcherByRoute(route);
  }

  private getMatcherByRoute(route: string): RegExp {
    let matcher = this.matchers.get(route);

    if (!matcher) {
      matcher = new RegExp(`^${route}$`);
    }

    this.matchers.set(route, matcher);

    return matcher;
  }
}
