import { IncomingMessage, ServerResponse as NodeServerResponse } from 'http';
import { enhanceServerResponse } from '../utils/enhanceServerResponse';
import { RequestHandler, RequestMethod, ServerRequest } from '../types';
import { RouteHandlerMap } from './RouteHandlerMap';

export class Router {
  private handlers: RouteHandlerMap;

  public constructor() {
    this.handlers = new RouteHandlerMap();
  }

  public get(path: string, handler: RequestHandler): this {
    return this.add(path, RequestMethod.GET, handler);
  }

  public add(
    path: string,
    method: RequestMethod,
    handler: RequestHandler,
  ): this {
    this.handlers.add(path, method, handler);

    return this;
  }

  public async handle(
    path: string,
    method: RequestMethod,
    req: IncomingMessage,
    res: NodeServerResponse<IncomingMessage>,
  ): Promise<void> {
    const absolutePath = `http://${req.headers.host || 'localhost'}${path}`;
    const url = new URL(absolutePath);

    const entries = this.handlers.get(url.pathname, method);

    if (!entries.length) {
      res.writeHead(404);
      res.write('Route not found.');
      res.end();

      return;
    }

    const { matcher, params, handler } = entries[0];

    // TODO: wrap into function
    (req as ServerRequest).params = params;
    (req as ServerRequest).query = url.searchParams;

    console.log(
      'Handle',
      matcher,
      JSON.stringify(params || {}),
      url.searchParams.toString(),
    );

    try {
      await handler(req as ServerRequest, enhanceServerResponse(res));
    } catch (error) {
      console.log('Error', error);
      res.writeHead(500);
      res.end(String(error));
    }
  }
}
