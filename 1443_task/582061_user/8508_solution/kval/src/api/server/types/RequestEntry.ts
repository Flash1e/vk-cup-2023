import { RequestHandler } from 'src/api/server/types/RequestHandler';

export type RequestEntry = {
  readonly matcher: RegExp;
  readonly params: Record<string, string>;
  readonly handler: RequestHandler;
};
