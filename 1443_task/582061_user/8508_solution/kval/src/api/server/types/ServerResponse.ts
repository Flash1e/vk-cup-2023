import {
  IncomingMessage,
  ServerResponse as NodeServerResponse,
} from 'node:http';

export type ServerResponse = NodeServerResponse<IncomingMessage> & {
  json(obj: object): void;

  file(filePath: string, fallback?: () => void): Promise<void>;

  redirect(url: string): void;
};
