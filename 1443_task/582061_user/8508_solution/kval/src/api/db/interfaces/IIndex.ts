import { RecordPointer, IndexType } from '../types';

export interface IIndex<T extends IndexType = IndexType> {
  readonly type: T;
  readonly field: string;

  add(pointer: RecordPointer, record: any): void;
}

export interface IKeyIndex extends IIndex<IndexType.Key> {
  getCount(key: string): number;

  has(key: string, id: number): boolean;

  get(key: string, id: number): RecordPointer | null;

  getAll(key: string): RecordPointer[];
}

export interface ISortIndex extends IIndex<IndexType.Sort> {
  getScore(idA: number, idB: number, order: number): number;
}

export interface IMapIndex extends IIndex<IndexType.Map> {
  get(id: number): [key: string, value: any];
}
