export type ItemPointer = {
  readonly id: number;
  readonly position: number;
  readonly length: number;
  readonly properties: {
    readonly namePosition: number;
    readonly nameLength: number;
    readonly position: number;
    readonly length: number;
  }[];
};
