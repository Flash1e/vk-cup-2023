export type RecordPointer = {
  readonly id: number;
  readonly position: number;
  readonly length: number;
  readonly properties: {
    [key: string]: {
      readonly position: number;
      readonly length: number;
    };
  };
};
