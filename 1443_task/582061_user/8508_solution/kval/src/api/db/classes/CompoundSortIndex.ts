import { QuerySortClause, RecordPointer } from '../types';
import { ISortIndex } from '../interfaces';

export class CompoundSortIndex {
  public constructor(
    private readonly sortIndex: ISortIndex | null,
    private readonly order = 1,
  ) {}

  /**
   * Sorts pointers. Returns copy of a given array
   * @param pointers
   * @returns
   */
  public sort(pointers: readonly RecordPointer[]): readonly RecordPointer[] {
    const index = this.sortIndex;

    if (!index) {
      return pointers;
    }

    const result = [...pointers];

    result.sort((pointerA, pointerB) => {
      const score = index.getScore(pointerA.id, pointerB.id, this.order);

      return score;
    });

    return result;
  }

  /**
   * Creates compound sort index from query clause
   * @param clause
   * @param indexMap
   * @returns
   */
  public static fromClauses(
    clauses: QuerySortClause[],
    indexMap: Map<string, ISortIndex>,
  ): CompoundSortIndex {
    // TODO: add support for sorting across multiple sort clauses!
    // TODO: show warning!
    const clause = clauses[0];

    if (!clause) {
      return new CompoundSortIndex(null);
    }

    return new CompoundSortIndex(
      indexMap.get(clause.field) || null,
      clause.order,
    );
  }
}
