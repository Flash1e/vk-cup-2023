import { Reader } from './types';
import { OPEN_BRACES_CHAR, CLOSE_BRACES_CHAR } from './chars';

export function createJsonArrayReader(
  onItem: (position: number, length: number) => void,
): Reader {
  let recordStartByteIndex = 0;
  let bracesCount = 0;
  let isRecordReadStarted = false;

  return {
    next: (char, index, chunkSize, chunksReadCount) => {
      if (char === OPEN_BRACES_CHAR) {
        bracesCount += 1;

        if (!isRecordReadStarted) {
          isRecordReadStarted = true;
          recordStartByteIndex = chunksReadCount * chunkSize + index;
        }
      }

      if (char === CLOSE_BRACES_CHAR) {
        bracesCount -= 1;
      }

      if (isRecordReadStarted && bracesCount === 0) {
        const position = recordStartByteIndex;
        const length = chunksReadCount * chunkSize + index - position + 1;

        onItem(position, length);

        isRecordReadStarted = false;
      }
    },
  };
}
