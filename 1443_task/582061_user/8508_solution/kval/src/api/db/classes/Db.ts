import { IndexType, PlainQuery, RecordPointer } from '../types';
import { IIndex, IKeyIndex, IMapIndex, ISortIndex } from '../interfaces';
import { JsonStorage } from './JsonStorage';
import { CompoundKeyIndex } from './CompoundKeyIndex';
import { CompoundSortIndex } from './CompoundSortIndex';

export class Db {
  private readonly storage: JsonStorage;

  private pointers: RecordPointer[];
  private indexes: {
    [T in IndexType]: Map<IIndex['field'], IIndex>;
  };

  public constructor() {
    this.storage = new JsonStorage();

    this.pointers = [];
    this.indexes = {
      [IndexType.Key]: new Map(),
      [IndexType.Sort]: new Map(),
      [IndexType.Map]: new Map(),
    };
  }

  /**
   * Gets record by id
   * @param id
   * @returns
   */
  public async getById<T extends any>(
    id: number,
    select: string[] = [],
    mapFields: string[] = [],
  ): Promise<T> {
    const pointer = this.pointers[id];

    if (!pointer) {
      throw new Error('Entity not found.');
    }

    return this.extract(pointer, select, mapFields);
  }

  /**
   * Queries records from db
   * @param query
   * @returns
   */
  public async getMany<T extends any>(
    query: PlainQuery,
  ): Promise<[T[], number]> {
    const {
      select,
      where,
      sort,
      mapFields,
      skip = 0,
      limit = undefined,
    } = query;

    const filterIndex = CompoundKeyIndex.fromClauses(
      where,
      this.indexes[IndexType.Key] as Map<string, IKeyIndex>,
    );
    const sortIndex = CompoundSortIndex.fromClauses(
      sort,
      this.indexes[IndexType.Sort] as Map<string, ISortIndex>,
    );

    const pointers = sortIndex.sort(filterIndex.filter(this.pointers));

    const partition = await Promise.all(
      pointers
        .slice(skip, limit ? skip + limit : undefined)
        .map(async (pointer) => this.extract(pointer, select, mapFields)),
    );

    return [partition, pointers.length];
  }

  /**
   * Connect to DB
   * @param path
   */
  public async connect(path: string): Promise<void> {
    await this.storage.connect(path);

    await this.syncPointers();
  }

  /**
   * Close connection
   */
  public async disconnect(): Promise<void> {
    await this.storage.disconnect();
  }

  /**
   * Adds index to DB
   */
  public addIndex(index: IIndex): void {
    this.indexes[index.type].set(index.field, index);
  }

  /**
   * Synchronize indexes with current storage
   */
  private async syncPointers(): Promise<void> {
    const pointers = await this.storage.createRecordPointers();

    const recordsWithPointers = await Promise.all(
      pointers.map(async ({ id, position, length, properties }) => {
        const buffer = await this.storage.readBuffer(position, length);

        const serialized = Db.DECODE_BUFFER(buffer);

        const pointer: RecordPointer = {
          id,
          position,
          length,
          properties: (
            await Promise.all(
              properties.map(
                async ({ position, length, namePosition, nameLength }) => ({
                  name: Db.DECODE_BUFFER(
                    await this.storage.readBuffer(namePosition, nameLength),
                  ),
                  position,
                  length,
                }),
              ),
            )
          ).reduce((acc, { name, ...rest }) => ({ ...acc, [name]: rest }), {}),
        };

        return [pointer, Db.DESERIALIZE(id, serialized)] as const;
      }),
    );

    for (let i = 0; i < recordsWithPointers.length; i += 1) {
      const [pointer, record] = recordsWithPointers[i];
      this.pointers[i] = pointer;

      const types = Object.values(IndexType);

      for (let j = 0; j < types.length; j += 1) {
        const indexMap = this.indexes[types[j]];

        for (const index of indexMap.values()) {
          (index as IKeyIndex).add(pointer, record);
        }
      }
    }
  }

  /**
   * Extract record data from pointer
   * @param pointer
   * @param properties
   * @returns
   */
  private async extract(
    pointer: RecordPointer,
    properties: string[] = [],
    mapIndexes: string[] = [],
  ): Promise<any> {
    const mappedFields: any = {};

    if (mapIndexes.length) {
      for (const indexField of mapIndexes) {
        const index = this.indexes[IndexType.Map].get(indexField) as IMapIndex;

        if (index) {
          const [key, value] = index.get(pointer.id);

          mappedFields[key] = value;
        }
      }
    }

    if (!properties.length) {
      const buffer = await this.storage.extractRecordPointer(pointer);

      return {
        ...Db.DESERIALIZE(pointer.id, Db.DECODE_BUFFER(buffer)),
        ...mappedFields,
      };
    }

    const props = await Promise.all(
      properties
        .filter((prop) => pointer.properties[prop])
        .map(async (prop) => {
          const { position, length } = pointer.properties[prop];
          const buffer = await this.storage.readBuffer(position, length);

          return Db.DECODE_BUFFER(buffer);
        }),
    );

    const serialized = `{${props.join(',')}}`;

    return {
      ...Db.DESERIALIZE(pointer.id, serialized),
      ...mappedFields,
    };
  }

  /**
   * Converts binary buffer into string
   * @param buffer
   * @returns
   */
  private static DECODE_BUFFER(buffer: Buffer): string {
    return buffer.toString('utf-8');
  }

  /**
   * Deserializes record into JSON
   * @param id
   * @param record
   * @returns
   */
  private static DESERIALIZE(id: number, record: string): any {
    const data = JSON.parse(record);

    return {
      ...data,
      id,
    };
  }
}
