import { IndexType, RecordPointer } from '../../types';
import { IKeyIndex } from '../../interfaces';

export type KeyIndexOptions = {
  readonly field: string;
  readonly mapFieldToKey?: (value: any) => any | null;
};

export class KeyIndex implements IKeyIndex {
  public readonly type: IndexType.Key;
  public readonly field: string;

  protected map: Map<any, Map<number, RecordPointer>>;

  public constructor(public readonly options: KeyIndexOptions) {
    this.type = IndexType.Key;
    this.field = options.field;

    this.map = new Map();
  }

  public getCount(key: any): number {
    const partition = this.map.get(key);

    if (!partition) {
      return 0;
    }

    return partition.size;
  }

  public has(key: any, id: number): boolean {
    const partition = this.map.get(key);

    if (!partition) {
      return false;
    }

    return partition.has(id);
  }

  public get(key: any, id: number): RecordPointer | null {
    const partition = this.map.get(key);

    if (!partition) {
      return null;
    }

    return partition.get(id) || null;
  }

  public getAll(key: any): RecordPointer[] {
    const partition = this.map.get(key);

    if (!partition) {
      return [];
    }

    return Array.from(partition.values());
  }

  public add(pointer: RecordPointer, record: any): void {
    const key = this.options.mapFieldToKey
      ? this.options.mapFieldToKey(record?.[this.field])
      : String(record?.[this.field]);

    if (typeof key === 'undefined' || key === null) {
      return;
    }

    const pointerMap = this.map.get(key) || new Map<number, RecordPointer>();

    pointerMap.set(pointer.id, pointer);

    this.map.set(key, pointerMap);
  }
}
