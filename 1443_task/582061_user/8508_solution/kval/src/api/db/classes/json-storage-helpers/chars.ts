export const OPEN_BRACES_CHAR = 123;
export const CLOSE_BRACES_CHAR = 125;
export const OPEN_SQUARE_BRACKET_CHAR = 91;
export const CLOSE_SQUARE_BRACKET_CHAR = 93;
export const QUOTE_CHAR = 34;
export const SLASH_CHAR = 92;
export const COLON_CHAR = 58;
export const COMMA_CHAR = 44;

export const DOT_CHAR = 46;
export const ZERO_CHAR = 47;

export const T_CHAR = 116;
export const R_CHAR = 114;
export const U_CHAR = 117;
export const E_CHAR = 101;
export const F_CHAR = 102;
export const A_CHAR = 97;
export const L_CHAR = 108;
export const S_CHAR = 115;
