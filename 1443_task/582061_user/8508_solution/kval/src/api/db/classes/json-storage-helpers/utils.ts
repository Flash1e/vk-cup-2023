import {
  A_CHAR,
  DOT_CHAR,
  E_CHAR,
  F_CHAR,
  L_CHAR,
  R_CHAR,
  S_CHAR,
  T_CHAR,
  U_CHAR,
  ZERO_CHAR,
} from './chars';

const BOOLEAN_ALPHABET = [
  T_CHAR,
  R_CHAR,
  U_CHAR,
  E_CHAR,
  F_CHAR,
  A_CHAR,
  L_CHAR,
  S_CHAR,
];

export function isBooleanChar(char: string | number): boolean {
  return BOOLEAN_ALPHABET.includes(char as number);
}

export function isNumberChar(char: string | number): boolean {
  return char >= DOT_CHAR && char <= ZERO_CHAR + 9;
}
