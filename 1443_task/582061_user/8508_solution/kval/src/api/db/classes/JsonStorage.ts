import { FileHandle, open } from 'fs/promises';

import { ItemPointer, RecordPointer } from '../types';
import {
  createJsonArrayReader,
  createJsonPropertyReader,
} from './json-storage-helpers';

const DEFAULT_CHUNK_SIZE = 128 * 1024;

/**
 * Low-level layer for working with JSON db file
 */
export class JsonStorage {
  private handle: FileHandle | null;
  private filePath: string | null;

  public constructor(private readonly readChunkSize = DEFAULT_CHUNK_SIZE) {
    this.handle = null;
    this.filePath = null;
  }

  /**
   * Connect to a file
   * @param filePath
   */
  public async connect(filePath: string): Promise<void> {
    this.filePath = filePath;
    this.handle = await open(filePath);
  }

  /**
   * Close connection
   */
  public async disconnect(): Promise<void> {
    await this.handle?.close();
  }

  /**
   * Reads JSON file and creates pointers to each record inside JSON
   * @returns
   */
  public createRecordPointers(): Promise<readonly ItemPointer[]> {
    return new Promise((resolve) => {
      const handle = this.ensureIsConnected();

      const pointers: ItemPointer[] = [];

      const stream = handle.createReadStream({
        autoClose: false,
        highWaterMark: this.readChunkSize,
      });

      let id = 0;
      let chunksReadCount = 0;
      let properties: ItemPointer['properties'] = [];

      const propReader = createJsonPropertyReader(
        (namePosition, nameLength, position, length) => {
          properties.push({
            namePosition,
            nameLength,
            position,
            length,
          });
        },
      );
      const arrayReader = createJsonArrayReader((position, length) => {
        pointers.push({
          id,
          position,
          length,
          properties,
        });

        properties = [];
        id += 1;
      });

      stream.on('data', (chunk) => {
        for (let i = 0; i < chunk.length; i += 1) {
          const char = chunk[i];

          propReader.next(char, i, this.readChunkSize, chunksReadCount, chunk);
          arrayReader.next(char, i, this.readChunkSize, chunksReadCount, chunk);
        }

        chunksReadCount += 1;
      });

      stream.on('end', () => {
        resolve(pointers);
      });
    });
  }

  /**
   * Reads buffer of binary data of given length from given position
   * @param position
   * @param length
   * @returns
   */
  public async readBuffer(position: number, length: number): Promise<Buffer> {
    const handle = this.ensureIsConnected();
    const buffer = Buffer.alloc(length);

    await handle.read({ buffer, position, length });

    return buffer;
  }

  /**
   * Extracts entity buffer from file by it's pointer
   * @param pointer
   * @returns
   */
  public async extractRecordPointer(pointer: RecordPointer): Promise<Buffer> {
    const handle = this.ensureIsConnected();

    const { position, length } = pointer;

    const buffer = Buffer.alloc(length);

    await handle.read({ buffer, position, length });

    return buffer;
  }

  /**
   * Ensure that storage is connected to the file
   *
   * TODO: check not just handle persistance, but actual connection
   */
  private ensureIsConnected(): FileHandle {
    if (!this.handle) {
      throw new Error(
        `JsonStorage is not connected to a file. DB path: ${this.filePath}`,
      );
    }

    return this.handle;
  }
}
