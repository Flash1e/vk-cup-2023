import { PlainQuery } from '../types';

export class Query {
  private readonly plain: PlainQuery;

  public constructor(query?: PlainQuery) {
    this.plain = {
      select: [],
      where: [],
      sort: [],
      skip: undefined,
      limit: undefined,
      mapFields: [],
      ...query,
    };
  }

  public select(fields: string[]): this {
    this.plain.select.push(...fields);

    return this;
  }

  public where(field: string, value: any): this {
    // TODO: remove possible duplicates
    this.plain.where.push({ field, value });

    return this;
  }

  public sortBy(field: string, order: number): this {
    // TODO: remove possible duplicates
    this.plain.sort.push({ field, order });

    return this;
  }

  public skip(count: number): this {
    this.plain.skip = count;

    return this;
  }

  public limit(count: number): this {
    this.plain.limit = count;

    return this;
  }

  public map(field: string): this {
    this.plain.mapFields.push(field);

    return this;
  }

  public build(): PlainQuery {
    return this.plain;
  }

  public serialize(): string {
    const { select, where, sort, mapFields, skip, limit } = this.plain;
    const components: string[] = [];

    if (select.length) {
      components.push(select.map((value) => `select[${value}]`).join('&'));
    }

    if (where.length) {
      components.push(
        where.map(({ field, value }) => `where[${field}:${value}]`).join('&'),
      );
    }

    if (sort.length) {
      components.push(
        sort.map(({ field, order }) => `sort[${field}:${order}]`).join('&'),
      );
    }

    if (mapFields.length) {
      components.push(mapFields.map((field) => `map[${field}]`).join('&'));
    }

    if (skip) {
      components.push(`skip[${skip}]`);
    }

    if (limit) {
      components.push(`limit[${limit}]`);
    }

    return components.join('&');
  }
}
