import { Reader } from './types';
import { SLASH_CHAR, QUOTE_CHAR } from './chars';

export function createJsonBoundValueReader(
  openBoundChar: number,
  closeBoundChar: number,
  onValue: (position: number, length: number) => void,
): Reader {
  const isBoundsEqual = openBoundChar === closeBoundChar;

  let valueStartByteIndex = 0;
  let boundCharCount = 0;
  let isValueReadStarted = false;
  let prevChar: number | string | null = null;

  /** Is current character is not belong to any string value */
  let isMetaChar = true;
  let enterMetaSpaceIndex = -1;

  return {
    next: (char, index, chunkSize, chunksReadCount) => {
      if (!isMetaChar && prevChar !== SLASH_CHAR && char === QUOTE_CHAR) {
        isMetaChar = true;
        enterMetaSpaceIndex = chunksReadCount * chunkSize + index;
      }

      if (isMetaChar) {
        if (isBoundsEqual && char === openBoundChar) {
          if (boundCharCount === 0 && !isValueReadStarted) {
            boundCharCount += 1;
            isValueReadStarted = true;
            valueStartByteIndex = chunksReadCount * chunkSize + index;
          } else if (boundCharCount === 1) {
            boundCharCount = 0;
          }
        }

        if (!isBoundsEqual && char === openBoundChar) {
          boundCharCount += 1;

          if (!isValueReadStarted) {
            isValueReadStarted = true;
            valueStartByteIndex = chunksReadCount * chunkSize + index;
          }
        }

        if (!isBoundsEqual && char === closeBoundChar) {
          boundCharCount -= 1;
        }
      }

      if (isValueReadStarted && boundCharCount === 0) {
        const position = valueStartByteIndex;
        const length = chunksReadCount * chunkSize + index - position + 1;

        onValue(position, length);

        isValueReadStarted = false;
      }

      if (isMetaChar && prevChar !== SLASH_CHAR && char === QUOTE_CHAR) {
        const enterIndex = chunksReadCount * chunkSize + index;

        if (enterIndex !== enterMetaSpaceIndex) {
          isMetaChar = false;
        }
      }

      prevChar = char;
    },
  };
}
