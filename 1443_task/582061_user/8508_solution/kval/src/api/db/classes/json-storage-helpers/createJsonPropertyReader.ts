import { Reader } from './types';
import { isBooleanChar, isNumberChar } from './utils';
import {
  CLOSE_BRACES_CHAR,
  CLOSE_SQUARE_BRACKET_CHAR,
  COLON_CHAR,
  COMMA_CHAR,
  OPEN_BRACES_CHAR,
  OPEN_SQUARE_BRACKET_CHAR,
  QUOTE_CHAR,
  SLASH_CHAR,
} from './chars';
import { createJsonBoundValueReader } from './createJsonBoundValueReader';
import { createJsonNumberReader } from './createJsonNumberReader';
import { createJsonBooleanReader } from './createJsonBooleanReader';

export function createJsonPropertyReader(
  onProperty: (
    namePosition: number,
    nameLength: number,
    position: number,
    length: number,
  ) => void,
): Reader {
  let prevChar: string | number | null = null;

  let propStartByteIndex = 0;
  let propNameLength = 0;
  let quotesCount = 0;
  let isPropReadStarted = false;
  let isPropValueStarted = false;

  let valueReader: Reader | null = null;

  const endPropertyRead = () => {
    propStartByteIndex = 0;
    propNameLength = 0;
    quotesCount = 0;
    isPropReadStarted = false;
    isPropValueStarted = false;
    valueReader = null;
  };

  const handleValueEnd = (_: number, length: number) => {
    onProperty(
      propStartByteIndex + 1,
      propNameLength - 2,
      propStartByteIndex,
      propNameLength + length + 1,
    );
    endPropertyRead();
  };

  return {
    next: (char, index, chunkSize, chunksReadCount, chunk) => {
      const isQuote = char === QUOTE_CHAR;
      /**
       * Is previous char marks that we start to read new property name
       */
      const isPrevCharMarker =
        prevChar === null ||
        prevChar === OPEN_BRACES_CHAR ||
        prevChar === COMMA_CHAR;

      // Detect property name open quote
      if (isQuote && isPrevCharMarker && !isPropReadStarted) {
        isPropReadStarted = true;
        propStartByteIndex = chunksReadCount * chunkSize + index;
      }

      // Count only unescaped quotes
      if (isQuote && prevChar !== SLASH_CHAR) {
        quotesCount += 1;
      }

      // Check if previous quote is property name closing quote
      if (char === COLON_CHAR && quotesCount === 2 && !isPropValueStarted) {
        propNameLength =
          chunksReadCount * chunkSize + index - propStartByteIndex;
        isPropValueStarted = true;
      }

      if (isPropValueStarted) {
        if (!valueReader) {
          if (char === OPEN_BRACES_CHAR) {
            valueReader = createJsonBoundValueReader(
              OPEN_BRACES_CHAR,
              CLOSE_BRACES_CHAR,
              handleValueEnd,
            );
          }

          if (char === OPEN_SQUARE_BRACKET_CHAR) {
            valueReader = createJsonBoundValueReader(
              OPEN_SQUARE_BRACKET_CHAR,
              CLOSE_SQUARE_BRACKET_CHAR,
              handleValueEnd,
            );
          }

          if (char === QUOTE_CHAR) {
            valueReader = createJsonBoundValueReader(
              QUOTE_CHAR,
              QUOTE_CHAR,
              handleValueEnd,
            );
          }

          if (isNumberChar(char)) {
            valueReader = createJsonNumberReader(handleValueEnd);
          }

          if (isBooleanChar(char)) {
            valueReader = createJsonBooleanReader(handleValueEnd);
          }
        }

        if (valueReader) {
          valueReader.next(char, index, chunkSize, chunksReadCount, chunk);
        }
      }

      prevChar = char;
    },
  };
}
