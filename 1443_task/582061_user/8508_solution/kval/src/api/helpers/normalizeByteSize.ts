export function normalizeByteSize(size: number): [size: number, power: number] {
  const power = Math.floor(Math.log2(size) / 10) * 10;

  return [Number((size / 2 ** power).toFixed(2)), power];
}
