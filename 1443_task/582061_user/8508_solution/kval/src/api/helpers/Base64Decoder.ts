import { Mimetype } from '../constants';
import { getExtension } from './getExtension';

export type Base64Meta = {
  readonly extension: string | null;
  readonly size: number;
};

export class Base64Decoder {
  public constructor(private readonly encoded: string) {}

  public getMeta(): Base64Meta {
    let extension: Base64Meta['extension'] = null;
    let size: Base64Meta['size'] = 0;

    const mimetypeDelimiterIndex = this.encoded.indexOf(';');

    if (mimetypeDelimiterIndex !== -1) {
      const [, mimetype] = this.encoded
        .slice(0, mimetypeDelimiterIndex)
        .split(':');

      try {
        extension = getExtension(mimetype as Mimetype);
      } catch (error) {
        // TODO: register missing mimetype

        // eslint-disable-next-line no-console
        console.log(error);
      }
    }

    const [, base64] = this.encoded.split(',');

    size = Math.ceil(base64.length / 4) * 3;

    const paddings = base64.slice(base64.length - 2);

    if (paddings === '==') {
      size -= 2;
    } else if (paddings[1] === '=') {
      size -= 1;
    }

    return {
      extension,
      size,
    };
  }
}
