import { Mimetype, MimetypeExtensionReverseMap } from '../constants';

export function getMimetype(ext: string): Mimetype {
  const mimetype = MimetypeExtensionReverseMap[ext];

  if (!mimetype) {
    throw new Error(`Unknown extension: ${ext}. Could not get mimetype.`);
  }

  return mimetype;
}
