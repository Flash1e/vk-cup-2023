import { Folder } from 'types';
import { Registry } from '../Registry';
import { validateFolder, validatePagination } from '../validators';
import { Router } from '../server';
import { LetterProjector } from '../projections';
import { CookieParser } from '../server/utils/CookieParser';

export const router = new Router()

  .get('/api/health', async (req, res) => {
    res.writeHead(200);
    res.end('ok');
  })

  .get('/api/letters/:folder', async (req, res) => {
    const [page, limit] = validatePagination(req.query);
    const folder = validateFolder(req.params.folder as Folder);

    const filters = req.query
      .get('filter')
      ?.split(',')
      .reduce((acc, key) => ({ ...acc, [key]: true }), {} as any) || {
      read: false,
      bookmark: false,
      doc: false,
    };
    const sort = req.query.get('sort');

    const [letters, count] = await Registry.getLetterRepository().getMany(
      folder,
      page,
      limit,
      filters,
      sort || 'newest',
    );

    res.setHeader('Access-Control-Allow-Origin', '*');
    res.json({
      letters: letters.map(LetterProjector.prettify),
      count,
    });
  })

  .get('/api/letters/:folder/:id/attachments', async (req, res) => {
    const doc = await Registry.getLetterRepository().getDoc(
      Number(req.params.id),
    );

    res.setHeader('Access-Control-Allow-Origin', '*');
    res.json({
      attachments: LetterProjector.toAttachments(doc),
    });
  })

  .get('/api/letters/:folder/:id', async (req, res) => {
    const letter = await Registry.getLetterRepository().getById(
      Number(req.params.id),
    );

    res.setHeader('Access-Control-Allow-Origin', '*');
    res.json({
      letter: LetterProjector.prettify(letter),
    });
  })

  // TODO: move to different router
  .get('/', async (_, res) => res.redirect('/inbox'))

  .get('/404', async (_, res) => res.file('./404.html'))

  .get('/:folder', async (req, res) => {
    try {
      validateFolder(req.params.folder as Folder);

      const lang = new CookieParser(req.headers.cookie || '').get('lang');

      return await res.file(`./${lang || 'ru'}.html`);
    } catch (error) {
      res.redirect('/404');
    }
  })

  .get('/:folder/:id', async (req, res) => {
    try {
      validateFolder(req.params.folder as Folder);

      const lang = new CookieParser(req.headers.cookie || '').get('lang');

      return await res.file(`./${lang || 'ru'}.html`);
    } catch (error) {
      res.redirect('/404');
    }
  })

  .get('/_next/(\\S*/?)+\\.(css|js|json)', async (req, res) =>
    res.file(decodeURI((req.url || '/')?.slice(1)), () => res.redirect('/404')),
  )

  .get('/static/(\\S*/?)+\\.(png|svg|jpeg|jpg)', async (req, res) =>
    res.file(decodeURI((req.url || '/')?.slice(1)), () => res.redirect('/404')),
  );
