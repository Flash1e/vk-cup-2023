import { parseNumber } from '../parsers';

export function validatePagination(
  query: URLSearchParams,
): [page: number, limit: number] {
  const page = parseNumber(query.get('page') || '', 0);
  const limit = parseNumber(query.get('limit') || '', 10);

  return [page, limit];
}
