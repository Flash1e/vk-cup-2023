import { Letter, LetterAttachment, LetterFlag, LetterListItem } from 'types';
import { Base64Decoder, normalizeByteSize } from '../helpers';

export abstract class LetterProjector {
  public static prettify(
    letter: Letter | LetterListItem,
  ): Letter | LetterListItem {
    const { flag, ...rest } = letter;

    return {
      ...rest,
      flag: LetterProjector.mapFlag(flag),
    };
  }

  public static shrink(letter: LetterListItem): LetterListItem {
    const { text, ...rest } = this.prettify(letter) as LetterListItem;

    return {
      ...rest,
      text: text.slice(0, 50),
    };
  }

  public static toAttachments(doc: Letter['doc']): LetterAttachment[] {
    const image = doc?.img;

    if (image) {
      const meta = new Base64Decoder(image).getMeta();
      const name = `image_1.${meta.extension || 'unknown'}`;
      const [size, power] = normalizeByteSize(meta.size);

      return [
        {
          name,
          size: {
            power,
            value: size,
          },
          id: 1,
          // TODO: set correct type
          type: 'image',
          src: image,
        },
      ];
    }

    return [];
  }

  private static mapFlag(flag: string | undefined): LetterFlag | undefined {
    switch (flag) {
      case 'Заказы':
        return LetterFlag.Orders;

      case 'Финансы':
        return LetterFlag.Finance;

      case 'Регистрации':
        return LetterFlag.Accounts;

      case 'Путешевствия':
      case 'Путешествия':
        return LetterFlag.Travel;

      case 'Билеты':
        return LetterFlag.Ticket;

      case 'Штрафы и налоги':
        return LetterFlag.Government;

      default:
        return undefined;
    }
  }
}
