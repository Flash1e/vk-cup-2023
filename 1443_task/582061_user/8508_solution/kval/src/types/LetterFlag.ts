export enum LetterFlag {
  Orders = 'orders',
  Finance = 'finance',
  Accounts = 'accounts',
  Travel = 'travel',
  Ticket = 'ticket',
  Government = 'government',
}
