import { LetterFlag } from './LetterFlag';

export type Letter = {
  readonly id: number;
  readonly author: {
    readonly name: string;
    readonly surname: string;
    readonly email: string;
    readonly avatar: string;
  };
  readonly to: {
    readonly name: string;
    readonly surname: string;
    readonly email: string;
    readonly avatar: string;
  }[];
  readonly title: string;
  readonly text: string;
  readonly bookmark: boolean;
  readonly important: boolean;
  readonly read: boolean;
  readonly folder: string;
  readonly date: string;
  readonly doc?: {
    readonly img: string;
  };
  readonly flag?: LetterFlag;
};
