export enum Folder {
  Inbox = 'inbox',
  Important = 'important',
  Sent = 'sent',
  Drafts = 'drafts',
  Archive = 'archive',
  Spam = 'spam',
  Trash = 'trash',
}
