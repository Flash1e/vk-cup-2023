export const Settings = () => (
  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="none">
    <path
      fill="currentColor"
      fillRule="evenodd"
      d="M11.7 7.5a3 3 0 1 0-3.4 5 3 3 0 0 0 3.4-5Zm-2.5 3a1 1 0 1 1 1.6-1 1 1 0 0 1-1.6 1Z"
      clipRule="evenodd"
    />
    <path
      fill="currentColor"
      fillRule="evenodd"
      d="M9 2h2a1 1 0 0 1 .9.5l.8 1.5a1 1 0 0 0 .9.4h2.2a1 1 0 0 1 .8.5l1.1 2a1 1 0 0 1 0 1l-1 1.6a1 1 0 0 0 0 1l1 1.7a1 1 0 0 1 0 1l-1 1.9a1 1 0 0 1-1 .5h-2.1a1 1 0 0 0-.9.5l-.8 1.4a1 1 0 0 1-.9.4H9a1 1 0 0 1-.8-.4l-1-1.5a1 1 0 0 0-.8-.4H4.2a1 1 0 0 1-.8-.5l-1.1-2a1 1 0 0 1 0-1l1-1.6a1 1 0 0 0 0-1l-1-1.7a1 1 0 0 1 0-1l1-1.9a1 1 0 0 1 1-.5h2.1a1 1 0 0 0 .9-.4l.9-1.5a1 1 0 0 1 .8-.4Zm2 3-.5-1h-1L9 5a3 3 0 0 1-2.6 1.4H4.8l-.2.4-.3.5.7 1.2a3 3 0 0 1 0 3l-.7 1.3.3.5.2.3h1.6A3 3 0 0 1 9 15.1l.5.9h1l.5-1a3 3 0 0 1 2.6-1.4h1.6l.2-.4.3-.5-.7-1.2a3 3 0 0 1 0-3l.6-1.2-.3-.6-.1-.3h-1.6A3 3 0 0 1 11 5Z"
      clipRule="evenodd"
    />
  </svg>
);
