import { Component, createEffect, createSignal, lazy, Show } from 'solid-js'
import { Routes, Route, Navigate, useParams } from '@solidjs/router'

import styles from './App.module.css'
import Header from './components/header/Header'
import Toolbar from './components/toolbar/Toolbar'

import Folder from './components/folder/Folder'
import Message from './components/message/Message'
import { useGlobal } from './GlobalContext'
import Settings from './components/settings/Settings'
import Popup from './components/common/Popup'
import NewLetter from './components/newletter/NewLetter'

// const Folder = lazy(() => import('./components/folder/Folder'))
// const Message = lazy(() => import('./components/message/Message'))

const App: Component = () => {
  const [showSettings, setShowSettings] = useGlobal().showSettings

  return (
    <>
      <div
        classList={{ [styles.root]: true, [styles.minimized]: showSettings() }}
      >
        <Header />
        <Toolbar />
        <div class={styles.mainCont}>
          <Routes>
            <Route path="/message/:id" component={Message} />
            <Route path="/folder/:folder" component={Folder} />
            <Route path="/" element={<Navigate href="/folder/0" />} />
          </Routes>
        </div>
      </div>
      <Settings />
      <NewLetter />
    </>
  )
}

export default App
