import {
  Component,
  Accessor,
  JSX,
  For,
  ComponentProps,
  createEffect,
  createSignal,
  Show,
  createResource,
} from 'solid-js'
import ToolbarButton from './ToolbarButton'
import styles from './Toolbar.module.css'
import MailTopicBtn from './MailTopicBtn'

import ExpandIcon from './icons/expand.svg?component-solid'
import PencilIcon from './icons/pencil.svg?component-solid'
import ArchiveIcon from './icons/archive.svg?component-solid'
import TrashIcon from './icons/trash.svg?component-solid'
import DraftIcon from './icons/draft.svg?component-solid'
import FolderIcon from './icons/important.svg?component-solid'
import InboxIcon from './icons/inbox.svg?component-solid'
import SentIcon from './icons/sent.svg?component-solid'
import SpamIcon from './icons/spam.svg?component-solid'
import SettingsIcon from './icons/settings.svg?component-solid'
import PlusIcon from './icons/plus.svg?component-solid'
import { ApiFolders } from '../../../common/api'
import { useGlobal } from '../../GlobalContext'
import Popup from '../common/Popup'

export { FolderIcon }

export const LogoMap: { [a: string]: Component<ComponentProps<'svg'>> } = {
  Входящие: InboxIcon,
  Важное: FolderIcon,
  Отправленные: SentIcon,
  Черновики: DraftIcon,
  Архив: ArchiveIcon,
  Спам: SpamIcon,
  Корзина: TrashIcon,
}

async function fetchCustomFolders() {
  let folders = await fetch('/api/customFolders')
  return (await folders.json()) as string[]
}

const Toolbar: Component = () => {
  const globalCtx = useGlobal()
  const lang = globalCtx.lang
  const [showSettings, setShowSettings] = globalCtx.showSettings
  const [showNewLetter, setShowNewLetter] = globalCtx.showNewLetter

  const [expanded, setExpanded] = createSignal(false)
  const [newFolder, setNewFolder] = createSignal(false)
  const [folderName, setFolderName] = createSignal('')
  createEffect(() =>
    document.body.classList.toggle(styles.toolbarExpanded, expanded())
  )

  const [customFolders, { refetch: refetchCustomFolders }] = createResource(
    0,
    fetchCustomFolders
  )

  function createFolder() {
    console.log('Create folder')
    fetch('/api/newFolder', {
      method: 'POST',
      body: folderName(),
    }).then((e) => {
      setNewFolder(false)
      refetchCustomFolders()
    })
  }

  return (
    <div class={styles.root}>
      <ToolbarButton
        class={styles.newLetter}
        icon={PencilIcon}
        onClick={() => setShowNewLetter(true)}
      >
        {lang.newLetter}
      </ToolbarButton>

      <ToolbarButton
        icon={ExpandIcon}
        class={styles.expandScreenHide}
        classList={{ [styles.expandHider]: !expanded() }}
        onClick={() => {
          setExpanded(!expanded())
        }}
      >
        {lang.menu}
      </ToolbarButton>

      <For each={ApiFolders}>
        {(e, i) => (
          <MailTopicBtn
            id={i()}
            name={lang.folderNames[i()]}
            icon={LogoMap[e] ?? PlusIcon}
            setExpanded={setExpanded}
          />
        )}
      </For>

      <div class={styles.divider} />
      <For each={customFolders()}>
        {(e, i) => (
          <MailTopicBtn
            id={10 + i()}
            name={e}
            icon={FolderIcon}
            setExpanded={setExpanded}
          />
        )}
      </For>

      <Show when={newFolder()}>
        <input
          value={folderName()}
          onChange={(e) => setFolderName((e.target as HTMLInputElement).value)}
          onKeyDown={(e) => {
            setFolderName((e.target as HTMLInputElement).value)
            if (e.key === 'Enter') {
              e.preventDefault()
              createFolder()
            }
          }}
        />
      </Show>
      <ToolbarButton
        icon={PlusIcon}
        class={styles.newDir}
        onClick={() => {
          if (!newFolder()) {
            setNewFolder(true)
            setExpanded(true)
            setFolderName('')
          } else {
            setNewFolder(false)
            createFolder()
          }
        }}
      >
        {newFolder() ? lang.createFolder : lang.newFolder}
      </ToolbarButton>

      <ToolbarButton
        icon={SettingsIcon}
        onClick={() => setShowSettings((x) => !x)}
        class={styles.settingsBtn}
      >
        {lang.settings}
      </ToolbarButton>
    </div>
  )
}

export default Toolbar
