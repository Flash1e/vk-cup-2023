import { Component, ComponentProps, createEffect, JSX, Setter } from 'solid-js'
import { useNavigate, useParams } from '@solidjs/router'
import ToolbarButton from './ToolbarButton'
import { useGlobal } from '../../GlobalContext'
import { MsgManipDetails } from '../folder/Message'

interface MailTopicBtnProps {
  icon: Component<ComponentProps<'svg'>>
  name: string
  id: number
  setExpanded: Setter<boolean>
}

const MailTopicBtn: Component<MailTopicBtnProps> = (p) => {
  const navigate = useNavigate()
  const [folder, setFolder] = useGlobal().folder
  const isFolder = () => folder() == p.id

  return (
    <ToolbarButton
      onDragOver={(e) => {
        e.preventDefault()
        if (e.dataTransfer) e.dataTransfer.dropEffect = 'move'
      }}
      onDrop={(e) => {
        const msgUrl = e.dataTransfer?.getData('text/plain')
        const msgEl = document.querySelector(`[data-msgurl="${msgUrl}"]`)
        const manipOptions: MsgManipDetails = {
          onlySelected: false,
          action: {
            type: 'move',
            targetF: p.id,
          },
        }
        console.log(msgEl, msgUrl)
        if (msgEl) {
          msgEl.dispatchEvent(
            new CustomEvent('manip-msg', {
              detail: manipOptions,
            })
          )
        }
      }}
      icon={p.icon}
      onClick={() => {
        navigate('/folder/' + p.id)
        p.setExpanded(false)
      }}
      active={isFolder()}
    >
      {p.name}
    </ToolbarButton>
  )
}

export default MailTopicBtn
