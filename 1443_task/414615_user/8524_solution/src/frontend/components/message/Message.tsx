import { useParams } from '@solidjs/router'
import {
  Component,
  createEffect,
  createResource,
  createSignal,
  Show,
} from 'solid-js'
import { ApiFlags, ApiMsg, ApiUser } from '../../../common/api'
import Attachment from '../common/Attachment'
import Avatar from '../common/Avatar'
import Category from '../common/Category'
import { Bookmarker, ReadButton } from '../common/MessageButtons'
import ImportantIcon from '../common/assets/important.svg?component-solid'
import styles from './Message.module.css'
import { Language } from '../../lang'
import { useGlobal } from '../../GlobalContext'
import { displayName, formatTime } from '../common/format'
import { purifyInto } from '../common/purify'

async function loadMsg(folder: string): Promise<ApiMsg> {
  const res = await fetch('/api/message/' + folder)
  const msg: ApiMsg = await res.json()

  fetch(`/api/setRead/${msg.id}?true`)

  return msg
}

function getDate(lang: Language, date: Date): string {
  // const curDate = new Date('Sun Nov 27 2022 22:24:36 GMT+0300')
  const curDate = new Date()
  curDate.setHours(0, 0, 0, 0)
  date.setHours(0, 0, 0, 0)
  if (date.getTime() == curDate.getTime()) {
    return lang.today
  }

  if (curDate.getTime() - date.getTime() <= 86400000) {
    return lang.yesterday
  }

  return date.toLocaleString(lang.dateLocale, {
    day: '2-digit',
    month: 'long',
    year: curDate.getFullYear() == date.getFullYear() ? undefined : 'numeric',
  })
}

function formatDate(lang: Language, d: number): string {
  const day = getDate(lang, new Date(d))
  const date = new Date(d)
  return `${day}, ${formatTime(date)}`
}

function buildRecvs(lang: Language, u: ApiUser[]): string {
  let r = [lang.you]
  u.slice(0, 3).forEach((e) => r.push(displayName(lang, e)))
  return r.join(', ')
}

const Message: Component = function () {
  const lang = useGlobal().lang

  const params = useParams()
  const msgId = () => params.id
  const [msg] = createResource(msgId, () => loadMsg(msgId()))

  const [read, setRead] = createSignal(false)
  createEffect(() => setRead(msg()?.r ?? false))

  const [bookmarked, setBookmarked] = createSignal(false)
  createEffect(() => setBookmarked(msg()?.b ?? false))

  const authorFormatted = () => {
    const author = msg()?.a
    if (author === undefined) return ''
    return displayName(lang, author)
  }

  const [bodyRef, setBodyRef] = createSignal<HTMLDivElement | null>(null)
  createEffect(() => {
    let node = bodyRef()
    let body = msg()?.te
    if (node && body) {
      while (node.lastChild) node.removeChild(node.lastChild)
      purifyInto(body, node)
    }
  })

  return (
    <>
      <div class={styles.root}>
        <div class={styles.titleRoot}>
          <div class={styles.title}>{msg()?.t}</div>
          <div class={styles.categoryRoot}>
            <Category category={msg()?.fl ?? -1} />
            {lang.flag[msg()?.fl ?? -1]}
          </div>
        </div>
        <div
          class={styles.infoContainer}
          classList={{ [ReadButton.hoverStyle]: true }}
        >
          <ReadButton id={msg()?.id ?? -1} onChange={setRead} read={read()} />
          <Avatar id={msg()?.a.a ?? -1} class={styles.avatar} />
          <div class={styles.authorContainer}>
            <div class={styles.authorMain}>
              {authorFormatted()}
              <div class={styles.date}>{formatDate(lang, msg()?.d ?? 0)}</div>
              <Show when={msg()?.i}>
                <ImportantIcon class={styles.importantIcon} />
              </Show>
              <div
                class={styles.bookmarker}
                classList={{
                  [styles.hideBookmarker]: !bookmarked(),
                }}
              >
                <Bookmarker
                  bookmarked={bookmarked()}
                  setBookmarked={setBookmarked}
                  id={msg()?.id ?? -1}
                />
              </div>
            </div>
            <div class={styles.targets}>
              {lang.mailTo}: {buildRecvs(lang, msg()?.to ?? [])}{' '}
              <Show when={(msg()?.to.length ?? 0) > 3}>
                <span
                  class={styles.underlined}
                  title={msg()
                    ?.to.slice(3)
                    .map((e) => displayName(lang, e))
                    .join(', ')}
                >
                  {lang.recipientsPrefix}
                  {(msg()?.to.length ?? 3) - 3} {lang.recipientsPostfix}
                </span>
              </Show>
            </div>
          </div>
        </div>
        <Show when={(msg()?.do ?? -1) != -1}>
          <div class={styles.attachment}>
            <Attachment id={msg()?.do ?? -1} />
            <div class={styles.fileNum}>
              1 {lang.file}
              <a
                class={styles.download}
                href={'/file/' + msg()?.do}
                download={true}
              >
                {lang.download}{' '}
                <span class={styles.secondary}>
                  ({((msg()?.ds ?? 0) / 1024 / 1024).toFixed(2)}Mb)
                </span>
              </a>
            </div>
          </div>
        </Show>

        <div class={styles.bodyText} ref={(ref) => setBodyRef(ref)}></div>
      </div>
      <div class={styles.spacer} />
    </>
  )
}

export default Message
