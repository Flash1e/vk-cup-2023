import { useGlobal } from '../../GlobalContext'
import Popup from '../common/Popup'
import styles from './NewLetter.module.css'
import CloseIcon from './assets/Close.svg?component-solid'
import { createSignal, Show, Component, createEffect } from 'solid-js'
import Button from '../common/Button'
import { ApiMsg } from '../../../common/api'
import { purifyChildren, purifyInto } from '../common/purify'
import Loader from '../common/Loader'
import Attachment from '../common/Attachment'
import AttachmentIcon from '../folder/assets/attachment.svg?component-solid'
import UndoIcon from './assets/undo.svg?component-solid'
import AlLeftIcon from './assets/alLeft.svg?component-solid'
import AlCenterIcon from './assets/alCenter.svg?component-solid'
import NumListIcon from './assets/numList.svg?component-solid'
import MarkerListIcon from './assets/markerList.svg?component-solid'
import LinkIcon from './assets/link.svg?component-solid'

function parseTarget(senders: string) {
  return senders
    .split(/[\s,;]+/g)
    .map((e) => e.trim())
    .filter((t) => t != '')
}

async function uploadFile(file: File) {
  let res = await fetch('/api/upload', {
    method: 'POST',
    headers: {
      'content-type': file.type,
    },
    body: file,
  })
  return parseInt(await res.text())
}

const InlineEffect: Component<{
  cssProp: string
  cssVal: string[]
  action: string
  symb: string
}> = (p) => {
  return (
    <Button
      onClick={(e) => {
        e.preventDefault()
        document.execCommand(p.action)
      }}
      style={{
        [p.cssProp]: p.cssVal[0],
      }}
    >
      {p.symb}
    </Button>
  )
}

const isMacLike = /(Mac|iPhone|iPod|iPad)/i.test(navigator.platform)

const NewLetter = () => {
  const globalCtx = useGlobal()
  const lang = globalCtx.lang
  const [showNewLetter, setShowNewLetter] = globalCtx.showNewLetter
  const [bodyEmpty, setBodyEmpty] = createSignal(true)
  let inputRef: HTMLDivElement | undefined

  const [targetF, setTargetF] = createSignal('')
  const [titleF, setTitleF] = createSignal('')
  const [attach, setAttach] = createSignal(-1)

  const [errText, setErrText] = createSignal('')

  const [loading, setLoading] = createSignal(0)
  const [htmlMode, setHtmlMode] = createSignal(false)
  const [htmlContents, setHtmlContents] = createSignal('')

  const [showCreateLink, setShowCreateLink] = createSignal(false)
  const [linkText, setLinkText] = createSignal('')
  const [linkUrl, setLinkUrl] = createSignal('')
  let urlInputRef: HTMLInputElement | undefined

  return (
    <Popup
      show={showNewLetter()}
      onDismiss={() => setShowNewLetter(false)}
      class={styles.popupRoot}
      onClick={() => setShowNewLetter(false)}
      onDrop={(e) => {
        const file = e.dataTransfer?.files[0]
        if (file) {
          e.preventDefault()
          setLoading((l) => l + 1)
          uploadFile(file).then((id) => {
            setLoading((l) => l - 1)
            setAttach(id)
          })
        }
      }}
    >
      <div
        class={styles.root}
        onClick={(e) => e.stopPropagation()}
        onKeyDown={(e) => {
          let modifKey = e.ctrlKey
          if (isMacLike) modifKey = e.metaKey
          if (modifKey) {
            let prevEvent = true
            let key = e.key
            if (key == 'b') document.execCommand('bold')
            else if (key == 'i') document.execCommand('italic')
            else if (key == 'u') document.execCommand('underline')
            else if (key == '%') document.execCommand('strikethrough')
            else if (key == 'L') document.execCommand('justifyLeft')
            else if (key == 'E') document.execCommand('justifyCenter')
            else if (key == 'R') document.execCommand('justifyRight')
            else if (key == '&') document.execCommand('insertOrderedList')
            else if (key == '*') document.execCommand('insertUnorderedList')
            else if (key == 'k') {
              if (!showCreateLink())
                requestAnimationFrame(() => urlInputRef?.focus())
              setShowCreateLink((a) => !a)
              setLinkUrl('')
              setLinkText('')
            } else prevEvent = false
            if (prevEvent) e.preventDefault()
          }
        }}
      >
        <div class={styles.inp}>
          <div class={styles.inpTitle}>{lang.subject}:</div>
          <input
            class={styles.inpInput}
            value={titleF()}
            onChange={(e) => {
              setTitleF((e.target as HTMLInputElement).value)
            }}
          ></input>
          <CloseIcon onClick={() => setShowNewLetter(false)} />
        </div>
        <div class={styles.inp}>
          <div class={styles.inpTitle}>{lang.toWho}:</div>
          <input
            class={styles.inpInput}
            value={targetF()}
            onChange={(e) => {
              setTargetF((e.target as HTMLInputElement).value)
            }}
          ></input>
        </div>
        <div class={styles.mainBar}>
          <Show when={!htmlMode()}>
            <Button
              onClick={(e) => {
                e.preventDefault()
                document.execCommand('undo')
              }}
            >
              <UndoIcon />
            </Button>
            <Button
              onClick={(e) => {
                e.preventDefault()
                document.execCommand('redo')
              }}
              style={{
                transform: 'scale(-1, 1)',
              }}
            >
              <UndoIcon />
            </Button>

            <InlineEffect
              symb="B"
              action="bold"
              cssProp="font-weight"
              cssVal={['700', 'bold']}
            />
            <InlineEffect
              symb="I"
              action="italic"
              cssProp="font-style"
              cssVal={['italic']}
            />
            <InlineEffect
              symb="U"
              action="underline"
              cssProp="text-decoration"
              cssVal={['underline']}
            />
            <InlineEffect
              symb="S"
              action="strikethrough"
              cssProp="text-decoration"
              cssVal={['line-through']}
            />

            <Button
              onClick={(e) => {
                e.preventDefault()
                document.execCommand('justifyLeft')
              }}
            >
              <AlLeftIcon />
            </Button>
            <Button
              onClick={(e) => {
                e.preventDefault()
                document.execCommand('justifyCenter')
              }}
            >
              <AlCenterIcon />
            </Button>
            <Button
              onClick={(e) => {
                e.preventDefault()
                document.execCommand('justifyRight')
              }}
              style={{
                transform: 'scale(-1, 1)',
              }}
            >
              <AlLeftIcon />
            </Button>
            <Button
              onClick={(e) => {
                e.preventDefault()
                document.execCommand('insertOrderedList')
              }}
            >
              <NumListIcon />
            </Button>
            <Button
              onClick={(e) => {
                e.preventDefault()
                document.execCommand('insertUnorderedList')
              }}
            >
              <MarkerListIcon />
            </Button>
            <div>
              <Button
                onClick={(e) => {
                  e.preventDefault()
                  setShowCreateLink((a) => !a)
                }}
              >
                <LinkIcon />
              </Button>
              <Popup
                show={showCreateLink()}
                onDismiss={() => {
                  if (!showCreateLink())
                    requestAnimationFrame(() => urlInputRef?.focus())
                  setShowCreateLink(false)

                  setLinkUrl('')
                  setLinkText('')
                }}
              >
                <div class={styles.linkPopup}>
                  {lang.link}
                  <input
                    value={linkUrl()}
                    ref={urlInputRef}
                    onChange={(e) =>
                      setLinkUrl((e.target as HTMLInputElement).value)
                    }
                  />
                  {lang.text}
                  <input
                    value={linkText()}
                    onChange={(e) =>
                      setLinkText((e.target as HTMLInputElement).value)
                    }
                  />
                  <Button
                    onClick={(e) => {
                      let link = document.createElement('a')
                      let tarUrl = linkUrl()
                      if (tarUrl.indexOf('http') != 0)
                        tarUrl = 'https://' + tarUrl
                      link.setAttribute('href', tarUrl)
                      link.innerText = linkText()
                      inputRef?.focus()
                      document.execCommand(
                        'insertHtml',
                        undefined,
                        link.outerHTML
                      )
                      setShowCreateLink(false)
                    }}
                    class={styles.createLnkBtn + ' ' + styles.primBtn}
                  >
                    {lang.ceateLink}
                  </Button>
                </div>
              </Popup>
            </div>
          </Show>
          <Button
            class={styles.htmlMode}
            onClick={(e) =>
              setHtmlMode((prevMode) => {
                if (prevMode) {
                  if (inputRef) {
                    while (inputRef.lastChild)
                      inputRef.removeChild(inputRef.lastChild)
                    purifyInto(htmlContents(), inputRef)
                  }
                } else {

                  if(inputRef) {
                    purifyChildren(inputRef)
                    setHtmlContents(inputRef?.innerHTML ?? '')
                  }
                }
                return !prevMode
              })
            }
            active={htmlMode()}
          >
            {lang.htmlMode}
          </Button>
        </div>
        <textarea
          onChange={(e) =>
            setHtmlContents((e.target as HTMLInputElement).value)
          }
          value={htmlContents()}
          style={{
            flex: htmlMode() ? 1 : 0,
            opacity: htmlMode() ? 1 : 0,
            resize: 'none',
          }}
        />
        <div
          contentEditable={true}
          data-placeholder={lang.atLeastSmth}
          class={styles.bodyInput}
          classList={{
            [styles.emptyBody]: bodyEmpty(),
          }}
          style={{
            flex: htmlMode() ? 0 : 1,
          }}
          onInput={() => {
            if (inputRef) {
              setHtmlContents(inputRef?.innerHTML)
              setBodyEmpty(inputRef?.innerHTML == '')
            }
          }}
          ref={(r) => {
            inputRef = r
            if (r) {
              r.innerHTML = htmlContents()
            }
          }}
        ></div>
        <Show when={attach() != -1}>
          <Attachment id={attach()} />
        </Show>
        <div class={styles.finBar}>
          <Show when={loading() == 0} fallback={() => <Loader size="36px" />}>
            <Button
              class={styles.primBtn}
              style={{
                width: '160px',
              }}
              onClick={() => {
                let targets = parseTarget(targetF())
                if (targets.length == 0) {
                  setErrText(lang.oneRecipientErr)
                  return
                }
                if (inputRef) purifyChildren(inputRef)

                const apiMsg: ApiMsg = {
                  id: 0, // filled on server
                  a: {
                    // filled on server
                    n: '',
                    s: '',
                    e: '',
                    a: -1,
                  },
                  to: targets.map((e) => ({
                    n: '',
                    s: '',
                    e,
                    a: -1,
                  })),
                  t: titleF(),
                  te: inputRef?.innerHTML ?? '',
                  st: inputRef?.innerText.substring(0, 256) ?? '',
                  b: false,
                  i: false,
                  r: true,

                  f: -1, // filled on server
                  d: 0, // filled on server
                  do: attach(),
                  ds: 0, // filled on server
                  fl: -1,
                }
                setLoading((l) => l + 1)
                setErrText('')
                fetch('/api/send', {
                  method: 'POST',
                  headers: {
                    'Content-Type': 'application/json',
                  },
                  body: JSON.stringify(apiMsg),
                }).then((e) => {
                  document.dispatchEvent(new CustomEvent('refresh-new-folder'))
                  setLoading((l) => l - 1)
                  setShowNewLetter(false)
                  setTargetF('')
                  setTitleF('')
                  setHtmlContents('')
                  setAttach(-1)
                })
              }}
            >
              {lang.send}
            </Button>
          </Show>
          <Button
            class={styles.attachBtn}
            onClick={() => {
              const input = document.createElement('input')
              input.type = 'file'

              input.onchange = (e) => {
                const target = e?.target as HTMLInputElement
                const file = (target.files ?? [])[0]
                if (file) {
                  setLoading((l) => l + 1)
                  uploadFile(file).then((id) => {
                    setLoading((l) => l - 1)
                    setAttach(id)
                  })
                }
              }

              input.click()
            }}
          >
            <AttachmentIcon />
          </Button>
          <span class={styles.errText}>{errText()}</span>
        </div>
      </div>
    </Popup>
  )
}

export default NewLetter
