import { Component, JSX, mergeProps, splitProps } from 'solid-js'

import styles from './Button.module.css'

interface ButtonProps extends JSX.ButtonHTMLAttributes<HTMLButtonElement> {
  active?: boolean
}

const Button: Component<ButtonProps> = (props) => {
  const propsMerged = mergeProps({ active: false }, props)
  const [p, a] = splitProps(propsMerged, ['active', 'children', 'classList'])


  return (
    <button
      classList={{
        [styles.root]: true,
        [styles.active]: p.active,
        ...p.classList,
      }}
      {...a}
    >
      {p.children}
    </button>
  )
}

export default Button
