import { Component, Show } from 'solid-js'
import styles from './Avatar.module.css'

const Avatar: Component<{ id: number; class?: string }> = (props) => (
  <div class={styles.avatarContainer + ' ' + (props.class ?? '')}>
    <Show when={props.id != -1}>
      <img
        class={styles.avatar}
        width="32"
        height="32"
        src={'/file/' + props.id}
      />
    </Show>
  </div>
)

export default Avatar
