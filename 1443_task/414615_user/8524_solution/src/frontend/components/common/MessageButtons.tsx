import { Component, Setter, Show } from 'solid-js'
import styles from './MessageButtons.module.css'

import BookmarkFullIcon from './assets/bookmark_full.svg?component-solid'
import BookmarkIcon from './assets/bookmark.svg?component-solid'
import ImportantIcon from './assets/important.svg?component-solid'

interface ReadButtonProps {
  id: number
  onChange: (newVal: boolean) => void
  read: boolean
}

export const ReadButton: Component<ReadButtonProps> & { hoverStyle: string } = (
  props
) => {
  return (
    <div
      class={styles.newMsgContainer}
      onClick={(e) => {
        const readNew = !props.read
        props.onChange(readNew)
        fetch(`/api/setRead/${props.id}?${readNew}`)

        e.stopPropagation()
      }}
    >
      <div
        class={styles.newMsg}
        classList={{
          [styles.unread]: !props.read,
        }}
      />
    </div>
  )
}

ReadButton.hoverStyle = styles.newMsgHover

export const MsgMarker: Component<{
  important: boolean
  bookmarked: boolean
  singleIcon?: boolean
}> = (props) => {
  const shouldShowImportant = () => {
    if (!props.singleIcon) return props.important
    if (props.bookmarked) return false
    return props.important
  }

  return (
    <>
      <Show when={shouldShowImportant()}>
        <ImportantIcon />
      </Show>
      <Show when={props.bookmarked}>
        <BookmarkFullIcon />
      </Show>
    </>
  )
}

export const Bookmarker: Component<{
  id: number
  bookmarked: boolean
  setBookmarked: Setter<boolean>
}> = (props) => {
  return (
    <div
      class={styles.btn}
      onClick={(e) => {
        const bookmark = !props.bookmarked
        props.setBookmarked(bookmark)
        fetch(`/api/setBookmarked/${props.id}?${bookmark}`)
        e.stopPropagation()
      }}
    >
      <Show when={!props.bookmarked}>
        <BookmarkIcon />
      </Show>
      <Show when={props.bookmarked}>
        <BookmarkFullIcon />
      </Show>
    </div>
  )
}
