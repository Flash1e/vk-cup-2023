import { Component } from 'solid-js'
import styles from './Loader.module.css'

const Loader: Component<{ size?: string }> = (p) => {
  return (
    <div
      class={styles.loader}
      style={{
        width: p.size ?? '48px',
        height: p.size ?? '48px',
      }}
    ></div>
  )
}

export default Loader
