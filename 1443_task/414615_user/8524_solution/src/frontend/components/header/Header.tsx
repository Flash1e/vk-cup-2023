import { Component, createEffect, createSignal, Show } from 'solid-js'

import styles from './Header.module.css'
import Logo from './assets/logo.svg?component-solid'
import Text from './assets/text.svg?component-solid'
import BackArrow from './assets/backarrow.svg?component-solid'
import { useHref, useLocation } from '@solidjs/router'
import { useGlobal } from '../../GlobalContext'
import Button from '../common/Button'

const Header: Component = () => {
  const location = useLocation<Location>()
  const globalCtx = useGlobal()
  const lang = globalCtx.lang
  const [headerToolRef, setHeaderToolRef] = globalCtx.headerToolRef
  const back = () => (location.pathname.indexOf('message') ?? -1) != -1

  return (
    <div class={styles.root}>
      <div class={styles.ico}>
        <Show when={!back()}>
          <Logo class={styles.icoTitle} />
        </Show>
        <Show when={back()}>
          <Button
            onClick={(e) => {
              if (back()) history.back()
            }}
          >
            <BackArrow />
            <span class={styles.title}>{lang.back}</span>
          </Button>
        </Show>
      </div>
      <div class={styles.filler}></div>
      <div class={styles.headerTool} ref={(ref) => setHeaderToolRef(ref)}></div>
    </div>
  )
}

export default Header
