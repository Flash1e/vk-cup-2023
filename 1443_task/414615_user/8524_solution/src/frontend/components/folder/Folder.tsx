import {
  Component,
  createEffect,
  createSignal,
  For,
  ParentComponent,
  Show,
  Switch,
  Match,
  onMount,
} from 'solid-js'
import { Dynamic, Portal } from 'solid-js/web'
import { useParams } from '@solidjs/router'

import { useGlobal } from '../../GlobalContext'
import Message, { SelectionEl } from './Message'
import { ApiFolders, ApiMsgTitle } from '../../../common/api'
import styles from './Folder.module.css'
import UnfoldIcon from './assets/unfold.svg?component-solid'
import SelectedIcon from './assets/selected.svg?component-solid'
import Popup from '../common/Popup'
import Button from '../common/Button'

import BookmarkFullIcon from '../common/assets/bookmark_full.svg?component-solid'
import AttachmentIcon from './assets/attachment.svg?component-solid'
import { Language } from '../../lang'

import EmptyFolderLight from './assets/empty_folder_light.png'
import EmptyFolderDark from './assets/empty_folder_dark.png'
import EmptyFolderDefault from './assets/empty_folder.svg?component-solid'
import Loader from '../common/Loader'
import { FolderIcon, LogoMap } from '../toolbar/Toolbar'

async function loadFolder(
  folder: string,
  filters: string,
  start_id: number
): Promise<ApiMsgTitle[]> {
  let url = new URL('/api/folder', location.href)
  url.searchParams.set('fid', folder)
  url.searchParams.set('filters', filters)
  url.searchParams.set('sid', start_id.toString())
  const res = await fetch(url.href)
  return await res.json()
}

interface FilterRowProps {
  value: boolean
  onChange: (a: boolean) => void
}

const FilterRow: ParentComponent<FilterRowProps> = (props) => {
  return (
    <Button
      class={styles.filterRow}
      onClick={() => props.onChange(!props.value)}
    >
      <SelectedIcon
        style={{ opacity: props.value ? '1' : '0', 'margin-right': '-4px' }}
      />
      {props.children}
    </Button>
  )
}

function mapFlagsToText(
  lang: Language,
  unread: boolean,
  marked: boolean,
  attach: boolean
) {
  let enabledCount = 0
  if (unread) enabledCount++
  if (marked) enabledCount++
  if (attach) enabledCount++

  if (enabledCount == 0) return lang.filter
  if (enabledCount == 1) {
    if (unread) return lang.unread
    if (marked) return lang.marked
    if (attach) return lang.attach
  }

  return lang.filters
}

const Folder: Component = () => {
  const params = useParams()
  const globalCtx = useGlobal()
  const lang = globalCtx.lang
  const [folder, setFolder] = globalCtx.folder
  const [headerToolRef, setHeaderToolRef] = globalCtx.headerToolRef
  const [theme, setTheme] = globalCtx.theme
  const [showFilterTool, setShowFilterTool] = createSignal(false)

  const [filterUnread, setFilterUnread] = createSignal(false)
  const [filterMarked, setFilterMarked] = createSignal(false)
  const [filterAttach, setFilterAttach] = createSignal(false)

  const noFilter = () => !(filterAttach() || filterUnread() || filterMarked())

  createEffect(() => {
    setFolder(parseInt(params.folder))
  })

  const filterString = () => {
    let filterString = ''
    if (filterUnread()) filterString += 'u'
    if (filterMarked()) filterString += 'm'
    if (filterAttach()) filterString += 'a'
    return filterString
  }

  const [data, setData] = createSignal<ApiMsgTitle[]>([])
  const [loading, setLoading] = createSignal(false)
  const [loadMore, setLoadMore] = createSignal(false)
  const [allLoaded, setAllLoaded] = createSignal(false)

  const [prevState, setPrevState] = createSignal('')
  // Reset on filter and folder changes
  createEffect(() => {
    let curState = filterString() + folder()
    setTimeout(moreData, 100)
  })

  let lazyMarkerRef: HTMLDivElement | undefined = undefined
  const moreData = () => {
    let curState = filterString() + folder()
    if (loading()) return
    if (prevState() == '') {
      setPrevState(curState)
    }

    if (curState == prevState()) {
      if (!loadMore()) return
      if (allLoaded()) return
    } else {
      setSelected([])
      setData([])
    }

    let last_id = -0xfe
    let curData = data()
    if (curData.length > 0) last_id = curData[curData.length - 1].id
    setLoading(true)
    loadFolder(folder().toString(), filterString(), last_id - 1).then((d) => {
      if (filterString() + folder() == curState) {
        setAllLoaded(d.length == 0)
        setData([...data(), ...(d ?? [])])

        setPrevState(curState)
      }
      setLoading(false)
      setTimeout(moreData, 100)
    })
  }
  onMount(() => {
    const observer = new IntersectionObserver(
      (a) => {
        setLoadMore(a[0].isIntersecting)
        if (a[0].isIntersecting) setTimeout(moreData, 100)
      },
      {
        threshold: 0.2,
      }
    )
    if (lazyMarkerRef) observer.observe(lazyMarkerRef)
    document.addEventListener('refresh-new-folder', () => {
      let curState = filterString() + folder()
      loadFolder(folder().toString(), filterString(), -0xff).then((d) => {
        if (filterString() + folder() == curState) {
          let newData = [...data(), ...(d ?? [])]
          newData = newData
            .filter((e, i) => newData.findIndex((x) => x.id == e.id) == i)
            .sort((a, b) => b.id - a.id)
          setData(newData)
        }
      })
    })
  })

  const [selected, setSelected] = createSignal([] as SelectionEl[])
  const [moveToShow, setMoveToShow] = createSignal(false)
  const [customFolders, setCustomFolders] = createSignal([] as string[])

  return (
    <>
      <Show
        when={(data() ?? []).length > 0}
        fallback={() => (
          <div class={styles.emptyCont}>
            <Show when={!loading()} fallback={() => <Loader />}>
              <Switch fallback={<EmptyFolderDefault />}>
                <Match when={theme() == 'light'}>
                  <img src={EmptyFolderLight} />
                </Match>
                <Match when={theme() == 'dark'}>
                  <img src={EmptyFolderDark} />
                </Match>
              </Switch>
              {lang.emptyFolder}
            </Show>
          </div>
        )}
      >
        <div class={styles.msgsRoot}>
          <For each={data() ?? []}>
            {(e) => (
              <Message
                msg={e}
                onSelectChange={(s, o) => {
                  if (s) {
                    setSelected([...selected(), o])
                  } else {
                    const filtered = selected().filter((x) => x.id != o.id)
                    setSelected([...filtered])
                  }
                  console.log(s, o, selected().length)
                }}
              />
            )}
          </For>
        </div>
        <div class={styles.spacer}>
          <Show when={loading()}>
            <Loader />
          </Show>
        </div>
      </Show>

      <div class={styles.lazyMarker} ref={lazyMarkerRef} />

      <Portal mount={headerToolRef()}>
        <Button
          active={false}
          onClick={() => setShowFilterTool((x) => !x)}
          class={styles.filterTool}
        >
          <Show when={filterAttach()}>
            <AttachmentIcon />
          </Show>
          <Show when={filterMarked()}>
            <BookmarkFullIcon />
          </Show>
          <Show when={filterUnread()}>
            <div class={styles.unreadIcon} />
          </Show>
          {mapFlagsToText(lang, filterUnread(), filterMarked(), filterAttach())}
          <UnfoldIcon />
        </Button>
        <Portal>
          <Popup
            show={showFilterTool()}
            onDismiss={() => setShowFilterTool(false)}
          >
            <div class={styles.filterPopup}>
              <FilterRow
                value={noFilter()}
                onChange={(x) => {
                  setFilterUnread(!x)
                  setFilterMarked(!x)
                  setFilterAttach(!x)
                }}
              >
                {lang.allLet}
              </FilterRow>

              <FilterRow
                value={filterUnread()}
                onChange={(x) => setFilterUnread(x)}
              >
                <div class={styles.unreadIcon} />
                {lang.unread}
              </FilterRow>

              <FilterRow
                value={filterMarked()}
                onChange={(x) => setFilterMarked(x)}
              >
                <BookmarkFullIcon />
                {lang.marked}
              </FilterRow>

              <FilterRow
                value={filterAttach()}
                onChange={(x) => setFilterAttach(x)}
              >
                <AttachmentIcon />
                {lang.attach}
              </FilterRow>
            </div>
          </Popup>
        </Portal>
      </Portal>
      <div
        class={styles.selectedOp}
        style={{
          right: selected().length == 0 ? '-600px' : '24px',
        }}
      >
        <Button
          onClick={() => {
            let op = {
              type: 'setRead',
              goal: !selected()[0].read(),
            }
            selected().forEach((x) =>
              x.el.dispatchEvent(
                new CustomEvent('manip-msg', {
                  detail: {
                    onlySelected: true,
                    action: op,
                  },
                })
              )
            )
          }}
        >
          {selected()[0]?.read() ? lang.unreadOp : lang.readOp}
        </Button>
        <Button
          onClick={() => {
            let op = {
              type: 'setMark',
              goal: !selected()[0].marked(),
            }
            selected().forEach((x) =>
              x.el.dispatchEvent(
                new CustomEvent('manip-msg', {
                  detail: {
                    onlySelected: true,
                    action: op,
                  },
                })
              )
            )
          }}
        >
          {selected()[0]?.marked() ? lang.unbookmarkOp : lang.bookmarkOp}
        </Button>
        <Button
          onClick={() =>
            setMoveToShow((x) => {
              if (!x) {
                fetch('/api/customFolders')
                  .then((e) => e.json())
                  .then((f) => setCustomFolders(f))
              }
              return !x
            })
          }
        >
          {lang.moveTo}
        </Button>
        <Popup
          show={moveToShow()}
          onDismiss={() => setMoveToShow(false)}
          class={styles.moveToPop}
        >
          <For each={ApiFolders}>
            {(e, i) => (
              <Button
                onClick={() => {
                  let op = {
                    type: 'move',
                    targetF: i(),
                  }
                  selected().forEach((x) =>
                    x.el.dispatchEvent(
                      new CustomEvent('manip-msg', {
                        detail: {
                          onlySelected: true,
                          action: op,
                        },
                      })
                    )
                  )
                  setMoveToShow(false)
                }}
              >
                <Dynamic component={LogoMap[e]} />
                {lang.folderNames[i()]}
              </Button>
            )}
          </For>
          <For each={customFolders()}>
            {(e, i) => (
              <Button
                onClick={() => {
                  let op = {
                    type: 'move',
                    targetF: 10 + i(),
                  }
                  selected().forEach((x) =>
                    x.el.dispatchEvent(
                      new CustomEvent('manip-msg', {
                        detail: {
                          onlySelected: true,
                          action: op,
                        },
                      })
                    )
                  )
                  setMoveToShow(false)
                }}
              >
                <FolderIcon />
                {e}
              </Button>
            )}
          </For>
        </Popup>
      </div>
    </>
  )
}

export default Folder
