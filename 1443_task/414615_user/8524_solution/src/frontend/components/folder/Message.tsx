import { useNavigate } from '@solidjs/router'
import { Component, createEffect, createSignal, Show } from 'solid-js'
import { ApiMsgTitle } from '../../../common/api'
import styles from './Message.module.css'
import Category from '../common/Category'

import AttachmentIcon from './assets/attachment.svg?component-solid'
import TickIcon from './assets/tick.svg?component-solid'
import Attachment from '../common/Attachment'
import { Bookmarker, MsgMarker, ReadButton } from '../common/MessageButtons'
import Avatar from '../common/Avatar'
import Popup from '../common/Popup'
import { Language } from '../../lang'
import { useGlobal } from '../../GlobalContext'
import { displayName, formatTime } from '../common/format'

function formatDateToString(lang: Language, date: Date): string {
  // const curDate = new Date('Sun Nov 27 2022 22:24:36 GMT+0300')
  const curDate = new Date()
  const diff = curDate.getTime() - date.getTime()

  if (
    date.getDate() == curDate.getDate() &&
    date.getMonth() == curDate.getMonth() &&
    date.getFullYear() == curDate.getFullYear()
  ) {
    return formatTime(date)
  }

  // If diff is less than ~11 months show short date
  if (diff > 0 && diff < 330 * 24 * 60 * 60 * 1000) {
    return date
      .toLocaleString(lang.dateLocale, {
        day: '2-digit',
        month: 'short',
      })
      .slice(0, 6)
  }

  return date.toLocaleString(lang.dateLocale, {
    // day: '2-digit',
    month: '2-digit',
    year: 'numeric',
  })
}

export interface MsgManipDetails {
  onlySelected: boolean
  action:
    | {
        type: 'move'
        targetF: number
      }
    | {
        type: 'setRead'
        goal: boolean
      }
    | {
        type: 'setMark'
        goal: boolean
      }
}

export interface SelectionEl {
  id: number
  el: HTMLButtonElement
  read: () => boolean
  marked: () => boolean
}

const Message: Component<{
  msg: ApiMsgTitle
  onSelectChange: (a: boolean, o: SelectionEl) => void
}> = (props) => {
  const navigate = useNavigate()
  const lang = useGlobal().lang

  const [attachmentVisible, setAttachmentVisible] = createSignal(false)
  const [bookmarked, setBookmarked] = createSignal(false)
  createEffect(() => setBookmarked(props.msg.b))

  const [read, setRead] = createSignal(false)
  createEffect(() => setRead(props.msg.r))

  const [selected, setSelected] = createSignal(false)
  const [hide, setHide] = createSignal(false)
  createEffect((previd) => {
    if (previd != props.msg.id) {
      setHide(false)
      setSelected(false)
    }
    return previd
  }, 0)
  const msgUrl = () =>
    new URL(`/message/${props.msg.id}`, location.href).toString()

  let btnRef: HTMLButtonElement | undefined

  createEffect((prev) => {
    if (prev != selected()) {
      if (btnRef) {
        props.onSelectChange(selected(), {
          id: props.msg.id,
          el: btnRef,
          marked: bookmarked,
          read: read,
        })
      }
    }

    return selected()
  }, false)

  return (
    <Show when={!hide()}>
      <button
        draggable={true}
        data-msgurl={msgUrl()}
        onClick={() => navigate('/message/' + props.msg.id)}
        class={styles.root}
        classList={{
          [ReadButton.hoverStyle]: true,
          [styles.rootSelected]: selected(),
        }}
        onDragStart={(e) => {
          e.dataTransfer?.setData('text/plain', msgUrl())
        }}
        onDragEnd={(e) => {
          console.log(e)
        }}
        on:manip-msg={(e: CustomEvent) => {
          console.log('New event')
          e.preventDefault()
          const options = e.detail as MsgManipDetails
          if (options.onlySelected && !selected()) return
          const action = options.action
          if (action.type == 'move') {
            fetch(`/api/moveTo/${props.msg.id}/${action.targetF}`).then((e) => {
              if (props.msg.f != action.targetF) {
                setHide(true)
                setSelected(false)
              }
            })
          }
          if (action.type == 'setRead') {
            fetch(`/api/setRead/${props.msg.id}?${action.goal}`).then((e) =>
              setRead(action.goal)
            )
          }

          if (action.type == 'setMark') {
            fetch(`/api/setBookmarked/${props.msg.id}?${action.goal}`).then(
              (e) => setBookmarked(action.goal)
            )
          }
        }}
        ref={btnRef}
      >
        <div class={styles.divider} />
        <div class={styles.msgRow}>
          <ReadButton id={props.msg.id} read={read()} onChange={setRead} />
          <Avatar class={styles.hideOnSelect} id={props.msg.a.a} />
          <div
            class={styles.checkContainer}
            onClick={(e) => e.stopPropagation()}
          >
            <label class={styles.checkRoot}>
              <input
                type="checkbox"
                checked={selected()}
                onChange={(e) => {
                  setSelected((e.target as HTMLInputElement).checked)
                  e.stopPropagation()
                }}
              />
              <span class={styles.checkMain}>
                <TickIcon />
              </span>
            </label>
          </div>

          <div class={styles.author} classList={{ [styles.bold]: !read() }}>
            {displayName(lang, props.msg.a)}
          </div>
          <div class={styles.msgMarker}>
            <MsgMarker
              bookmarked={bookmarked()}
              important={props.msg.i}
              singleIcon={true}
            />
          </div>
          <div class={styles.bookmark}>
            <Bookmarker
              id={props.msg.id}
              bookmarked={bookmarked()}
              setBookmarked={setBookmarked}
            />
          </div>
          <div class={styles.text}>
            <span class={styles.title} classList={{ [styles.bold]: !read() }}>
              {props.msg.t}
            </span>
            {props.msg.st}
          </div>

          <Show when={props.msg.fl != -1 || props.msg.do != -1}>
            <div class={styles.iconCont}>
              <div class={styles.category}>
                <Category category={props.msg.fl} />
              </div>

              <Show when={props.msg.do != -1}>
                <div
                  class={styles.attach}
                  classList={{
                    [styles.btn]: true,
                    [styles.btnPressed]: attachmentVisible(),
                  }}
                  onClick={(e) => {
                    setAttachmentVisible(!attachmentVisible())
                    e.stopPropagation()
                  }}
                >
                  <Popup
                    show={attachmentVisible()}
                    onDismiss={() => setAttachmentVisible(false)}
                  >
                    <div class={styles.imgpopup}>
                      <Attachment id={props.msg.do} />
                    </div>
                  </Popup>
                  <AttachmentIcon />
                </div>
              </Show>
            </div>
          </Show>

          <div
            class={styles.date}
            title={new Date(props.msg.d).toLocaleString(lang.dateLocale)}
          >
            {formatDateToString(lang, new Date(props.msg.d))}
          </div>
        </div>
      </button>
    </Show>
  )
}

export default Message
