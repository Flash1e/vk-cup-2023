'use strict';

var promises = require('fs/promises');
var http = require('http');
var path = require('path');
var zlib = require('zlib');
var crypto = require('crypto');

const ApiFolders = [
    'Входящие',
    'Важное',
    'Отправленные',
    'Черновики',
    'Архив',
    'Спам',
    'Корзина',
];
const ApiFlags = [
    'Заказы',
    'Финансы',
    'Регистрации',
    'Путешествия',
    'Билеты',
    'Штрафы и налоги',
];

console.log(__dirname);
class FileMapper {
    idToFile = new Map();
    curImgsCounter = 0;
    add(s) {
        const [infoS, dataS] = s.split(',');
        const data = Buffer.from(dataS, 'base64');
        const mimeString = infoS.split(':')[1].split(';')[0];
        return this.addRaw(data, mimeString);
    }
    addRaw(data, mime) {
        const hash = crypto.createHash('sha256')
            .update(Buffer.concat([data, Buffer.from(mime)]))
            .digest();
        let curId = 0;
        curId |= hash[0] << 0;
        curId |= hash[1] << 8;
        curId |= hash[2] << 16;
        curId |= hash[3] << 24;
        curId |= hash[4] << 32;
        curId |= hash[5] << 40;
        curId = Math.abs(curId);
        if (!this.idToFile.has(curId)) {
            this.idToFile.set(curId, [mime, data]);
        }
        return curId;
    }
    getFromId(id) {
        return this.idToFile.get(id);
    }
}
async function loadAssets() {
    const assetsFolder = path.join(__dirname, 'assets');
    const assetNames = await promises.readdir(assetsFolder);
    const assetsEntries = await Promise.all(assetNames.map(async (e) => {
        const f = await promises.readFile(path.join(assetsFolder, e));
        return [e, f];
    }));
    return Object.fromEntries(assetsEntries);
}
async function loadDb() {
    let full_db = JSON.parse(await promises.readFile(path.join(__dirname, 'db.json'), 'utf-8'));
    full_db = full_db.sort((a, b) => new Date(a.date).getTime() - new Date(b.date).getTime());
    const files = new FileMapper();
    function mapUser(user) {
        return {
            n: user.name,
            s: user.surname,
            e: user.email,
            a: user.avatar ? files.add(user.avatar) : -1,
        };
    }
    const msgs = full_db.map((e, i) => {
        const msg = {
            id: i,
            a: mapUser(e.author),
            to: e.to.map(mapUser),
            t: e.title,
            te: e.text,
            st: e.text.substring(0, 256),
            b: e.bookmark,
            i: e.important,
            r: e.read,
            f: e.folder ? ApiFolders.indexOf(e.folder) : 0,
            d: new Date(e.date).getTime(),
            do: e.doc ? files.add(e.doc.img) : -1,
            ds: 0,
            fl: ApiFlags.indexOf(e.flag ?? '-1'),
        };
        // console.log('asdasd', files.getFromId(msg.do)?.length)
        const f = files.getFromId(msg.do);
        if (f) {
            msg.ds = f[1].byteLength;
        }
        return msg;
    });
    return {
        files,
        msgs,
    };
}
function collect_body(req) {
    let chunks = [];
    let tot_len = 0;
    return new Promise((resolve, reject) => {
        req.on('data', (d) => {
            chunks.push(d);
            tot_len += d.length;
            if (tot_len > 1024 * 1024 * 32)
                // >32mb
                req.destroy();
        });
        req.on('end', () => {
            resolve(Buffer.concat(chunks));
        });
    });
}
async function main() {
    const [assets, db, mainPage] = await Promise.all([
        loadAssets(),
        loadDb(),
        promises.readFile(path.join(__dirname, 'index.html')),
    ]);
    console.log('Loaded');
    const { msgs, files } = db;
    let customFolders = [];
    http.createServer((req, res) => {
        try {
            let finish = (b) => res.write(b);
            if (req.headers['Accept-Encoding']?.indexOf('gzip') != -1) {
                finish = (b) => {
                    res.setHeader('Content-Encoding', 'gzip');
                    zlib.gzip(b, (err, c) => res.end(c));
                    return true;
                };
            }
            const url = new URL(req.url ?? '/', 'https://localhost');
            let parts = url.pathname.split('/');
            // console.log(parts.length)
            if (parts.length < 3) {
                finish(mainPage);
                return;
            }
            if (parts[1] == 'assets') {
                if (assets.hasOwnProperty(parts[2])) {
                    let mimeType = 'text/plain';
                    const ext = parts[2].split('.').pop();
                    if (ext == 'js')
                        mimeType = 'text/javascript';
                    if (ext == 'css')
                        mimeType = 'text/css';
                    if (ext == 'png')
                        mimeType = 'image/png';
                    if (ext == 'svg')
                        mimeType = 'image/svg+xml';
                    res.setHeader('Content-Type', mimeType);
                    // Assets change their name if their content is changed, infinite cache is allowed
                    res.setHeader('Cache-Control', 'public, max-age=8640000'); // 100 days
                    finish(assets[parts[2]]);
                }
                else {
                    res.statusCode = 404;
                    res.end();
                }
                return;
            }
            if (parts[1] == 'file') {
                res.setHeader('Cache-Control', 'max-age=604800'); // 7 days cache
                let id = parseInt(parts[2]);
                const f = files.getFromId(id);
                if (f) {
                    res.setHeader('Content-Type', f[0]);
                    finish(f[1]);
                }
                else {
                    res.statusCode = 404;
                    res.end();
                }
                return;
            }
            if (parts[1] == 'api') {
                res.setHeader('Content-Type', 'application/json');
                if (parts[2] == 'folder') {
                    const fid = parseInt(url.searchParams.get('fid') ?? '0'); // folder id
                    const filters = url.searchParams.get('filters');
                    const unread = filters?.indexOf('u') != -1;
                    const marked = filters?.indexOf('m') != -1;
                    const attached = filters?.indexOf('a') != -1;
                    let start_id = parseInt(url.searchParams.get('sid') ?? '0');
                    if (start_id == -0xff)
                        start_id = msgs.length - 1;
                    let collected_messages = [];
                    for (let i = start_id; i >= 0; i--) {
                        const m = msgs[i];
                        if (m.f != fid)
                            continue;
                        if (unread && m.r)
                            continue;
                        if (marked && !m.b)
                            continue;
                        if (attached && m.do == -1)
                            continue;
                        collected_messages.push({
                            id: m.id,
                            a: m.a,
                            t: m.t,
                            st: m.st,
                            b: m.b,
                            i: m.i,
                            r: m.r,
                            do: m.do,
                            d: m.d,
                            fl: m.fl,
                            f: m.f,
                        });
                        if (collected_messages.length > 24)
                            break;
                    }
                    finish(JSON.stringify(collected_messages));
                    return;
                }
                if (parts[2] == 'message') {
                    const id = parseInt(parts[3]);
                    finish(JSON.stringify(msgs[id]));
                    return;
                }
                if (parts[2] == 'setBookmarked') {
                    const id = parseInt(parts[3]);
                    msgs[id].b = url.search[1] == 't';
                    finish('{}');
                    return;
                }
                if (parts[2] == 'setRead') {
                    const id = parseInt(parts[3]);
                    msgs[id].r = url.search[1] == 't';
                    finish('{}');
                    return;
                }
                if (parts[2] == 'send') {
                    collect_body(req).then((e) => {
                        let msg = JSON.parse(e.toString());
                        const f = files.getFromId(msg.do);
                        if (f) {
                            msg.ds = f[1].byteLength;
                        }
                        msg.a = {
                            n: '',
                            s: '',
                            e: '<self>',
                            a: -1,
                        };
                        msg.id = msgs.length;
                        msg.f = 2;
                        msg.d = new Date().toString();
                        msgs.push(msg);
                        console.log('Send mesage', msg);
                        finish('{}');
                    });
                    return;
                }
                if (parts[2] == 'upload') {
                    const mimeType = req.headers['content-type'];
                    collect_body(req).then((e) => {
                        let id = files.addRaw(e, mimeType ?? 'image/png');
                        finish(id.toString());
                    });
                    return;
                }
                if (parts[2] == 'moveTo') {
                    const msgId = parseInt(parts[3]);
                    const target = parseInt(parts[4]);
                    msgs[msgId].f = target;
                    finish('{}');
                    return;
                }
                if (parts[2] == 'newFolder') {
                    collect_body(req).then((e) => {
                        const name = e.toString();
                        console.log('New folder', name);
                        customFolders.push(name);
                        finish('{}');
                    });
                    return;
                }
                if (parts[2] == 'customFolders') {
                    finish(JSON.stringify(customFolders));
                    return;
                }
                res.statusCode = 404;
                res.end();
                return;
            }
            finish(mainPage);
        }
        catch (err) {
            // throw err
            try {
                res.statusCode = 500;
                res.end();
            }
            catch (e) { }
        }
    }).listen(3000, () => {
        console.log('Server is ready on http://localhost:3000');
    });
}
main();
// let dbLoaded = false
