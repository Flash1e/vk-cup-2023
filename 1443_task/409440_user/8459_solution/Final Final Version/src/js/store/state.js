export default {
    currentLocation: 'list',
    currentLetter: null,
    letters: [],
    page: 1,
    limit: 15,
    filters: {
        'read.false': false,
        'bookmark.true': false,
        'doc.__null__': false,
    },
    langConfig: {},
}
