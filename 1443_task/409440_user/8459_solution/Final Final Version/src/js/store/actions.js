import { getLetters } from '../utils/api.js';


export default {
    async lettersChange(context, payload = {page: 1, limit: 20}) {
        let filters = context.state.filters;
        filters = Object.keys(filters).filter(key => filters[key]);

        const response = await getLetters({
            folder: context.state.currentFolder,
            ...filters.reduce((accumulator, value) => {
                value = value.split('.');
                return {...accumulator, [value[0]]: value[1]};
            }, {}),
            ...payload
        });

        context.commit('pageChange', payload.page);
        context.commit('lettersChange', response.results);
    },

    async loadMore(context) {
        const page = context.state.page + 1;
        const limit = context.state.limit;

        this.lettersChange(context, {page, limit});
    },

    letterChange(context, payload = {letter: null}) {
        context.commit('letterChange', payload.letter);
    },

    locationChange(context, payload = {location: 'list'}) {
        context.commit('locationChange', payload.location);
    },

    async folderChange(context, payload = {folder: '__all__'}) {
        context.commit('folderChange', payload.folder);

        this.locationChange(context, {
            location: 'list',
            _id: null,
        });

        await this.lettersChange(context);
    },

    async filterChange(context, payload = {filter: '__all__'}) {
        context.commit('filterChange', payload.filter);

        await this.lettersChange(context);
    },

    async languageChange(context, payload = {}) {
        const lang = document.querySelector('#settings-language input[type="radio"]:checked').value;
        const langConfig = (await import(`../config/lang/${lang}.js`)).default;

        context.commit('languageChange', langConfig);
    }
};