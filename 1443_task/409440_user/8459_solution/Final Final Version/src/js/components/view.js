import Component from './lib/component.js';
import store from '../store/index.js';

import { formatDateLong, round, getFileSize } from '../utils/utils.js';

export default class View extends Component {
    constructor() {
        super({
            store,
            element: document.querySelector('#view'),
        });
    }

    render() {
        this.element.innerHTML = "";  // Clear the list

        if (store.state.currentLocation !== 'view') 
            return;

        const letter = store.state.currentLetter;

        if (!letter) return;

        this.element.innerHTML = `
        <header class="view-header">
            <h1 class="text-weight-bold">
                ${letter.title}
            </h1>

            ${ "flag" in letter ? 
                `<div
                    class="view-category-content-wrapper"
                >
                    <div class="view-category-content">
                        <span class="ico ico-24">
                            <i class="icon-${letter.flag.toLowerCase().replace(/ /g,"-")}"></i>
                        </span>
                        <span
                            class="text text-color-primary"
                            data-i18n="view.flag-${letter.flag.toLowerCase()}"
                        >v
                            ${letter.flag}
                        </span>
                    </div>
                </div>` : 
                ""
            }
        </header>
        <div class="view-head">
            <div class="unread-dot">
                <div read="${letter.read}" class="read-dot"></div>
            </div>
            <div class="view-head-content">
                <div class="avatar">
                    <img 
                        src=${letter.author.avatar || "assets/default-avatar.png"}
                        alt="avatar"
                    />
                </div>
                <div class="view-head-info">
                    <div class="view-head-info--title">
                        <p class="text text-color-primary">
                            ${letter.author.name} ${letter.author.surname}
                        </p>
                        <p class="date text text-type-footnote text-color-secondary">
                            ${formatDateLong(letter.date)}
                        </p>
                        <span class="ico ico-20">
                            ${
                                letter.important ? 
                                `<i class="icon-exclamation-mark red"></i>` : 
                                letter.bookmark ? 
                                `<i class="icon-bookmark-solid red"></i>` :
                                ""
                            }
                        </span>
                    </div>
                    <p class="text text-type-footnote text-color-secondary">
                        <span data-i18n="view.to-whom">Кому: Вы</span>
                        ${
                            letter.to.slice(0, 3).map((to) => {
                                return `<span>, ${to.name} ${to.surname}</span>`;
                            }).join("")
                        }
                        ${
                            letter.to.length > 3 ? 
                            `<span class="text-underline">
                                <span data-i18n="view.and">еще</span> ${letter.to.length - 3} <span data-i18n="view.recipients">получателей</span> 
                            </span>` : ""
                        }
                    </p>
                </div>
            </div>
        </div>
        ${ "doc" in letter ? 
            `<div ${"doc" in letter ? "" : "hidden"} class="view-attaches">
                <div class="view-attaches-content">
                    <div class="view-images">
                        ${
                            Object.entries(letter.doc).map(([_key, value]) => {
                                return `
                                    <div class="view-image">
                                        <img src="${value}" alt="image" />
                                        <div class="view-image-download text-color-primary">
                                            <span class="ico ico-16">
                                                <i class="icon-download"></i>
                                            </span>
                                            <a
                                                href="#"
                                                class="text"
                                                data-i18n="view.download-button"
                                            >
                                                Скачать
                                            </a>
                                        </div>
                                    </div>
                                `;
                            }).join("")
                        }
                    </div>
                    <p class="view-action text text-type-footnote">
                        <span>
                            ${Object.keys(letter.doc).length} <span data-i18n="view.files">файл(а)</span>
                        </span>
                        <span>
                            <a
                                href="#"
                                class="text-color-select"
                                data-i18n="view.download-button"
                            >
                                Скачать
                            </a>
                            &nbsp;
                            <span class="text-color-secondary">
                                (${
                                    round(Object.values(letter.doc).reduce(
                                        (acc, value) => acc + getFileSize(value),
                                        0
                                    ) / 1_000_000, 2)
                                }) Mb
                            </span>
                        </span>
                    </p>
                </div>`:
                ""
        }
        </div>
        <div class="view-body">
            <p class="text text-type-letter text-color-primary">
                ${letter.text}
            </p>
        </div>`;

    }
}
