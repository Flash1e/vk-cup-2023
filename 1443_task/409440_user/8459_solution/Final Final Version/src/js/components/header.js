import Component from './lib/component.js';
import store from '../store/index.js';
import { showAttr } from '../utils/utils.js';


export default class Header extends Component {
    constructor() {
        super({
            store,
            element: document.querySelector('#header .header-content'),
        });
    }

    render() {
        const backButton = this.element.querySelector('#back-button');
        const logoBox = this.element.querySelector('#logo-box');
        const filterButton = this.element.querySelector('#filter-button');

        showAttr(backButton, store.state.currentLocation !== 'view', 'hidden');
        showAttr(logoBox, store.state.currentLocation === 'view', 'hidden');
        showAttr(filterButton, store.state.currentLocation === 'view', 'hidden');
    }
}