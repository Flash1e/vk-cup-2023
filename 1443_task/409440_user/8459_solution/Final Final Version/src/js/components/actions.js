import Component from './lib/component.js';
import store from '../store/index.js';

import { getAttrsObj } from '../utils/utils.js';

export default class Actions extends Component {
    constructor() {
        super({
            store,
            element: document.querySelectorAll('[data-action]'),
        });
    }

    static onClick(e) {
        if (e.target.hasAttribute('skip')) {
            console.log(e);
            e.stopPropagation();
            return;
        }

        let actionKey = this.getAttribute('data-action');
        let props = getAttrsObj(this);

        store.dispatch(actionKey, props);
    }

    init() {
        this.element.forEach(function (elem) {
            elem.addEventListener('click', Actions.onClick, false);
        });
    }
}
