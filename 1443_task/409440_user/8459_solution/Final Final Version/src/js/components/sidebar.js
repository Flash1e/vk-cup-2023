import Component from './lib/component.js';
import store from '../store/index.js';

export default class Sidebar extends Component {
    constructor() {
        super({
            store,
            element: document.querySelector('#sidebar .folders'),
        });
    }

    init() {
        document.querySelector('#compose-button').addEventListener('click', (e) => {
            document.querySelector('#compose-window').removeAttribute('hidden');
        }, false);
        document.querySelector('#wysiwyg-close-button').addEventListener('click', (e) => {
            document.querySelector('#compose-window').setAttribute('hidden', '');
        }, false);
    }

    render() {
        let folder = store.state.currentFolder;

        let selected = this.element.querySelector('button[selected]');
        if (selected) {
            selected.removeAttribute('selected');
        }

        let newSelected = this.element.querySelector(`[folder="${folder}"]`);
        newSelected.setAttribute('selected', '');
    }
}