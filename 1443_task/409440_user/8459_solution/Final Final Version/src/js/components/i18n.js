import Component from './lib/component.js';
import store from '../store/index.js';


export default class I18n extends Component {
    constructor() {
        super({
            store,
        });
    }

    init() {
        store.dispatch("languageChange", {});  // Language is choosen by first checked radio button in settings
    }

    render() {
        document.querySelectorAll('[data-i18n]').forEach((element) => {
            const keys = element.getAttribute('data-i18n').split('.');
            const value = keys.reduce((obj, i) => obj[i] ?? {}, store.state.langConfig);

            if (value) {
                element.innerHTML = value;
            }
        });
        
        let lang = store.state.langConfig.lang ?? 'ru';
        document.querySelectorAll('.country-flag-img').forEach((element) => {
            element.src = `/assets/icons/flags/${lang}.png`;
            element.alt = lang;
        });
    }
}