export const formatDateShort = (date, lang="ru-RU") => {
    // if date is today use format "HH:MM"
    // else if years are equal use format "MMM DD"
    // else use format "DD.MM.YYYY"

    let format = {
        day: "numeric",
        month: "numeric",
        year: "2-digit",
    };

    date = new Date(date);
    const today = new Date();

    if (date.getFullYear() === today.getFullYear()) {
        if (date.getMonth() === today.getMonth() && 
            date.getDate() === today.getDate()) {
            format = {
                hour: "numeric",
                minute: "numeric",
            };
        } else {
            format = {
                month: "short",
                day: "numeric",
            };
        }
    }

    date = date.toLocaleString(lang, format);

    return date;
}


export const formatDateLong = (date, lang="ru-RU") => {
    console.log(lang);

    let format = {
        day: "numeric",
        month: "long",
        year: "numeric",
        hour: "numeric",
        minute: "numeric",
    };

    date = new Date(date);
    const today = new Date();

    if (date.getFullYear() === today.getFullYear()) {
        if (date.getMonth() === today.getMonth() && 
            date.getDate() === today.getDate()) {
            format = {
                weekday: "long",
                hour: "numeric",
                minute: "numeric",
            };
        } else {
            format = {
                day: "numeric",
                month: "long",
                hour: "numeric",
                minute: "numeric",
            };
        }
    }

    date = date.toLocaleString(lang, format);

    return date;
}


export const getBase64 = (file) => {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = (error) => reject(error);
    });
};


export const getFileSize = (base64str) => {
    return (base64str.length * (3/4)) - 2;
}


export const round = (x, range) => {
    return x.toPrecision(range);
}


export const calcFileSizes = (files) => {
    return round(Object.values(files).reduce(
        (acc, value) => acc + getFileSize(value),
        0
    ) / 1_000_000, 2);
}


export const getAttrsObj = (element) => {
    return element.getAttributeNames().reduce((acc, name) => {
        return {...acc, [name]: element.getAttribute(name)};
    }, {});
}


export const showAttr = (element, condition, attr, attrValue = '') => {
    if (!(element instanceof Element)) {
        return;
    }

    if (condition) {
        element.setAttribute(attr, attrValue);
    } else {
        element.removeAttribute(attr);
    }

    return element;
}


let throttleTimer;
export const throttle = (callback, time) => {
  if (throttleTimer) return;
    throttleTimer = true;
    setTimeout(() => {
        callback();
        throttleTimer = false;
    }, time);
}