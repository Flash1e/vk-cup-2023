 // Load up components
import Actions from "./components/actions.js";
import Letters from "./components/letters.js";
import View from "./components/view.js";
import Header from "./components/header.js";
import Sidebar from "./components/sidebar.js";
import Filter from "./components/filter.js";

import I18n from "./components/i18n.js";
import Themer from "./components/themer.js";

import "./wysiwyg-editor.js";

// Instantiate components
const actionsInstance = new Actions();
const lettersInstance = new Letters();
const viewInstance = new View();
const headerInstance = new Header();
const sidebarInstance = new Sidebar();
const filterInstance = new Filter();
const i18nInstance = new I18n();
const themerInstance = new Themer();

// Initial inits
lettersInstance.init();
actionsInstance.init();
sidebarInstance.init();
i18nInstance.init();
themerInstance.init();

// Initial renders
headerInstance.render();
lettersInstance.render();
sidebarInstance.render();
filterInstance.render();
viewInstance.render();
i18nInstance.render();