const url = require('url');


exports.paginateResults = (model) => async (req, res) => {
    // console.log(res.results);

    const query = url.parse(req.url, true).query;
    const page = query.page ? parseInt(query.page) : 1;
    const limit = query.limit ? parseInt(query.limit) : 10;

    const startIndex = (page - 1) * limit;
    const endIndex = page * limit;

    const out = res.results || { results: undefined, total: 0 };

    try {
        let results_ = await model.setInit(out.results);
        results_ = await results_.skip(startIndex);
        results_ = await results_.limit(limit);
        results_ = await results_.exec();

        out.results = results_;
        out.total = results_.length;

        if (endIndex < out.total - 1) {
            out.next = {
                page: page + 1,
                limit: limit
            }
        }
    
        if (startIndex > 0) {
            out.previous = {
                page: page - 1,
                limit: limit
            }
        }

        res.results = out;
    } catch (error) {
        res.writeHead(500, { 'Content-Type': 'application/json' });
        res.write(JSON.stringify({ message: error.message }));
        res.end();
    }

    return {req, res};
};

exports.searchResults = (model) => async (req, res) => {
    // console.log(res.results);

    const query = url.parse(req.url, true).query;
    const searchParams = Object.assign({}, query);
    
    delete searchParams.page;
    delete searchParams.limit;

    const out = res.results || { results: undefined, total: 0 };

    try {
        let results_ = await model.setInit(out.results);
        results_ = await results_.find(searchParams);
        results_ = await results_.exec();

        out.results = results_;
        out.total = results_.length;

        res.results = out;
    } catch (error) {
        res.writeHead(500, { 'Content-Type': 'application/json' });
        res.write(JSON.stringify({ message: error.message }));
        res.end();
    }

    return {req, res};
};

exports.sortResults = (model) => async (req, res) => {
    // console.log(res.results);

    const out = res.results || { results: undefined, total: 0 };

    try {
        let results_ = await model.setInit(out.results);
        results_ = await results_.sortByDate();
        results_ = await results_.exec();

        out.results = results_;
        out.total = results_.length;
        
        res.results = out;
    } catch (error) {
        res.writeHead(500, { 'Content-Type': 'application/json' });
        res.write(JSON.stringify({ message: error.message }));
        res.end();
    }

    return {req, res};
};