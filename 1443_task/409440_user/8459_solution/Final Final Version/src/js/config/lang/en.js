export default {
    "lang": "en",

    "header": {
        "back-button": "Return",

        "filter-button": "Filter",
        "filter-button-plural": "Filters",
        
        "filter-all": "All messages",
        "filter-unread": "Unread",
        "filter-flagged": "Flagged",
        "filter-doc": "With attachments",
        "filter-reset": " Reset everything"
    },
    "sidebar": {
        "compose-button": "Compose",
        
        "folder-all": "Inbox",
        "folder-important": "Important",
        "folder-sent": "Sent",
        "folder-drafts": "Drafts",
        "folder-archive": "Archive",
        "folder-spam": "Spam",
        "folder-trash": "Trash",
        "new-folder-button": "New folder",
    },
    "settings": {
        "settings-button": "Settings",

        "theme-button": "Interface",
        "language-button": "Language: English",

        "theme-header": "Customize the appearance and design theme of your email",

        "language-header": "Switch Language",
        "language-apply-button": "Switch",

        "theme": {
            "light-mode": "Light theme",
            "dark-mode": "Dark mode",
            "anime": "Anime"
        }
    },
    "view": {
        "to-whom": "To: you",
        "and": "and",
        "recipients": "others",

        "flag-билеты": "Tickets",
        "flag-заказы": "Orders",
        "flag-путешевствия": "Travel",
        "flag-регистрации": "Accounts",
        "flag-финансы": "Finance",
        "flag-штрафы и налоги": "Fines and taxes",

        "download-button": "Download",
        "files": "file(s)",
    },
    "empty-list": {
        "empty-list-header": "No messages",
    },
    "wysiwyg": {
        "header": "New message",
        "toolbar": {
            "paragraph": "Paragraph",
            "h1": "Header 1",
            "h2": "Header 2",
            "h3": "Header 3",
            "justify-left": "Justify left",
            "justify-center": "Justify center",
            "justify-right": "Justify right",
            "outdent": "outdent",
            "indent": "indent",
            "ordered-list": "Ordered list",
            "unordered-list": "Unordered list",
        },
        "send-button": "Send",
        "cancel-button": "Cancel",
        "save-button": "Save",
    }
}