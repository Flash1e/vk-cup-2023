const editor = document.querySelector('#wysiwyg-editor');
const toolbar_buttons = editor.querySelectorAll('.toolbar [data-command]');
const visualView = editor.querySelector('.content-area .visual-view');
let caretPosition = 0;

toolbar_buttons.forEach(button => {
    button.addEventListener('click', (e) => {
        visualView.focus();
        restoreSelection(visualView, caretPosition);

        const action = button.dataset.command;
        const value = button.dataset.value || "";

        document.execCommand(action, false, value);
    }, false);
});

function saveSelection(containerEl) {
    var range = window.getSelection().getRangeAt(0);
    var preSelectionRange = range.cloneRange();
    preSelectionRange.selectNodeContents(containerEl);
    preSelectionRange.setEnd(range.startContainer, range.startOffset);
    var start = preSelectionRange.toString().length;

    return {
        start: start,
        end: start + range.toString().length
    }
}

function restoreSelection(containerEl, savedSel) {
    var charIndex = 0, range = document.createRange();
    range.setStart(containerEl, 0);
    range.collapse(true);
    var nodeStack = [containerEl], node, foundStart = false, stop = false;

    while (!stop && (node = nodeStack.pop())) {
        if (node.nodeType == 3) {
            var nextCharIndex = charIndex + node.length;
            if (!foundStart && savedSel.start >= charIndex && savedSel.start <= nextCharIndex) {
                range.setStart(node, savedSel.start - charIndex);
                foundStart = true;
            }
            if (foundStart && savedSel.end >= charIndex && savedSel.end <= nextCharIndex) {
                range.setEnd(node, savedSel.end - charIndex);
                stop = true;
            }
            charIndex = nextCharIndex;
        } else {
            var i = node.childNodes.length;
            while (i--) {
                nodeStack.push(node.childNodes[i]);
            }
        }
    }

    var sel = window.getSelection();
    sel.removeAllRanges();
    sel.addRange(range);
}


visualView.addEventListener('input', (e) => {
    console.log('hello', e);
    caretPosition = saveSelection(visualView);

    toolbar_buttons.forEach(button => {
        button.checked = false;
    });

    const selectionParent = window.getSelection().anchorNode.parentNode;

    if(!childOf(selectionParent, editor)) return false;

    parentTagActive(selectionParent);
});

function childOf(child, parent) {
    return parent.contains(child);
}

function parentTagActive(elem) {
    if(!elem ||!elem.classList || elem.classList.contains('visual-view')) return false;
    
    let toolbarButton;
    
    let tagName = elem.tagName.toLowerCase();
    toolbarButton = document.querySelectorAll(`.toolbar [data-tag-name="${tagName}"]`)[0];
    if(toolbarButton) {
      toolbarButton.checked = true;
    }
    
    let textAlign = elem.style.textAlign;
    toolbarButton = document.querySelectorAll(`.toolbar [data-style="align:${textAlign}"]`)[0];
    if(toolbarButton) {
        toolbarButton.checked = true;
    }
    
    return parentTagActive(elem.parentNode);
}
